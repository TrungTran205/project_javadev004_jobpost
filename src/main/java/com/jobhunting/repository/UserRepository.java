package com.jobhunting.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jobhunting.entity.User;

@Repository // dùng để truy vấn data từ DB, tương tác với data giữa các Table trong DB
// Class này bắt buộc extends JPA Repository
public interface UserRepository extends JpaRepository<User, Long> // T: Generic Type, Kiểu hay class của 1 Entity (java class), Bảng (table) , ID là kiểu của primary key
{
	@Query("select distinct u from User u where u.userName = :userName")
	public User findByUserName(@Param("userName")String userName);
	
	@Query("select distinct u from User u where u.userName = :userName and u.password = :password")
	public User findByUserNamePassword(@Param("userName")String userName,@Param("password")String password);
	
	@Query("select distinct u from User u where u.userName = :userName and u.email =:email and u.phoneNumber = :phoneNumber")
	public User findByUserNameEmailPhoneNumber(@Param("userName")String userName,@Param("email")String email,@Param("phoneNumber")String phoneNumber);
	
	// 				%...% la co chua ki tu "..."
	//					...%    bat dau voi ki tu "..."
	// 				%...    ket thuc voi ki tu "..."
	@Query("select u from User u where u.firstName like %:name% or u.lastName like %:name%")
	public List<User> searchByName(@Param("name")String name);
	
	@Query("select u from User u where u.userType.name = 'CANDIDATE'")
	public List<User> findAllCandidates();
	
	@Query("select u from User u join u.applyList l where l.id =:jobApplyId")
	public User getUserDetailByJobAppliedId(@Param("jobApplyId")Long jobApplyId);
}


