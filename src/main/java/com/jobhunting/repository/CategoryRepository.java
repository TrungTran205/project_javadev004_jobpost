package com.jobhunting.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jobhunting.entity.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

	@Query("select c from Category c where c.deleted = false ")
	public List<Category> getAllNotDeleted();
	
	@Query("Select c from Category c where c.name like %:name% and c.deleted = false ")
	public List<Category> findCategoryByName(@Param("name") String name);
	
	@Query("select distinct c from Category c join c.postList p where p.vacancy > 0 and p.deleted = false ")
	public List<Category> getCategoriesAndVacancy();
}
