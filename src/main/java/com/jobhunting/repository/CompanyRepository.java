package com.jobhunting.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jobhunting.entity.Company;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {

	@Query("Select c from Company c where c.name like %:name%")
	public List<Company> findCompanyByCompanyName(@Param("name") String name);

	@Query("Select c from Company c where c.deletedCompany = false")
	public List<Company> findAllCompanyExceptDeleted();

	@Query("Select c from Company c where c.deletedCompany = false and c.name like %:name%")
	public List<Company> searchByNameExceptDeleted(@Param("name") String name);

	@Query("select count(*) from Company c join c.postList p where c.id = :companyId and p.deleted = false ")
	public Long countPost(@Param("companyId") Long companyId);

}
