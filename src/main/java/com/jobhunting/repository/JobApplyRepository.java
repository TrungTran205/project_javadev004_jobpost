package com.jobhunting.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jobhunting.entity.JobApply;

@Repository
public interface JobApplyRepository extends JpaRepository<JobApply, Long> {

	@Query("select count(*) from JobApply a where a.status like '%ACCEPTED%' and a.post.id = :postId and a.post.deleted = 'false' ")
	public Long countAccept(@Param("postId") Long postId);
	
	@Query("select a from JobApply a where (a.post.nameJob like %:name% or a.name like %:name%) and  a.post.creator = :userName ")
	public List<JobApply> searchByName(@Param("name") String name, @Param("userName") String userName);

	@Query("select a from JobApply a where a.post.company.deletedCompany = false")
	public List<JobApply> getAllApplyExceptDeleted();
	
	@Query("select distinct a from JobApply a join a.userList u where u.id =:userId and a.post.deleted = 'false'")
	public List<JobApply> getAllApplyExceptDeletedByUser(@Param("userId")Long userId);
	
	@Query("select count(*) from JobApply a join a.userList u where u.id =:userId and a.post.deleted = 'false'")
	public Long getCountAllApplyExceptDeletedByUser(@Param("userId")Long userId);
//	@Query("select a from JobApply a join a.userList u where u.id =:userId")
//	public List<JobApply> getAllApplyExceptDeletedByEmployer(@Param("userId")Long userId);
	
	@Query("select a from JobApply a join a.userList u where (u.userName = :userName and a.post.nameJob like %:name% or a.name like %:name%) ")
	public List<JobApply> searchByNameJob(@Param("name") String name, @Param("userName") String userName);
}
