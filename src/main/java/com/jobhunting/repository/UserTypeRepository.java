package com.jobhunting.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jobhunting.entity.UserType;

@Repository
public interface UserTypeRepository extends JpaRepository<UserType, Long> {
	@Query("select t from UserType t where t.name = :name")
	public UserType getByName(@Param("name") String name);
}
