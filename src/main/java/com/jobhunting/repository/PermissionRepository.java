package com.jobhunting.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jobhunting.entity.Permission;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, Long> {
	
	@Query("select p from Permission p where p.name = :name")
	public Permission getByName(@Param("name") String name);
	
}
