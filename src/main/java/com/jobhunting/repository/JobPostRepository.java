package com.jobhunting.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jobhunting.entity.JobPost;

@Repository
public interface JobPostRepository extends JpaRepository<JobPost, Long> {
	
	@Query("select j from JobPost j where j.nameJob like %:name% and j.deleted = false ")
	public List<JobPost> searchByName(@Param("name") String name);
	
	@Query("select j from JobPost j where j.nameJob like %:name% and j.deleted = false and j.creator = :userName ")
	public List<JobPost> searchJobOfUserByName(@Param("name") String name, @Param("userName") String userName);
	
	@Query("select p from JobPost p where p.deleted = false and p.company.deletedCompany = false")
	public List<JobPost> getAllNotDeleted();
	
	@Query("select j from JobPost j where j.creator = :userName and j.deleted = false")
	public List<JobPost> getAllPostsNotDeletedByUserName(@Param("userName") String userName);
	
	@Query("select j from JobPost j where j.nameJob like %:name% and j.category.id = :categoryId and j.type = :type")
	public List<JobPost> searchByCategoryTypeName(@Param("name") String name, @Param("categoryId") Long categoryId,
			@Param("type") String type);
	
	@Query("select distinct j from JobPost j join j.jobApplyList u join u.userList k where k.id = :userId")
	public List<JobPost> searchByCandidate(@Param("userId") Long userId);
	
	@Query("select distinct j from JobPost j join j.likeJobByList u where u.userName = :userName")
	public List<JobPost> getPostLikeListByUser(@Param("userName") String userName);
}
