package com.jobhunting.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;


@Entity
@Table(name = "ApplyJob")
public class JobApply {
	private Long id;
	private List<User> userList;
	private JobPost post;
//	private List<JobPost> postList;
	private String name;
	private String email;
	private String status;
	private String fileName;
	
	private MultipartFile cvImageFile;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "APPLYJOB_ID", unique = true, nullable = false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="APPLY_NAME", length=125)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="APPLY_EMAIL", length=125)
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name = "STATUS")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name = "FILE_NAME")
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	@Transient
	public MultipartFile getCvImageFile() {
		return cvImageFile;
	}

	public void setCvImageFile(MultipartFile imageFile) {
		this.cvImageFile = imageFile;
	}
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "USER_APPLY", joinColumns = { @JoinColumn(name = "APPLYJOB_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "USER_ID",unique=true) })
	public List<User> getUserList() {
		return userList;
	}
	public void setUserList(List<User> userList) {
		this.userList = userList;
	}
		
	@ManyToOne
	@JoinColumn(name = "JOBPOST_ID")
	public JobPost getPost() {
		return post;
	}
	public void setPost(JobPost post) {
		this.post = post;
	}	
}
