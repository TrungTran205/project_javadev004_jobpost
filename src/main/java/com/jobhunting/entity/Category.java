package com.jobhunting.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
public class Category {
	private Long id;
	private String name;
	private Integer availableJobCount;
	
	private List<JobPost> postList;
	
	private Boolean deleted = false;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CATEGORY_ID", unique = true, nullable = false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="CATEGORY_NAME")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@OneToMany(mappedBy = "category")
	public List<JobPost> getPostList() {
		return postList;
	}
	public void setPostList(List<JobPost> postList) {
		this.postList = postList;
	}
	
	@Transient
	public Integer getAvailableJobCount() {
		return availableJobCount;
	}
	public void setAvailableJobCount(Integer availableJobCount) {
		this.availableJobCount = availableJobCount;
	}
	
	@Column(name="DELETED")
	public Boolean getDeleted() {
		return deleted;
	}
	
	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
	
	
}
