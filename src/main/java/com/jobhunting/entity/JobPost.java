package com.jobhunting.entity;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "JobPost")
public class JobPost {

	private Long id;
	
	// For Job
	private String nameJob;
	private Category category;
	private String type;
	private Long vacancy;
	private String description;
	private String requirement;
	private Long salary;
//	private JobApply jobApply;
	private List<JobApply> jobApplyList;
	
	// For Company
	private Company company;
	
	private Date postDay;

	private Date endDay;
	public Boolean expired;
	
	private String creator;
	
	private Boolean deleted = false;

//	private User likeJobBy; // Dung de save Job ma user thich
	private List<User> userList;
	
	private List<User> likeJobByList;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "JOBPOST_ID", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "NAME_JOB", length = 128)
	public String getNameJob() {
		return nameJob;
	}

	public void setNameJob(String nameJob) {
		this.nameJob = nameJob;
	}

	@Column(name = "TYPE", length = 50)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "VACANCY")
	public Long getVacancy() {
		return vacancy;
	}

	public void setVacancy(Long vacancy) {
		this.vacancy = vacancy;
	}

	@Column(name = "DESCRIPTION", length = 2800)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(name = "SALARY", length = 128)
	public Long getSalary() {
		return salary;
	}

	public void setSalary(Long salary) {
		this.salary = salary;
	}
	
	@Column(name = "REQUIREMENT", length = 1500)
	public String getRequirement() {
		return requirement;
	}

	public void setRequirement(String requirement) {
		this.requirement = requirement;
	}

	@Column(name = "DATE", length = 128)
	public Date getPostDay() {
		return postDay;
	}

	public void setPostDay(Date postDay) {
		this.postDay = postDay;
	}

	@Column(name = "END_DAY", nullable = true)
	public Date getEndDay() {
		return endDay;
	}

	public void setEndDay(Date endDay) {
		this.endDay = endDay;
	}

	@Transient
	public Boolean getExpired() {
		long millis = System.currentTimeMillis();
		Date date = new Date(millis);
		if (date.getTime() > endDay.getTime()) {
			return true;
		}
		return false;
	}

	public void setExpired(Boolean expired) {
		this.expired = expired;
	}

	@Column(name = "DELETED")
	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	@Column(name = "CREATOR", length = 128)
	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	// Post <-> Job's Category
	@ManyToOne
	@JoinColumn(name = "CATEGORY_ID")
	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@ManyToOne
	@JoinColumn(name = "COMPANY_ID")
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

//	@ManyToOne
//	@JoinColumn(name = "USER_ID")
//	public User getLikeJobBy() {
//		return likeJobBy;
//	}
//
//	public void setLikeJobBy(User likeJobBy) {
//		this.likeJobBy = likeJobBy;
//	}

	@ManyToMany
	@JoinTable(name = "USER_JOBPOST", joinColumns = { @JoinColumn(name = "JOBPOST_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "USER_ID") })
	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}
	
	@OneToMany(mappedBy = "post",cascade=CascadeType.ALL)
	public List<JobApply> getJobApplyList() {
		return jobApplyList;
	}

	public void setJobApplyList(List<JobApply> jobApplyList) {
		this.jobApplyList = jobApplyList;
	}
	
	
	@ManyToMany
	@JoinTable(name = "USER_JOBPOSTLIKE", joinColumns = { @JoinColumn(name = "JOBPOST_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "USER_ID") })
	public List<User> getLikeJobByList() {
		return likeJobByList;
	}

	public void setLikeJobByList(List<User> likeJobByList) {
		this.likeJobByList = likeJobByList;
	}

}
