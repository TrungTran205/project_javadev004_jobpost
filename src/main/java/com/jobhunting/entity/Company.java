package com.jobhunting.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

@Entity
public class Company {
	private Long id;
	private String name;

	private String info;
	private String scale;
	private String welfare;
	private String address;
	private String email;
	private String logoName;
	private String imageName;

	private MultipartFile logoFile;
	private MultipartFile imageFile;

	private List<JobPost> postList;
	private List<User> userList;
	
	private Boolean deletedCompany=false;
	
	// Dung de xac dinh chuyen tu trang newPost hay trang listCompany
	private Boolean fromJobPost = false;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "COMPANY_ID", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "COMPANY_NAME")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "INFO", length = 1500)
	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	@Column(name = "SCALE", nullable = false)
	public String getScale() {
		return scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	@Column(name = "WELFARE", length = 2000)
	public String getWelfare() {
		return welfare;
	}

	public void setWelfare(String welfare) {
		this.welfare = welfare;
	}

	@Column(name = "ADDRESS")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "EMAIL")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "LOGO_NAME")
	public String getLogoName() {
		return logoName;
	}

	public void setLogoName(String logoName) {
		this.logoName = logoName;
	}

	@Column(name = "IMAGE_NAME")
	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	@Transient
	public MultipartFile getLogoFile() {
		return logoFile;
	}

	public void setLogoFile(MultipartFile logoFile) {
		this.logoFile = logoFile;
	}

	@Transient
	public MultipartFile getImageFile() {
		return imageFile;
	}

	public void setImageFile(MultipartFile imageFile) {
		this.imageFile = imageFile;
	}
	
	
	@OneToMany(mappedBy = "company",cascade = {CascadeType.ALL})
	public List<JobPost> getPostList() {
		return postList;
	}

	public void setPostList(List<JobPost> postList) {
		this.postList = postList;
	}

	@ManyToMany
	@JoinTable(name = "USER_COMPANY", joinColumns = { @JoinColumn(name = "USER_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "COMPANY_ID") })
	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}
	
	@Column(name="DELETED_COMPANY")
	public Boolean getDeletedCompany() {
		return deletedCompany;
	}

	public void setDeletedCompany(Boolean deletedCompany) {
		this.deletedCompany = deletedCompany;
	}

	@Transient
	public Boolean getFromJobPost() {
		return fromJobPost;
	}

	public void setFromJobPost(Boolean fromJobPost) {
		this.fromJobPost = fromJobPost;
	}

}
