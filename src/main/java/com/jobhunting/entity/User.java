
package com.jobhunting.entity;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

@Entity
@Table(name = "user")
public class User {

	private Long id;
	private String userName;
	private String password;
	private String email;
	private String phoneNumber;
	private String firstName;
	private String lastName;
	private String imageName; // Ten hinh anh luu trong database

	private Date dob;
	private String address;
	private String experiences;
	private String skills;
	
	private Boolean isActive = false;

	private MultipartFile imageFile; // File hinh anh luu trong memory

	private List<Permission> permissionList;
	private UserType userType;
	
	private List<JobPost> postList;
//	private List<JobPost> jobLiked;
	private List<JobPost> jobLikedByUserList;

	private List<JobApply> applyList;
	private List<Company> companyList;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "USER_ID", unique = true, nullable = false)
	public Long getId() {

		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "USER_NAME", length = 128)
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "PASSWORD", length = 256)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "EMAIL", length = 128)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "PHONE_NUMBER", length = 20)
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Column(name = "FIRST_NAME", length = 20)
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "LAST_NAME", length = 20)
	public String getLastName() {

		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "IMAGE_NAME")
	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	// Sinh ra trong Entity ma khong muon save trong Database -> Dung @transient
	@Transient
	public MultipartFile getImageFile() {
		return imageFile;
	}

	public void setImageFile(MultipartFile imageFile) {
		this.imageFile = imageFile;
	}

	@Column(name = "IS_ACTIVE", length = 1)
	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "USER_PERMISSION", joinColumns = { @JoinColumn(name = "USER_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "PERMISSION_ID") })
	public List<Permission> getPermissionList() {
		return permissionList;
	}

	public void setPermissionList(List<Permission> permissionList) {
		this.permissionList = permissionList;
	}

	// user <-> user type
	@ManyToOne
	@JoinColumn(name = "USERTYPE_ID")
	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	// Job post list <-> user
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name = "USER_JOBPOST", joinColumns = { @JoinColumn(name = "USER_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "JOBPOST_ID") })
	public List<JobPost> getPostList() {
		return postList;
	}

	public void setPostList(List<JobPost> postList) {
		this.postList = postList;
	}

//	@OneToMany(mappedBy = "likeJobBy")
//	public List<JobPost> getJobLiked() {
//		return jobLiked;
//	}
//
//	public void setJobLiked(List<JobPost> jobLiked) {
//		this.jobLiked = jobLiked;
//	}

	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name = "USER_APPLY", joinColumns = { @JoinColumn(name = "USER_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "APPLYJOB_ID",unique=true) })
	public List<JobApply> getApplyList() {
		return applyList;
	}

	public void setApplyList(List<JobApply> applyList) {
		this.applyList = applyList;
	}

	@ManyToMany
	@JoinTable(name = "USER_COMPANY", joinColumns = { @JoinColumn(name = "USER_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "COMPANY_ID") })
	public List<Company> getCompanyList() {
		return companyList;
	}
	
	public void setCompanyList(List<Company> companyList) {
		this.companyList = companyList;
	}
	
	@Column(name="DOB",length=10)
	public Date getDob() {
		return dob;
	}
	
	public void setDob(Date dob) {
		this.dob = dob;
	}
	
	@Column(name = "ADDRESS", length = 255)
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	@Column(name = "EXPERIENCES", length = 1024)
	public String getExperiences() {
		return experiences;
	}
	
	public void setExperiences(String experiences) {
		this.experiences = experiences;
	}
	
	@Column(name = "SKILLS", length = 1024)
	public String getSkills() {
		return skills;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}

	@ManyToMany
	@JoinTable(name = "USER_JOBPOSTLIKE", joinColumns = { @JoinColumn(name = "USER_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "JOBPOST_ID") })
	public List<JobPost> getJobLikedByUserList() {
		return jobLikedByUserList;
	}

	public void setJobLikedByUserList(List<JobPost> jobLikedByUserList) {
		this.jobLikedByUserList = jobLikedByUserList;
	}
}