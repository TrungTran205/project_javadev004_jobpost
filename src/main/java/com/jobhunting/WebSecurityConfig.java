package com.jobhunting;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Bean
	public UserDetailsService userDetailsService() {
		return new UserDetailsServiceImpl();
	};

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	};

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService()).passwordEncoder(passwordEncoder());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		.antMatchers(
				"/", 
				"/home/**",
				"/login",
				"/forgotPassword",
				"/resetPassword",
				"/changePassword",
				"/register",
				"/registerConfirm",
				"/registerEmployer",
				"/uploads/**",
				"/resources/**")
		.permitAll()
		.antMatchers("/admin/**")
//		.hasRole("ADMIN")
		.hasAnyRole("ADMIN","EMPLOYER","CANDIDATE")
		.anyRequest()
		.authenticated()
		.and()
		.formLogin()
		.loginPage("/login")
		.defaultSuccessUrl("/admin/",true)
		.failureUrl("/login?error")
		.and()
		.logout()
		.permitAll()
		.logoutSuccessUrl("/home/")
		.and()
		.csrf()
		.disable();
	}
}
