package com.jobhunting.model;

import org.springframework.web.multipart.MultipartFile;

public class JobApplyModel {
	// demo cho thong tin khong muon save vao database
	private Long jobPostId;
	private String name;
	private String email;
	private MultipartFile cvImageFile;
	private String fileName;

	
	public Long getJobPostId() {
		return jobPostId;
	}
	public void setJobPostId(Long jobPostId) {
		this.jobPostId = jobPostId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public MultipartFile getCvImageFile() {
		return cvImageFile;
	}
	public void setCvImageFile(MultipartFile imageFile) {
		this.cvImageFile = imageFile;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
}
