package com.jobhunting;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jobhunting.services.UserService;

@Component
public class InitializationLoader {
	@Autowired
	private UserService userService;

	@PostConstruct
	public void init() {
		try {
			if (userService.getByUserName("admin") == null) {
				userService.createDefaultAdmin();
			}
			if(userService.getAllCategory().isEmpty() == true) {
				userService.createDefaultCategory();
			}
			if(userService.getAllType().isEmpty() == true) {
				userService.createDefaultUserType();
			}
			if(userService.getAllPermission().size() == 1) {
				userService.createDefaultPermission();
			}
		} catch (Exception e) {
// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
