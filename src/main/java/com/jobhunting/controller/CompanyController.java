package com.jobhunting.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.jobhunting.entity.Company;
import com.jobhunting.repository.CompanyRepository;
import com.jobhunting.services.CompanyService;
import com.jobhunting.services.ImageService;

@Controller
@RequestMapping(value = "/masterdata/company")
public class CompanyController {

	@Autowired
	private ImageService imageService;

	@Autowired
	private CompanyService companyService;

	// Add new Company
	@RequestMapping(value = "/newCompany", method = RequestMethod.GET)
	public ModelAndView getnewCompanyView(@RequestParam("fromJobPost") Boolean fromJobPost) {
		Company company = new Company();
		ModelAndView model = new ModelAndView();
		if (fromJobPost == true) {
			model.setViewName("newCompany");
		} else {
			model.setViewName("admin-newCompany");
		}
		model.addObject("company", company);
		model.addObject("fromJobPost", fromJobPost);
		return model;
	}

	@RequestMapping(value = "/saveCompany", method = RequestMethod.POST)
	public String saveCompany(@ModelAttribute("company") Company companyModel, Model model,
			HttpServletRequest request) {
		String uploadPath = request.getServletContext().getRealPath("uploads");

		String imageName = imageService.uploadFile(uploadPath, companyModel.getImageFile());
		String logoName = imageService.uploadFile(uploadPath, companyModel.getLogoFile());

		if (imageName != null && !imageName.isEmpty()) {
			companyModel.setImageName(imageName);
		}

		if (logoName != null && !logoName.isEmpty()) {
			companyModel.setLogoName(logoName);
		}
		String errorMessage = companyService.saveCompany(companyModel);
		if (errorMessage == null) {
			companyService.saveCompany(companyModel);
			List<Company> companyList = companyService.getAllCompanyExceptDeleted();
			model.addAttribute("companyList", companyList);
		} else {
			model.addAttribute("errorMessage", errorMessage);
			if (companyModel.getFromJobPost() == true) {
				return "redirect:/masterdata/company/newCompany?fromJobPost=true";
			} else {
				return "redirect:/masterdata/company/newCompany?fromJobPost=false";
			}
		}
		if (companyModel.getFromJobPost() == true) {
			model.addAttribute("company", companyModel);
			return "redirect:/home/employer/newPost";
		} else {
			return "redirect:/masterdata/company/listCompany";
		}

	}

	// Xem list Company
	@RequestMapping(value = "/listCompany", method = RequestMethod.GET)
	public ModelAndView getListCompanyView() {
		List<Company> companyList = companyService.getAllCompanyExceptDeleted();
		ModelAndView model = new ModelAndView();
		model.setViewName("listCompany");
		model.addObject("companyList", companyList);
		return model;
	}

	// Xoa Company
	@RequestMapping(value = "/deleteCompany", method = RequestMethod.GET)
	public ModelAndView deleteCompany(@RequestParam("id") Long id) {
		List<Company> compList = companyService.deleteCompanyStatus(id);
		ModelAndView model = new ModelAndView();
		model.setViewName("listCompany");
		model.addObject("compList", compList);
		return model;
	}
	
//	@RequestMapping(value = "/deleteCompany", method = RequestMethod.GET)
//	public ModelAndView deleteCompany(@RequestParam("id") Long id) {
//		List<Company> compList = companyService.deleteCompany(id);
//		ModelAndView model = new ModelAndView();
//		model.setViewName("listCompany");
//		model.addObject("compList", compList);
//		return model;
//	}

	// Sua Company
	@RequestMapping(value = "/editCompany", method = RequestMethod.GET)
	public ModelAndView editCompany(@RequestParam("id") Long id) {
		Company company = companyService.findById(id);
		ModelAndView model = new ModelAndView();
		model.setViewName("editCompany");
		model.addObject("company", company);
		return model;
	}

	@RequestMapping(value = "/updateCompany", method = RequestMethod.POST)
	public String updateCompany(@ModelAttribute("company") Company companyModel, Model model,
			HttpServletRequest request) {
		String uploadPath = request.getServletContext().getRealPath("uploads");

		String imageName = imageService.uploadFile(uploadPath, companyModel.getImageFile());
		String logoName = imageService.uploadFile(uploadPath, companyModel.getLogoFile());

		if (imageName != null && !imageName.isEmpty()) {
			companyModel.setImageName(imageName);
		}

		if (logoName != null && !logoName.isEmpty()) {
			companyModel.setLogoName(logoName);
		}
		String errorMessage = companyService.updateCompany(companyModel);
		if (errorMessage != null) {
			model.addAttribute("errorMessage", errorMessage);
			return "editCompany";
		} else {
			companyService.updateCompany(companyModel);
			List<Company> companyList = companyService.getAllCompanyExceptDeleted();
			model.addAttribute("companyList", companyList);
			return "listCompany";
		}
	}

	// Tim kiem Company
	@RequestMapping(value = "/searchCompany", method = RequestMethod.POST)
	public String searchCompany(@RequestParam("search") String searchValue, Model model) {
		List<Company> companyList = companyService.searchByNameExceptDeleted(searchValue);
		model.addAttribute("companyList", companyList);
		model.addAttribute("searchValue", searchValue);

		return "listCompany";
	}

	// Xem thong tin Company
	@RequestMapping(value = "/viewCompanyDetails", method = RequestMethod.GET)
	public ModelAndView getCompanyDetailsView(@RequestParam("id") Long id) {
		Company company = companyService.findById(id);
		ModelAndView model = new ModelAndView();
		model.setViewName("viewCompanyDetails");
		model.addObject("company", company);
		return model;
	}

}
