package com.jobhunting.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.jobhunting.entity.JobApply;
import com.jobhunting.entity.JobPost;
import com.jobhunting.entity.Permission;
import com.jobhunting.entity.User;
import com.jobhunting.entity.UserType;
import com.jobhunting.services.EmployerService;
import com.jobhunting.services.HomeService;
import com.jobhunting.services.ImageService;
import com.jobhunting.services.JobApplyService;
import com.jobhunting.services.PermissionService;
import com.jobhunting.services.UserTypeService;

@Controller
@RequestMapping(value = "/admin")
public class AdminController {

	@Autowired
	private HomeService homeService;
	
	@Autowired
	private EmployerService employerService;

	@Autowired
	private UserTypeService userTypeService;
	
	@Autowired
	private PermissionService permissionService;

	@Autowired
	private ImageService imageService;

	@Autowired
	private JobApplyService jobApplyService;

	@RequestMapping(value = "/newuser", method = RequestMethod.GET)
	public ModelAndView addNewUser() {
		User user = new User();
		List<UserType> userTypeList = userTypeService.getAllUserType();
		List<Permission> permissionList = permissionService.getAllPermission();

		ModelAndView model = new ModelAndView();
		model.setViewName("newUser");
		model.addObject("user", user);
		model.addObject("userTypeList", userTypeList);
		model.addObject("permissionList", permissionList);
		return model;
	}

	@RequestMapping(value = "/edituser", method = RequestMethod.GET)
	public ModelAndView getEditUserView(@RequestParam("id") Long id) {
		User user = homeService.getUserDetail(id);
		List<UserType> userTypeList = userTypeService.getAllUserType();
		List<Permission> permissionList = permissionService.getAllPermission();

		ModelAndView model = new ModelAndView();
		model.setViewName("editUser");
		model.addObject("user", user);
		model.addObject("userTypeList", userTypeList);
		model.addObject("permissionList", permissionList);
		return model;
	}

	@RequestMapping(value = "/listuser", method = RequestMethod.GET)
	public ModelAndView getListUserView() {
		List<User> userList = homeService.getAllUsers();
		ModelAndView model = new ModelAndView();
		model.setViewName("listUser");
		model.addObject("userList", userList);
		return model;
	}
	
	@RequestMapping(value = "/listPost", method = RequestMethod.GET)
	public ModelAndView getListPostView() {
		List<JobPost> postList = employerService.getAllPostsNotDeleted();
//		List<JobPost> postList = employerService.getAllPosts();
		ModelAndView model = new ModelAndView();
		model.setViewName("admin-listPost");
		model.addObject("postList", postList);
		return model;
	}
	
	@RequestMapping(value = "/viewDetails", method = RequestMethod.GET)
	public ModelAndView getDetailsView(@RequestParam("postId") Long postId) {
		JobPost post = employerService.getJobPostDetail(postId);
		ModelAndView model = new ModelAndView();
		model.setViewName("admin-viewJobDetails");
		model.addObject("post", post);
		return model;
	}
	
	@RequestMapping(value = "/listCandidates", method = RequestMethod.GET)
	public ModelAndView getListResumeView() {
		List<JobApply> applyList = jobApplyService.getAllApply();
		ModelAndView model = new ModelAndView();
		model.setViewName("admin-listCandidate");
		model.addObject("applyList", applyList);
		return model;
	}
	
	@RequestMapping(value = "/listCVs", method = RequestMethod.GET)
	public ModelAndView getListCVView() {
		List<JobApply> applyList = jobApplyService.getAllApply();
		ModelAndView model = new ModelAndView();
		model.setViewName("admin-listCV");
		model.addObject("applyList", applyList);
		return model;
	}
	
	@RequestMapping(value = "/listUserJobApplied", method = RequestMethod.GET)
	public ModelAndView getUserJobAppliedList(HttpServletRequest request) {
		List<User> userList = homeService.getAllCandidates();
		List<Long> countList = new ArrayList<>();
		
		for(User user:userList){
			Long count = jobApplyService.getCountAllApplyExceptDeletedByUser(user.getId());
			countList.add(count);
		}
		
		ModelAndView model = new ModelAndView();
		model.setViewName("admin-listUserJobApply");
		model.addObject("userList", userList);
		model.addObject("countList", countList);
		return model;
	}
	
	@RequestMapping(value = "/listUserJobAppliedDetails", method = RequestMethod.GET)
	public ModelAndView getUserJobAppliedList(@RequestParam("userId")Long userId) {
//		List<JobApply> applyList = jobApplyService.getAllApplyExceptDeletedByUser(userId);
		List<JobPost> postList = employerService.searchByCandidate(userId);
		ModelAndView model = new ModelAndView();
		model.setViewName("admin-listUserJobApplyDetails");
		model.addObject("postList", postList);
		return model;
	}

	@RequestMapping(value = "/updateuser", method = RequestMethod.POST)
	public String updateUser(@ModelAttribute("user") User userModel, HttpServletRequest request, Model model) {
		String uploadPath = request.getServletContext().getRealPath("uploads");
		String imageName = imageService.uploadFile(uploadPath, userModel.getImageFile());
		if (imageName != null && !imageName.isEmpty()) {
			userModel.setImageName(imageName);
		}
		String errorMessage = homeService.updateUser(userModel);
		
		if (errorMessage != null) {
			List<UserType> userTypeList = userTypeService.getAllUserType();
			List<Permission> permissionList = permissionService.getAllPermission();

			model.addAttribute("user", userModel);
			model.addAttribute("errorMessage", errorMessage);
			model.addAttribute("userTypeList", userTypeList);
			model.addAttribute("permissionList", permissionList);
			return "editUser";
		} 
		homeService.updateUser(userModel);
		List<User> userList = homeService.getAllUsers();
		// Save thanh cong & chuyen sang trang List User
		model.addAttribute("userList", userList);
		model.addAttribute("user", userModel);

		return "listUser";
	}

	@RequestMapping(value = "/saveuser", method = RequestMethod.POST)
	public String saveUser(@ModelAttribute("user") User userModel, Model model) {
		List<User> userList = homeService.getAllUsers();
		List<UserType> userTypeList = userTypeService.getAllUserType();
		List<Permission> permissionList = permissionService.getAllPermission();
		// New
		if (userModel.getId() == null) {
			String errorMessage = homeService.saveUser(userModel);
			model.addAttribute("errorMessage", errorMessage);
			if (errorMessage != null) {
				model.addAttribute("userTypeList", userTypeList);
				model.addAttribute("permissionList", permissionList);
				return "newUser";
			}
		} else {
			homeService.saveUser(userModel);
		}
		model.addAttribute("userList", userList);
		return "redirect:/admin/listuser";
		// Old
//		// Save thanh cong & chuyen sang trang List User
//		ModelAndView model = new ModelAndView();
//		model.setViewName("listUser");
//		model.addObject("userList", userList);
//		model.addObject("user",user);
//		return model;
	}

	@RequestMapping(value = "/deleteuser", method = RequestMethod.GET)
	public ModelAndView getDeleteUserView(@RequestParam("id") Long id) {
		List<User> userList = homeService.deleteUser(id);
		ModelAndView model = new ModelAndView();
		model.setViewName("listUser");
		model.addObject("userList", userList);
		return model;
	}

	// Dashboard (muon de gi de)
	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public ModelAndView getDashboardView() {
		ModelAndView model = new ModelAndView();
		model.setViewName("dashboard");
		return model;
	}
	
	@RequestMapping(value = "/")
	public String getDashboardView(Principal principal) {
		String userName = principal.getName();
		User user = homeService.getUserDetailByUserName(userName);
		List<Permission> roleList = user.getPermissionList();
		for (Permission permission : roleList) {
			if (permission.getName().equals("ADMIN")) {
				return "redirect:/admin/dashboard";
			} else {
				return "redirect:/home/";
			}
		}
		return "redirect:/home/";
	}

	@RequestMapping(value = "/viewUser", method = RequestMethod.GET)
	public ModelAndView viewUser(@RequestParam(name = "userId") Long userId) {
		User user = homeService.getUserDetail(userId);

		// Save thanh cong & chuyen sang trang List
		ModelAndView model = new ModelAndView();
		model.setViewName("viewUser");
		model.addObject("user", user);
		return model;
	}

	@RequestMapping(value = "/viewUserDetails", method = RequestMethod.GET)
	public ModelAndView viewUserDetails(@RequestParam(name = "jobAppliedId") Long jobAppliedId) {
		User user = homeService.getUserDetailByJobAppliedId(jobAppliedId);
//		User user = homeService.getUserDetail(userId);

		// Save thanh cong & chuyen sang trang List
		ModelAndView model = new ModelAndView();
		model.setViewName("viewUser");
		model.addObject("user", user);
		return model;
	}
	
	@RequestMapping(value = "/searchUser", method = RequestMethod.POST)
	public String searchUser(@RequestParam("search") String searchValue, 
			HttpServletRequest request, Model model) {
			
			List<User> userList = homeService.searchByName(searchValue);
			model.addAttribute("userList", userList);
			model.addAttribute("searchValue", searchValue);
		
		return "listUser";

	}
}
