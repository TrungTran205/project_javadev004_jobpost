package com.jobhunting.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.jobhunting.entity.UserType;
import com.jobhunting.services.UserTypeService;

@Controller
@RequestMapping(value = "/admin/userType")
public class UserTypeController {
	
	@Autowired
	private UserTypeService userTypeService;
	
	@RequestMapping(value="/newUserType", method=RequestMethod.GET)
	public ModelAndView getNewUserTypeView() {
		ModelAndView model = new ModelAndView();
		model.setViewName("newUserType");
		return model;
	}
	
	@RequestMapping(value="/listUserType", method=RequestMethod.GET)
	public ModelAndView getListUserTypeView() {
		List<UserType> typeList = userTypeService.getAllUserType();
		ModelAndView model = new ModelAndView();
		model.setViewName("listUserType");
		model.addObject("typeList",typeList);
		return model;
	}
	
	@RequestMapping(value = "saveUserType", method=RequestMethod.POST)
	public ModelAndView saveUserType(String name) {
		UserType userType = userTypeService.saveUserType(name);
		List<UserType> typeList = userTypeService.getAllUserType();
		ModelAndView model = new ModelAndView();
		model.setViewName("listUserType");
		model.addObject("userType", userType);
		model.addObject("typeList",typeList);
		return model;
	}
	
	@RequestMapping(value = "/deleteUserType", method=RequestMethod.GET)
	public ModelAndView getDeleteUserTypeView(@RequestParam("id") Long id) {
		List<UserType> typeList = userTypeService.deleteUserType(id);
		ModelAndView model = new ModelAndView();
		model.setViewName("listUserType");
		model.addObject("typeList", typeList);
		return model;
	}
	
}
