package com.jobhunting.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jobhunting.entity.User;
import com.jobhunting.services.HomeService;

@Controller
public class LoginController {

	@Autowired
	private HomeService homeService;
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String getLoginView(HttpServletRequest request){
		String error=request.getQueryString();
		
		if(error!=null){
		String errorMessage="Your user name or password is not correct";
		request.setAttribute("errorMessage",errorMessage);
		}
		return "login";
	}
	
	@RequestMapping(value = "/register")
	public String getRegisterView(Model model) {
		model.addAttribute("user", new User());
		return "register";
	}
	
	@RequestMapping(value = "/registerEmployer")
	public String getRegisterEmployerView(Model model) {
		model.addAttribute("user", new User());
		return "registerEmployer";
	}
	
	@RequestMapping(value = "/registerConfirm", method = RequestMethod.GET)
	public String getRegisterConfirmView() {
		return "registerConfirm";
	}

	@RequestMapping(value = "/registerEmployer", method = RequestMethod.POST)
	public String registerEmployer(@ModelAttribute("user") User user, Model model) {
		String errorMessage = homeService.registerEmployer(user);
		model.addAttribute("errorMessage", errorMessage);
		if (errorMessage != null) {
			return "register";
		}
		return "redirect:/login";
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String registerCandidate(@ModelAttribute("user") User user, Model model) {
		String errorMessage = homeService.registerCandidate(user);
		model.addAttribute("errorMessage", errorMessage);
		if (errorMessage != null) {
			return "register";
		}
		return "redirect:/login";
	}

}
