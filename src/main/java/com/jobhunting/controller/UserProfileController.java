package com.jobhunting.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.jobhunting.entity.User;
import com.jobhunting.model.PasswordChangeModel;
import com.jobhunting.services.HomeService;
import com.jobhunting.services.UserService;

@Controller
@RequestMapping(value = "/admin")
public class UserProfileController {

	@Autowired
	private HomeService homeService;
	
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/userProfile", method = RequestMethod.GET)
	public ModelAndView getUserProfileView(Principal principal) {

		String userName = principal.getName();
		User user = homeService.getUserDetailByUserName(userName);

		ModelAndView model = new ModelAndView();
		model.setViewName("viewUser");
		model.addObject("user", user);
		return model;
	}

	@RequestMapping(value = "/userProfile/changePassword", method = RequestMethod.GET)
	public ModelAndView getChangePasswordView() {

		PasswordChangeModel changePasswordModel = new PasswordChangeModel();

		ModelAndView model = new ModelAndView();
		model.setViewName("admin-changepassword");
		model.addObject("changePasswordModel", changePasswordModel);
		return model;
	}

	@RequestMapping(value = "/userProfile/changePassword", method = RequestMethod.POST)
	public String changePasswordView(@ModelAttribute("changePasswordModel") PasswordChangeModel changePasswordModel,
			Principal principal,
			Model model) {
		String userName = principal.getName();
		String error = this.userService.changePassword(userName, changePasswordModel);
		if (error != null) {
			model.addAttribute("errorMessage", error);
			return "admin-changepassword";
		}

		return "redirect:/admin/listuser";
	}
}
