package com.jobhunting.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.jobhunting.model.PasswordChangeModel;
import com.jobhunting.services.UserService;

@Controller
public class ForgotPasswordController {
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/forgotPassword", method = RequestMethod.GET)
	public String forgotPassword() {
		return "forgotPassword";
	}

	@RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
	public String resetPassword(String userName, String phoneNumber,String email, Model model) {
		PasswordChangeModel changePasswordModel = new PasswordChangeModel();
		String error = userService.checkUserInfo(userName, phoneNumber, email);
		if (error != null) {
			model.addAttribute("errorMessage", error);
			return "forgotPassword";
		}
			model.addAttribute("userName", userName);
			model.addAttribute("changePasswordModel",changePasswordModel);
			return "resetPassword";
	}

	@RequestMapping(value = "/changePassword", method = RequestMethod.POST)
	public String getChangePassword(@RequestParam(name="userName") String userName,@ModelAttribute("changePasswordModel") PasswordChangeModel changePasswordModel,Model model) {
		String error = userService.changePasswordForgot(userName, changePasswordModel);
		if (error != null) {
			model.addAttribute("errorMessage", error);
			model.addAttribute("userName",userName);
			return "resetPassword";
		}
		return "ChangePasswordSuccess";
	}
}
