package com.jobhunting.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.jobhunting.entity.Permission;
import com.jobhunting.services.PermissionService;

@Controller
@RequestMapping(value = "/admin/permission")
public class PermissionController {

	@Autowired
	private PermissionService permissionService;
	
	@RequestMapping(value="/newPermission", method=RequestMethod.GET)
	public ModelAndView getNewPermissionView() {
		ModelAndView model = new ModelAndView();
		model.setViewName("newPermission");
		return model;
	}
	
	@RequestMapping(value = "/listPermission", method=RequestMethod.GET)
	public ModelAndView getListPermissionView() {
		List<Permission> permissionList = permissionService.getAllPermission();
		ModelAndView model = new ModelAndView();
		model.setViewName("listPermission");
		model.addObject("permissionList",permissionList);
		return model;
	}
	
	@RequestMapping(value = "savePermission", method=RequestMethod.POST)
	public ModelAndView savePermission(String name) {
		Permission permission = permissionService.savePermission(name);
		List<Permission> permissionList = permissionService.getAllPermission();
		ModelAndView model = new ModelAndView();
		model.setViewName("listPermission");
		model.addObject("permission", permission);
		model.addObject("permissionList",permissionList);
		return model;
	}
	
}
