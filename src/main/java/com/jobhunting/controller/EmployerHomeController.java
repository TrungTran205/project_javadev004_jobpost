package com.jobhunting.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.jobhunting.entity.Category;
import com.jobhunting.entity.Company;
import com.jobhunting.entity.JobApply;
import com.jobhunting.entity.JobPost;

import com.jobhunting.entity.User;
import com.jobhunting.services.CategoryService;
import com.jobhunting.services.CompanyService;
import com.jobhunting.services.EmployerService;
import com.jobhunting.services.HomeService;
import com.jobhunting.services.JobApplyService;


@Controller
@RequestMapping(value = "/home/employer")
public class EmployerHomeController {
	@Autowired
	private EmployerService employerService;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private HomeService homeService;

	@Autowired
	private JobApplyService jobApplyService;

	@RequestMapping(value = "/newPost", method = RequestMethod.GET)
	public ModelAndView getNewPostView(Principal principal, HttpServletRequest request) {
		String userName = principal.getName();
		JobPost post = new JobPost();
		JobPost post1 = (JobPost) request.getSession().getAttribute("jobPost");
		if (post1 != null) {
			post = post1;
		}
		List<Category> categoryList = categoryService.getAllNotDeletedCategory();
		List<Company> companyList = companyService.getAllCompanyExceptDeleted();
		ModelAndView model = new ModelAndView();
		model.setViewName("newPost");
		model.addObject("post", post);
		model.addObject("userName", userName);
		model.addObject("categoryList", categoryList);
		model.addObject("companyList", companyList);
		return model;
	}

	@RequestMapping(value = "/editPost", method = RequestMethod.GET)
	public ModelAndView getEditPostView(@RequestParam("postId") Long postId, Principal principal) {
		JobPost post = employerService.getJobPostDetail(postId);
		String userName = principal.getName();

		List<Company> companyList = companyService.getAllCompanyExceptDeleted();
		List<Category> categoryList = categoryService.getAllCategory();

		ModelAndView model = new ModelAndView();
		model.setViewName("updatePost");
		model.addObject("post", post);
		model.addObject("userName", userName);
		model.addObject("companyList", companyList);
		model.addObject("categoryList", categoryList);
		return model;
	}

	@RequestMapping(value = "/listPost", method = RequestMethod.GET)
	public ModelAndView getListJobPostView(Principal principal) {
		String userName = principal.getName();
//		User user = homeService.getUserDetailByUserName(userName);
//		List<JobPost> postList = user.getPostList();
		List<JobPost> postList = employerService.getAllPostsNotDeletedByUserName(userName);
		List<JobPost> postList1 = employerService.getAllPostsNotDeleted();
		List<Category> categoryList = categoryService.getAllCategory();
		ModelAndView model = new ModelAndView();
		model.setViewName("listPost");
		model.addObject("postList", postList);
		model.addObject("postList1", postList1);
		model.addObject("categoryList", categoryList);
		return model;
	}

	@RequestMapping(value = "/viewJobDetails", method = RequestMethod.GET)
	public ModelAndView getDetailsView(@RequestParam("postId") Long postId) {
		JobPost post = employerService.getJobPostDetail(postId);
		ModelAndView model = new ModelAndView();
		model.setViewName("viewJobDetails");
		model.addObject("post", post);
		return model;
	}

	@RequestMapping(value = "/updatePost", method = RequestMethod.POST)
	public String updatePost(@ModelAttribute("post") JobPost jobPostModel, Model model1) {
		String errorMessage = employerService.updatePost(jobPostModel);
		List<JobPost> postList = employerService.getAllPostsExceptDeleted();
		List<Company> companyList = companyService.getAllCompanyExceptDeleted();
		if (errorMessage != null) {
			model1.addAttribute("errorMessage", errorMessage);
			List<Category> categoryList = categoryService.getAllCategory();
			model1.addAttribute("companyList", companyList);
			model1.addAttribute("categoryList", categoryList);
			model1.addAttribute("post", jobPostModel);
			return "editPost";
		} else {
			employerService.updatePost(jobPostModel);
			model1.addAttribute("postList", postList);
			model1.addAttribute("companyList", companyList);
			return "redirect:/home/employer/listPost";
		}
	}

	@RequestMapping(value = "/savePost", method = RequestMethod.POST)
	public String savePost(@ModelAttribute("post") JobPost jobPostModel, HttpServletRequest request,
			Principal principal, Model model) {
		String userName = principal.getName();
		User user = homeService.getUserDetailByUserName(userName);

		List<Company> companyList = companyService.getAllCompanyExceptDeleted();
		List<Category> categoryList = categoryService.getAllNotDeletedCategory();
		if (jobPostModel.getCompany() == null) {
			request.getSession().setAttribute("jobPost", jobPostModel);
			return "redirect:/masterdata/company/newCompany?fromJobPost=true";
		}
		
		String errorMessage = employerService.savePost(jobPostModel, user.getId());

		if (errorMessage != null) {
			model.addAttribute("errorMessage", errorMessage);
			model.addAttribute("categoryList", categoryList);
			model.addAttribute("companyList", companyList);
			model.addAttribute("post", jobPostModel);
			return "newPost";
		} else {
			employerService.savePost(jobPostModel, user.getId());
			List<JobPost> postList = user.getPostList();
			model.addAttribute("postList", postList);
			model.addAttribute("post", jobPostModel);
			request.getSession().removeAttribute("jobPost");
		}
		return "redirect:/home/employer/listPost";
	}

	@RequestMapping(value = "/deletePost", method = RequestMethod.GET)
	public String deletePost(@RequestParam("postId") Long postId, Model model) {
		List<JobPost> postList = employerService.deletePost(postId);
		model.addAttribute("postList", postList);
		return "redirect:/home/employer/listPost";
	}

	@RequestMapping(value = "/searchPost", method = RequestMethod.POST)
	public String searchPost(@RequestParam("name") String nameValue,@RequestParam("category") Long categoryIdValue,@RequestParam("type") String typeValue, HttpServletRequest request, Model model) {
		List<Category> categoryList = categoryService.getAllCategory();
		List<JobPost> postList1 = employerService.searchByCategoryTypeName(nameValue, categoryIdValue, typeValue);
		Category categoryValue = categoryService.getCategoryById(categoryIdValue);
		model.addAttribute("postList1", postList1);
		model.addAttribute("nameValue", nameValue);
		model.addAttribute("categoryValue", categoryValue);
		model.addAttribute("typeValue", typeValue);
		model.addAttribute("categoryList", categoryList);

		return "listPost";
	}

	@RequestMapping(value = "/searchYourPost", method = RequestMethod.POST)
	public String searchYourPost(@RequestParam("search") String searchValue, HttpServletRequest request, Model model,
			Principal principal) {
		String userName = principal.getName();

		List<JobPost> postList = employerService.searchJobOfUserByName(searchValue, userName);
		model.addAttribute("postList", postList);
		model.addAttribute("searchValue", searchValue);

		return "listPost";
	}

	@RequestMapping(value = "/listResume", method = RequestMethod.GET)
	public ModelAndView getAllResume() {
//		List<Resume> resumeList = resumeService.getAllResumes();
		ModelAndView model = new ModelAndView();
		model.setViewName("listResume");
//		model.addObject("resumeList", resumeList);
		return model;
	}

	@RequestMapping(value = "/searchResume", method = RequestMethod.POST)
	public String searchResume(@RequestParam("search") String searchValue, HttpServletRequest request, Model model) {

//		List<Resume> resumeList = resumeService.searchByName(searchValue);
//		model.addAttribute("resumeList", resumeList);
		model.addAttribute("searchValue", searchValue);

		return "listResume";
	}

	@RequestMapping(value = "/viewResumeDetails", method = RequestMethod.GET)
	public ModelAndView getResumeDetailsView(@RequestParam("resumeId") Long resumeId) {
//		Resume resume = resumeService.getResumeDetail(resumeId);
		ModelAndView model = new ModelAndView();
		model.setViewName("resume-detail");
//		model.addObject("resume", resume);
		return model;
	}

	@RequestMapping(value = "/listCandidates", method = RequestMethod.GET)
	public ModelAndView getAllCandidates(Principal principal) {
		String userName = principal.getName();
		User user = homeService.getUserDetailByUserName(userName);
		List<JobPost> postList = user.getPostList();
		List<JobApply> applyList1 = jobApplyService.getAllApply();
		List<JobApply> applyList = new ArrayList<JobApply>();
		for (JobApply jobApply : applyList1) {
			for (JobPost post : postList) {
				if (jobApply.getPost().getId() == post.getId()) {
					applyList.add(jobApply);
				}
			}
		}
		ModelAndView model = new ModelAndView();
		model.setViewName("listCandidates");
		model.addObject("applyList", applyList);
		return model;
	}
	@RequestMapping(value = "/listEmployee", method = RequestMethod.GET)
	public ModelAndView getAllEmployee() {
		List<JobApply> applyList = jobApplyService.getAllApplyExceptDeleted();

		ModelAndView model = new ModelAndView();
		model.setViewName("listCandidates");
		model.addObject("applyList", applyList);
		return model;
	}

	@RequestMapping(value = "/accept", method = RequestMethod.GET)
	public String acceptApply(@RequestParam("id") Long id, Model model, Principal principal) {
		String userName = principal.getName();
		User user = homeService.getUserDetailByUserName(userName);

		JobApply apply = jobApplyService.setStatus(id, "ACCEPTED");
		JobPost post = employerService.getJobPostDetail(apply.getPost().getId());
		Long accepted = jobApplyService.countAccept(apply.getPost().getId());
		Long vacancy = apply.getPost().getVacancy();
		if (accepted != null) {
			vacancy = vacancy - accepted;
			post.setVacancy(vacancy);
			employerService.savePost(post, user.getId());
		}
		String errorMessage = employerService.savePost(post, user.getId());
		model.addAttribute("apply", apply);
		model.addAttribute("errorMessage", errorMessage);
		return "redirect:/home/employer/listCandidates";
	}

	@RequestMapping(value = "/deny", method = RequestMethod.GET)
	public String denyApply(@RequestParam("id") Long id, Model model) {
		JobApply apply = jobApplyService.setStatus(id, "DENIED");
		model.addAttribute("apply", apply);
		return "redirect:/home/employer/listCandidates";
	}

	@RequestMapping(value = "/searchCandidate", method = RequestMethod.POST)
	public String searchCandidate(@RequestParam("search") String searchValue, HttpServletRequest request, Model model,
			Principal principal) {
		String userName = principal.getName();
		List<JobApply> applyList = jobApplyService.searchByName(searchValue, userName);
		model.addAttribute("applyList", applyList);
		model.addAttribute("searchValue", searchValue);

		return "listCandidates";
	}

	@RequestMapping(value = "/viewAppliedDetails", method = RequestMethod.GET)
	public ModelAndView getAppliedDetailsView(@RequestParam("id") Long id) {
		JobApply apply = jobApplyService.getAppliedDetails(id);
		ModelAndView model = new ModelAndView();
		model.setViewName("viewAppliedDetails");
		model.addObject("apply", apply);
		return model;
	}

}
