package com.jobhunting.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.jobhunting.entity.Category;
import com.jobhunting.services.CategoryService;

@Controller
@RequestMapping(value = "/admin/category")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;

	@RequestMapping(value = "/newCategory", method = RequestMethod.GET)
	public ModelAndView getnewCategoryView() {
		Category category = new Category();

		ModelAndView model = new ModelAndView();
		model.setViewName("newCategory");
		model.addObject("category", category);
		return model;
	}

	@RequestMapping(value = "/saveCategory", method = RequestMethod.POST)
	public String saveUser(@ModelAttribute("category") Category categoryModel, Model model) {
		String errorMessage = categoryService.saveCategory(categoryModel);
		List<Category> categoryList = categoryService.getAllNotDeletedCategory();

		if (errorMessage != null) {
			model.addAttribute("errorMessage", errorMessage);
			return "newCategory";
		} else {
			categoryService.saveCategory(categoryModel);
			model.addAttribute("categoryList", categoryList);
			model.addAttribute("category", categoryModel);
			return "listCategory";
		}
	}

	@RequestMapping(value = "/listCategory", method = RequestMethod.GET)
	public ModelAndView getListCategoryView() {
		List<Category> categoryList = categoryService.getAllNotDeletedCategory();
		ModelAndView model = new ModelAndView();
		model.setViewName("listCategory");
		model.addObject("categoryList", categoryList);
		return model;
	}

	@RequestMapping(value = "/editCategory", method = RequestMethod.GET)
	public ModelAndView getEditUserView(@RequestParam("catId") Long catId) {
		Category category = categoryService.getCategoryById(catId);

		ModelAndView model = new ModelAndView();
		model.setViewName("editCategory");
		model.addObject("category", category);
		return model;
	}

	@RequestMapping(value = "/updateCategory", method = RequestMethod.POST)
	public String updateUser(@ModelAttribute("category") Category categoryModel, Model model) {
		String errorMessage = categoryService.updateCategory(categoryModel);
		List<Category> categoryList = categoryService.getAllNotDeletedCategory();

		if (errorMessage != null) {
			model.addAttribute("errorMessage", errorMessage);
			return "editCategory";
		} else {
		model.addAttribute("categoryList", categoryList);
		model.addAttribute("category", categoryModel);
		return "listCategory";
		}
	}

	@RequestMapping(value = "/deleteCategory", method = RequestMethod.GET)
	public ModelAndView deleteCategory(@RequestParam("id") Long id) {
		List<Category> categoryList = categoryService.deleteCategory(id);
		ModelAndView model = new ModelAndView();
		model.setViewName("listCategory");
		model.addObject("categoryList", categoryList);
		return model;
	}

	@RequestMapping(value = "/searchCategory", method = RequestMethod.POST)
	public String searchCompany(@RequestParam("search") String searchValue, Model model) {
		List<Category> categoryList = categoryService.searchCategory(searchValue);
		model.addAttribute("categoryList", categoryList);
		model.addAttribute("searchValue", searchValue);

		return "listCategory";
	}

}
