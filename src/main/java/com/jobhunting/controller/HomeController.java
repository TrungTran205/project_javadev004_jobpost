package com.jobhunting.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.jobhunting.entity.Category;
import com.jobhunting.entity.JobApply;
import com.jobhunting.entity.JobPost;
import com.jobhunting.entity.User;
import com.jobhunting.model.JobApplyModel;
import com.jobhunting.model.PasswordChangeModel;
import com.jobhunting.services.CategoryService;
import com.jobhunting.services.EmployerService;
import com.jobhunting.services.HomeService;
import com.jobhunting.services.ImageService;
import com.jobhunting.services.JobApplyService;
import com.jobhunting.services.UserService;

@Controller
@RequestMapping(value = "/home")
public class HomeController {

	// Example for new View of User
	@Autowired
	private HomeService homeService;

	@Autowired
	private UserService userService;

	@Autowired
	private EmployerService employerService;

	@Autowired
	private JobApplyService jobApplyService;

	@Autowired
	private ImageService imageService;
	
	@Autowired
	private CategoryService categoryService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView getHomeView(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
		ModelAndView model = new ModelAndView();
		
		List<User> userList = homeService.getAllUsers();
		List<JobPost> postList = employerService.getAllPostsNotDeleted();
		
		if(request.getRemoteUser()!=null){
			String username = request.getUserPrincipal().getName();
			List<JobPost> postLikeList = userService.getPostLikeListByUser(username);
			request.setAttribute("postLikeList", postLikeList);
		}
		
		List<Category> categoryList = categoryService.getAllCategory();
		List<Category> categoryList1 = categoryService.getCategoriesAndVacancy();
		
		request.setAttribute("categoryList", categoryList);
		request.setAttribute("categoryList1", categoryList1);
		
		
		model.setViewName("user-home");
		model.addObject("userList", userList);
		model.addObject("postList", postList);

		
		return model;
	}

	@RequestMapping(value = "/candidates", method = RequestMethod.GET)
	public ModelAndView getCandidatesListView() {
		ModelAndView model = new ModelAndView();
		List<User> candidateList = homeService.getAllCandidates();
		model.setViewName("candidatesList");
		model.addObject("candidateList", candidateList);
		return model;
	}

	@RequestMapping(value = "/jobs", method = RequestMethod.GET)
	public ModelAndView getJobPostListView() {
		ModelAndView model = new ModelAndView();
		List<JobPost> postList1 = employerService.getAllPostsNotDeleted();
		List<Category> categoryList = categoryService.getAllCategory();
		model.setViewName("listPost");
		model.addObject("postList1", postList1);
		model.addObject("categoryList", categoryList);
		return model;
	}

	@RequestMapping(value = "/category", method = RequestMethod.GET)
	public ModelAndView getCategoryView(@RequestParam("catId") Long catId) {
		List<JobPost> postList = categoryService.getJobPosts(catId);
//		List<JobPost> postList = categoryService.getJobPostsNotExpired(catId);
		Category category = categoryService.getCategoryById(catId);
		ModelAndView model = new ModelAndView();
		model.setViewName("category");
		model.addObject("postList", postList);
		model.addObject("category", category);
		return model;
	}

	@RequestMapping(value = "/searchPost", method = RequestMethod.POST)
	public String searchPost(@RequestParam("name") String nameValue, @RequestParam("category") Long categoryIdValue,
			@RequestParam("type") String typeValue, HttpServletRequest request, Model model) {
		List<Category> categoryList = categoryService.getAllCategory();
		List<JobPost> postList1 = employerService.searchByCategoryTypeName(nameValue, categoryIdValue, typeValue);
		Category categoryValue = categoryService.getCategoryById(categoryIdValue);
		model.addAttribute("postList1", postList1);
		model.addAttribute("nameValue", nameValue);
		model.addAttribute("categoryValue", categoryValue);
		model.addAttribute("typeValue", typeValue);
		model.addAttribute("categoryList", categoryList);

		return "listPost";
	}

	@RequestMapping(value = "/searchResume", method = RequestMethod.POST)
	public String searchResume(@RequestParam("search") String searchValue, HttpServletRequest request, Model model) {
		model.addAttribute("searchValue", searchValue);
		request.getSession().setAttribute("a", searchValue);
		return "listResume";
	}
	
	@RequestMapping(value = "/searchJobApplied", method = RequestMethod.POST)
	public String searchJobApplied(@RequestParam("search") String searchValue, HttpServletRequest request, Model model, Principal principal) {
//		List<JobApply> applyList = jobApplyService.getAllApplyExceptDeletedByUser(user.getId());
		String userName=principal.getName();
		List<JobApply> applyList = jobApplyService.searchByNameJob(searchValue, userName);
		model.addAttribute("applyList",applyList);
		model.addAttribute("searchValue", searchValue);
		

		return "listJobApplied";
	}

	@RequestMapping(value = "/jobdetail", method = RequestMethod.GET)
	public ModelAndView getJobDetailView(@RequestParam("postId") Long postId) {
		JobPost post = employerService.getJobPostDetail(postId);
		ModelAndView model = new ModelAndView();
		model.setViewName("job-detail");
		model.addObject("post", post);
		return model;
	}

	@RequestMapping(value = "/resumedetail", method = RequestMethod.GET)
	public ModelAndView getResumeDetailView(Principal principal) {
		String userName = principal.getName();
		User user = homeService.getUserDetailByUserName(userName);
		ModelAndView model = new ModelAndView();
		model.setViewName("resume-detail");
		model.addObject("user", user);
		return model;
	}

	@RequestMapping(value = "/resumeDetailList", method = RequestMethod.GET)
	public ModelAndView getResumeDetailView() {
		ModelAndView model = new ModelAndView();
		List<User> userList = homeService.getAllCandidates();
		model.setViewName("resume-detail");
		model.addObject("userList", userList);
		return model;
	}

	@RequestMapping(value = "/applyjob", method = RequestMethod.GET)
	public ModelAndView getApplyJobView(@RequestParam("postId") Long postId) {
		JobPost post = employerService.getJobPostDetail(postId);
		JobApplyModel jobApplyModel = new JobApplyModel();
		ModelAndView model = new ModelAndView();
		model.setViewName("apply-job");
		model.addObject("post", post);
		model.addObject("jobApplyModel", jobApplyModel);
		return model;
	}

	@RequestMapping(value = "/applyjob", method = RequestMethod.POST)
	public ModelAndView applyJob(@ModelAttribute(" jobApply") JobApplyModel jobApply, HttpServletRequest request,
			Principal principal) {
		ModelAndView model = new ModelAndView();
		String userName = principal.getName();
		User user = homeService.getUserDetailByUserName(userName);
		String uploadPath = request.getServletContext().getRealPath("uploads");
		String imageName = imageService.uploadFile(uploadPath, jobApply.getCvImageFile());
		if (imageName != null && !imageName.isEmpty()) {
			jobApply.setFileName(imageName);
		}
		try{
			JobApply apply = jobApplyService.saveApply(jobApply, user.getId());
			model.addObject("apply", apply);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		model.setViewName("apply-job-success");
		
		return model;
	}

	@RequestMapping(value = "/employer", method = RequestMethod.GET)
	public ModelAndView getEmployerView() {
		List<JobPost> postList = employerService.getAllPosts();
		List<JobApply> applyList = jobApplyService.getAllApply();
		ModelAndView model = new ModelAndView();
		model.setViewName("employer-home");
		model.addObject("postList", postList);
		model.addObject("applyList", applyList);
		return model;
	}

	@RequestMapping(value = "/employee", method = RequestMethod.GET)
	public ModelAndView getEmployeeView() {
		List<JobPost> postList = employerService.getAllPosts();
		ModelAndView model = new ModelAndView();
		model.setViewName("employee-home");
		model.addObject("postList", postList);
		return model;
	}

	@RequestMapping(value = "/listJobApplied", method = RequestMethod.GET)
	public ModelAndView getListJobApplied(Principal principal) {
		String userName = principal.getName();
		User user = homeService.getUserDetailByUserName(userName);
		List<JobApply> applyList = jobApplyService.getAllApplyExceptDeletedByUser(user.getId());
//		long userId = user.getId();
		ModelAndView model = new ModelAndView();
		model.setViewName("listJobApplied");
		model.addObject("applyList", applyList);
		model.addObject("userName", userName);
		return model;
	}

	@RequestMapping(value = "/deleteApply", method = RequestMethod.GET)
	public ModelAndView deleteApply(@RequestParam("id") Long id,Principal principal) {
		String userName = principal.getName();
		User user = homeService.getUserDetailByUserName(userName);
		List<JobApply> applyList = jobApplyService.deleteApplyByCandidate(id,user.getId());
		ModelAndView model = new ModelAndView();
		model.setViewName("listJobApplied");
		model.addObject("applyList", applyList);
		return model;
	}

	@RequestMapping(value = "/viewJobDetails", method = RequestMethod.GET)
	public ModelAndView getJobDetailsView(@RequestParam("postId") Long postId) {
		JobPost post = employerService.getJobPostDetail(postId);
		ModelAndView model = new ModelAndView();
		model.setViewName("viewJobDetails");
		model.addObject("post", post);
		return model;
	}

	//Them, xoa User trong danh sach like job trong post
	@RequestMapping(value = "/likeJobPost", method = RequestMethod.GET)
	public ModelAndView likeJobPost(@RequestParam("postId") Long postId, Principal principal,
			HttpServletRequest request,Model model1) {
		String userName = principal.getName();
		User candidate = userService.getByUserName(userName);
		JobPost post = employerService.getJobPostDetail(postId);
		
		//0.Lay ra danh sach User like Job trong post vua tim bang Id
		List<User> userLikeJobList = post.getLikeJobByList();
		
		try{
			//1. Kiem tra danh sach nay cho truong hop job chua co User Like thi goi ham like
			if(userLikeJobList==null||userLikeJobList.isEmpty()){
				String result = employerService.likeJobPost(postId, userName);
				System.out.println(result+" Like Job List is null or empty");
			}else if(userLikeJobList!=null){
				System.out.println("Kiem tra list co candidate ko de dislike");
				//2.Truong hop job da co User Like thi kiem tra voi principal
				if(userLikeJobList.contains(candidate)){
					String result = employerService.dislikeJobPost(postId, userName);
					System.out.println(result);
				}else if(userLikeJobList.contains(candidate)==false){
					String result = employerService.likeJobPost(postId, userName);
					System.out.println(result + " different userName");	
				}	
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		//Nguyen nhan la do lenh if nay da chay uu tien va cho phep like ca nhung job da co user
		
		
		//List<JobPost> postList = employerService.getAllPosts();
		List<JobPost> postList = employerService.getAllPostsNotDeleted();
		
		List<JobPost> postLikeList = userService.getPostLikeListByUser(userName);
		
		List<Category> categoryList = categoryService.getCategoriesAndVacancy();
		request.setAttribute("categoryList", categoryList);
		
		ModelAndView model = new ModelAndView();
		model.setViewName("user-home");
		model.addObject("post", post);
		model.addObject("postLikeList", postLikeList);
		
		model.addObject("postList", postList);
		return model;
	}

	@RequestMapping(value = "/jobUserLiked", method = RequestMethod.GET)
	public ModelAndView getJobLikedView(Principal principal) {
		String userName = principal.getName();
		//Doi postList moi
		List<JobPost> postList = employerService.getAllSavedPost(userName);

		ModelAndView model = new ModelAndView();
		model.setViewName("user-job-liked");
		model.addObject("postList", postList);
		return model;
	}

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView getUserProfileView(Principal principal) {
		String userName = principal.getName();
		User user = homeService.getUserDetailByUserName(userName);
		ModelAndView model = new ModelAndView();
		model.setViewName("user-profile");
		model.addObject("user", user);
		return model;
	}

	@RequestMapping(value = "/editProfile", method = RequestMethod.GET)
	public ModelAndView getEditUserView(Principal principal) {
		String userName = principal.getName();
		User user = homeService.getUserDetailByUserName(userName);
		ModelAndView model = new ModelAndView();
		model.setViewName("editProfile");
		model.addObject("user", user);
		return model;
	}

	@RequestMapping(value = "/updateProfile", method = RequestMethod.POST)
	public String updateUser(@ModelAttribute("user") User userModel, HttpServletRequest request, Model model) {
		String uploadPath = request.getServletContext().getRealPath("uploads");
		String imageName = imageService.uploadFile(uploadPath, userModel.getImageFile());
		if (imageName != null && !imageName.isEmpty()) {
			userModel.setImageName(imageName);
		}
		String errorMessage = homeService.updateUser(userModel);

		if (errorMessage != null) {
			model.addAttribute("user", userModel);
			model.addAttribute("errorMessage", errorMessage);
			return "editProfile";
		}
		homeService.updateUser(userModel);
		// Save thanh cong & chuyen sang trang List User
		model.addAttribute("user", userModel);

		return "user-profile";
	}

	@RequestMapping(value = "/profile/changePassword", method = RequestMethod.GET)
	public ModelAndView getChangePasswordView() {
		PasswordChangeModel changePasswordModel = new PasswordChangeModel();
		ModelAndView model = new ModelAndView();
		model.setViewName("user-changepassword");
		model.addObject("changePasswordModel", changePasswordModel);
		return model;
	}

	@RequestMapping(value = "/profile/changePassword", method = RequestMethod.POST)
	public String changePasswordView(@ModelAttribute("changePasswordModel") PasswordChangeModel changePasswordModel,
			Principal principal, Model model) {
		String userName = principal.getName();
		String error = this.userService.changePassword(userName, changePasswordModel);
		if (error != null) {
			model.addAttribute("errorMessage", error);
			return "user-changepassword";
		}
		return "redirect:/home/profile";
	}
}
