package com.jobhunting.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jobhunting.entity.JobApply;
import com.jobhunting.entity.JobPost;
import com.jobhunting.entity.User;
import com.jobhunting.model.JobApplyModel;
import com.jobhunting.repository.JobApplyRepository;
import com.jobhunting.repository.JobPostRepository;
import com.jobhunting.repository.UserRepository;

@Service
public class JobApplyService {

	@Autowired
	private JobApplyRepository jobApplyRepository;

	@Autowired
	private JobPostRepository jobPostRepository;

	@Autowired
	private UserRepository userRepository;

	public JobApply getAppliedDetails(Long id) {
		Optional<JobApply> jobApplied = jobApplyRepository.findById(id);
		JobApply ja = jobApplied.get();
		return ja;
	}

	public JobApply saveApply(JobApplyModel jobApply, Long userId) {
		JobApply apply = new JobApply();
		Optional<JobPost> post = jobPostRepository.findById(jobApply.getJobPostId());

		Optional<User> user = userRepository.findById(userId);
		List<User> userList = new ArrayList<User>();
		userList.add(user.get());

		apply.setUserList(userList);
		apply.setPost(post.get());
		apply.setEmail(jobApply.getEmail());
		apply.setName(jobApply.getName());
		apply.setFileName(jobApply.getFileName());
		return jobApplyRepository.save(apply);
	}

	public String checkErrors(JobApplyModel jobApply) {

		if (jobApply.getName() == null || jobApply.getName().isEmpty()) {
			return "Name should be filled";
		}

		if (jobApply.getEmail() == null || jobApply.getEmail().isEmpty()) {
			return "Email should be filled";
		}

		if (jobApply.getCvImageFile() == null || jobApply.getCvImageFile().isEmpty()) {
			return "File should be submit";
		}

		return null;
	}

	public List<JobApply> deleteApply(Long id) {
		jobApplyRepository.deleteById(id);
		return jobApplyRepository.findAll();
	}
	
	public List<JobApply> deleteApplyByCandidate(Long id,Long userId) {
		jobApplyRepository.deleteById(id);
		return jobApplyRepository.getAllApplyExceptDeletedByUser(userId);
	}

	public List<JobApply> getAllApply() {
		return jobApplyRepository.findAll();
	}

	public List<JobApply> getAllApplyExceptDeleted () {
		return jobApplyRepository.getAllApplyExceptDeleted();
	}
	
	public List<JobApply> getAllApplyExceptDeletedByUser (Long userId) {
		return jobApplyRepository.getAllApplyExceptDeletedByUser(userId);
	}

	public JobApply setStatus(Long id, String status) {
		Optional<JobApply> jobApplied = jobApplyRepository.findById(id);
		JobApply ja = jobApplied.get();
		ja.setStatus(status);
		jobApplyRepository.save(ja);
		return ja;
	}

	public Long countAccept(Long postId) {
		return jobApplyRepository.countAccept(postId);
	}

	public List<JobApply> searchByName(String searchValue, String userName) {
		return jobApplyRepository.searchByName(searchValue, userName);
	}

	public Long getCountAllApplyExceptDeletedByUser(Long id) {
		return jobApplyRepository.getCountAllApplyExceptDeletedByUser(id);
	}

	public List<JobApply> searchByNameJob(String searchValue, String userName) {
		return jobApplyRepository.searchByNameJob(searchValue, userName);
	}

}
