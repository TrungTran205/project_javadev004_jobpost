package com.jobhunting.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jobhunting.entity.Category;
import com.jobhunting.entity.JobPost;
import com.jobhunting.repository.CategoryRepository;

@Service
public class CategoryService {
	@Autowired
	private CategoryRepository categoryRepository;
	
	public List<Category> getAllCategory() {
		List<Category> categoryList = categoryRepository.findAll();
		return categoryList;
	}
	
	public List<Category> getAllNotDeletedCategory() {
		List<Category> categoryList = categoryRepository.getAllNotDeleted();
		return categoryList;
	}
	
	public List<Category> getCategoriesAndVacancy() {
		List<Category> categoryList = categoryRepository.getCategoriesAndVacancy();
		for (Category category : categoryList) {
			List<JobPost> postList = category.getPostList();
			Integer count = 0;
			for (JobPost post : postList) {
				if (post.getVacancy() > 0 && post.getDeleted() == false) {
					count++;
				}
			}
			category.setAvailableJobCount(count);
		}
		return categoryList;
	}
	
	public Category getCategoryById(Long catId) {
		Optional<Category> cat = categoryRepository.findById(catId);
		Category category = cat.get();
		return category;
	}
	
	public List<JobPost> getJobPosts(Long catId) {
		Optional<Category> cat = categoryRepository.findById(catId);
		Category category = cat.get();
		List<JobPost> postList = category.getPostList();
		return postList;
	}
	
	public List<JobPost> getJobPostsNotExpired(Long catId) {
		Optional<Category> cat = categoryRepository.findById(catId);
		Category category = cat.get();
		List<JobPost> postList = category.getPostList();
		List<JobPost> postList1 = new ArrayList<>();
		for(JobPost post:postList){
			if(post.getExpired()==false){
				postList1.add(post);
			}
		}
		return postList1;
	}
	
	public String saveCategory(Category categoryModel) {
		if (categoryModel.getName() == null || categoryModel.getName().isEmpty()) {
			return "Category name should be filled";
		}
		categoryRepository.save(categoryModel);
		return null;
	}
	
	public String updateCategory (Category categoryModel) {
		Optional<Category> category = categoryRepository.findById(categoryModel.getId());
		Category categoryEntity = category.get();
		if (categoryModel.getName() == null || categoryModel.getName().isEmpty()) {
			return "Category name should be filled";
		}
		categoryEntity.setId(categoryModel.getId());
		categoryEntity.setName(categoryModel.getName());
		categoryRepository.save(categoryEntity);
		return null;
	}
	
	public List<Category> deleteCategory(Long id) {
		Optional<Category> category = categoryRepository.findById(id);
		Category cat = category.get();
		cat.setDeleted(true);
		categoryRepository.save(cat);
		return categoryRepository.getAllNotDeleted();
	}
	
	public List<Category> searchCategory(String name) {
		return categoryRepository.findCategoryByName(name);
	}
}
