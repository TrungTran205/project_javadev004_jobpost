package com.jobhunting.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jobhunting.entity.Category;
import com.jobhunting.entity.Company;
import com.jobhunting.entity.JobPost;
import com.jobhunting.entity.Permission;
import com.jobhunting.entity.User;
import com.jobhunting.entity.UserType;
import com.jobhunting.model.PasswordChangeModel;
import com.jobhunting.repository.CategoryRepository;
import com.jobhunting.repository.CompanyRepository;
import com.jobhunting.repository.JobPostRepository;
import com.jobhunting.repository.PermissionRepository;
import com.jobhunting.repository.UserRepository;
import com.jobhunting.repository.UserTypeRepository;

@Transactional
@Service
public class UserService {
	@Autowired
	private UserRepository userReposity;
	@Autowired
	private PermissionRepository permissionReposity;
	@Autowired
	private CompanyRepository companyRepository;
	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private UserTypeRepository userTypeRepository;
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	@Autowired
	private JobPostRepository jobPostRepository;
	
	private static final String DEFAULT_INITIAL_PASSWORD = "admin";

	public User getByUserName(String userName) {
		return userReposity.findByUserName(userName);
	}
	
	public List<Category> getAllCategory() {
		return categoryRepository.findAll();
	}
	
	public List<UserType> getAllType() {
		return userTypeRepository.findAll();
	}
	
	public List<Permission> getAllPermission() {
		return permissionReposity.findAll();
	}

	public void createDefaultAdmin() throws Exception {
// TODO create all groups and permissions
		String password = passwordEncoder.encode(DEFAULT_INITIAL_PASSWORD);
// creation of the super admin admin:admin)
		
		Permission permission = new Permission();
		permission.setName("ADMIN");
		permissionReposity.save(permission);
		
		List<Permission> permissionList = new ArrayList<Permission>();
		permissionList.add(permission);

		User user = new User();
		user.setUserName("admin");
		user.setPassword(password);
		user.setEmail("admin@greenacedamy.com");
		user.setFirstName("Administrator");
		user.setLastName("User");
		user.setIsActive(true);
		user.setPermissionList(permissionList);

//		List<User> userList = new ArrayList();
//		userList.add(user);
//		permission.setUserlist(userList);
		userReposity.save(user);
	}

	public void createDefaultCategory() throws Exception {
		Category category1 = new Category();
		category1.setName("Security");
		Category category2 = new Category();
		category2.setName("IT");
		Category category3 = new Category();
		category3.setName("Doctor");
		Category category4 = new Category();
		category4.setName("Engineer");
		Category category5 = new Category();
		category5.setName("Teacher");
		categoryRepository.save(category1);
		categoryRepository.save(category2);
		categoryRepository.save(category3);
		categoryRepository.save(category4);
		categoryRepository.save(category5);
	}

	public void createDefaultUserType() throws Exception {
		UserType type1 = new UserType();
		type1.setName("ADMIN");
		UserType type2 = new UserType();
		type2.setName("EMPLOYER");
		UserType type3 = new UserType();
		type3.setName("CANDIDATE");
		userTypeRepository.save(type1);
		userTypeRepository.save(type2);
		userTypeRepository.save(type3);
	}

	public void createDefaultPermission() throws Exception {
		Permission permission2 = new Permission();
		permission2.setName("EMPLOYER");
		Permission permission3 = new Permission();
		permission3.setName("CANDIDATE");
		permissionReposity.save(permission2);
		permissionReposity.save(permission3);
	}

	public String changePassword(String userName, PasswordChangeModel model) {
		if(model.getOldPassword().isEmpty()) {
			return "Old Password must not be empty";
		}
		if(model.getNewPassword1().isEmpty()) {
			return "New Password must not be empty";
		}
		if(model.getNewPassword2().isEmpty()) {
			return "Password Confirm must not be empty";
		}
		
		User user = userReposity.findByUserName(userName);
		if (user != null) {
			if (passwordEncoder.matches(model.getOldPassword(), user.getPassword())) {
				if (model.getNewPassword1().equals(model.getNewPassword2())) {
					String newEncodedPassword = passwordEncoder.encode(model.getNewPassword1());
					user.setPassword(newEncodedPassword);
					userReposity.save(user);
				} else {
					return "New Password is not matching";
				}

			} else {
				return "Old Password is not matching";
			}
		} else {
			return "User not found";
		}

		return null;
	}

	public String changePasswordForgot(String userName, PasswordChangeModel model) {
		if(model.getNewPassword1().isEmpty()) {
			return "New Password must not be empty";
		}
		if(model.getNewPassword2().isEmpty()) {
			return "Password Confirm must not be empty";
		}
		
		User user = userReposity.findByUserName(userName);
		if (user != null) {
			if (model.getNewPassword1().equals(model.getNewPassword2())) {
				String newEncodedPassword = passwordEncoder.encode(model.getNewPassword1());
				user.setPassword(newEncodedPassword);
				userReposity.save(user);
			} else {
				return "New Password is not matching";
			}
		} else {
			return "User not found";
		}
		return null;
	}
	
	public String checkUserInfo(String userName, String phoneNumber, String email) {
		User user = userReposity.findByUserNameEmailPhoneNumber(userName, email, phoneNumber);
		if(user==null){
			return "User not found";
		}	
		return null;
	}
	
	public String checkUserNamePassword(String userName, String password) {
		User user = userReposity.findByUserNamePassword(userName, password);
		if(user==null){
			return "Username or password is not correct";
		}	
		return null;
	}
	
	public List<JobPost> getPostLikeListByUser(String userName){
		return jobPostRepository.getPostLikeListByUser(userName);
	}

}
