package com.jobhunting.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jobhunting.entity.Permission;
import com.jobhunting.repository.PermissionRepository;

@Service
public class PermissionService {
	@Autowired
	private PermissionRepository permissionRepository;
	
	public List<Permission> getAllPermission(){
		return permissionRepository.findAll();
	}
	
	public Permission savePermission (String name) {
		Permission permissionEntity = new Permission();
		permissionEntity.setName(name);
		permissionRepository.save(permissionEntity);
		return permissionEntity;
	}
}
