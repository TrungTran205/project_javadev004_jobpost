package com.jobhunting.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jobhunting.entity.Company;
import com.jobhunting.repository.CompanyRepository;

@Service
public class CompanyService {
	@Autowired
	private CompanyRepository companyRepository;

	public List<Company> getAllCompany() {
		List<Company> companyList = companyRepository.findAll();
		return companyList;
	}

	public List<Company> getAllCompanyExceptDeleted() {
		List<Company> companyList = companyRepository.findAllCompanyExceptDeleted();
		return companyList;
	}

	public String saveCompany(Company companyModel) {

		if (companyModel.getName() == null || companyModel.getName().isEmpty()) {
			return "Company name should be filled";
		}

		if (companyModel.getInfo() == null || companyModel.getInfo().isEmpty()) {
			return "Company info should be filled";
		}

		if (companyModel.getLogoFile() == null || companyModel.getLogoFile().isEmpty()) {
			return "Company logo should be filled";
		}

		if (companyModel.getImageFile() == null || companyModel.getImageFile().isEmpty()) {
			return "Company image should be filled";
		}

		if (companyModel.getScale() == null || companyModel.getScale().isEmpty()) {
			return "Company scale should be filled";
		}

		if (companyModel.getWelfare() == null || companyModel.getWelfare().isEmpty()) {
			return "Company welfare should be filled";
		}

		if (companyModel.getAddress() == null || companyModel.getAddress().isEmpty()) {
			return "Company address should be filled";
		}

		if (companyModel.getEmail() == null || companyModel.getEmail().isEmpty()) {
			return "Company email should be filled";
		}

		companyRepository.save(companyModel);
		return null;
	}

	public String updateCompany(Company companyModel) {
		Optional<Company> company = companyRepository.findById(companyModel.getId());
		Company companyEntity = company.get();
		if (companyModel.getName() == null || companyModel.getName().isEmpty()) {
			return "Company name should be filled";
		}

		if (companyModel.getInfo() == null || companyModel.getInfo().isEmpty()) {
			return "Company info should be filled";
		}

		if (companyModel.getLogoFile() == null || companyModel.getLogoFile().isEmpty()) {
			return "Company logo should be filled";
		}

		if (companyModel.getImageFile() == null || companyModel.getImageFile().isEmpty()) {
			return "Company image should be filled";
		}

		if (companyModel.getScale() == null || companyModel.getScale().isEmpty()) {
			return "Company scale should be filled";
		}

		if (companyModel.getWelfare() == null || companyModel.getWelfare().isEmpty()) {
			return "Company welfare should be filled";
		}

		if (companyModel.getAddress() == null || companyModel.getAddress().isEmpty()) {
			return "Company address should be filled";
		}

		if (companyModel.getEmail() == null || companyModel.getEmail().isEmpty()) {
			return "Company email should be filled";
		}
		companyEntity.setId(companyModel.getId());
		companyEntity.setName(companyModel.getName());
		companyEntity.setInfo(companyModel.getInfo());
		companyEntity.setScale(companyModel.getScale());
		companyEntity.setWelfare(companyModel.getWelfare());
		companyEntity.setAddress(companyModel.getAddress());
		companyEntity.setEmail(companyModel.getEmail());
		companyRepository.save(companyEntity);
		return null;
	}

	public List<Company> deleteCompanyStatus(Long id) {
		Optional<Company> company = companyRepository.findById(id);
		Company deleteCompany = company.get();
		deleteCompany.setDeletedCompany(true);
		companyRepository.save(deleteCompany);
		return companyRepository.findAllCompanyExceptDeleted();
	}

	public Company findById(Long id) {
		Optional<Company> company = companyRepository.findById(id);
		return company.get();
	}

	public List<Company> findCompanyByCompanyName(String name) {
		List<Company> companyList = companyRepository.findCompanyByCompanyName(name);
		return companyList;
	}

	public List<Company> searchByNameExceptDeleted(String searchValue) {
		return companyRepository.searchByNameExceptDeleted(searchValue);
	}

	public Long countPost(Long companyId) {
		return companyRepository.countPost(companyId);
	}

	public List<Company> deleteCompany(Long id) {
		companyRepository.deleteById(id);
		return companyRepository.findAll();
	}
}
