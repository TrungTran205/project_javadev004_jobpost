package com.jobhunting.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.jobhunting.entity.Permission;
import com.jobhunting.entity.User;
import com.jobhunting.entity.UserType;
import com.jobhunting.repository.PermissionRepository;
import com.jobhunting.repository.UserRepository;
import com.jobhunting.repository.UserTypeRepository;

//Service la viet cho nhung logic cua Project
@Service
public class HomeService {
	// không cần new object
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserTypeRepository userTypeRepository;

	@Autowired
	private PermissionRepository permissionRepository;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

	public List<User> getAllCandidates() {
		return userRepository.findAllCandidates();
	}
	
	public User getUserDetail(Long userID) {
		Optional<User> user = userRepository.findById(userID);
		User u = user.get();
		return u;
	}

	public User getUserDetailByUserName(String userName) {
		User user = userRepository.findByUserName(userName);
		return user;
	}

	public List<User> searchByName(String name) {
		return userRepository.searchByName(name);
	}

	public String saveUser(User userModel) {
		if (userModel.getFirstName() == null || userModel.getFirstName().isEmpty()) {
			return "First Name must be filled";
		}
		if (userModel.getLastName() == null || userModel.getLastName().isEmpty()) {
			return "Last Name must be filled";
		}
		if (userModel.getUserName() == null || userModel.getUserName().isEmpty()) {
			return "Username must be filled";
		}
		if (userModel.getEmail() == null || userModel.getEmail().isEmpty()) {
			return "Email must be filled";
		}
		if (userModel.getPhoneNumber() == null || userModel.getPhoneNumber().isEmpty()) {
			return "Phone Number must be filled";
		}
		if (userModel.getUserType() == null) {
			return "Type must be chosen";
		}
		if (userModel.getPermissionList() == null || userModel.getPermissionList().isEmpty()) {
			return "Permission must be chosen";
		}

		List<Permission> permissionList = new ArrayList();
		Permission permission = permissionRepository.getByName("USER");
		if (permission == null) {
			permission = new Permission();
			permission.setName("USER");
		}

		String encodedPassword = passwordEncoder.encode("1234");
		userModel.setPassword(encodedPassword);
		userModel.setIsActive(true);
		userRepository.save(userModel);
		return null;
	}

	public String updateUser(User userModel) {
		Optional<User> user = userRepository.findById(userModel.getId());
		User userEntity = user.get();
			
		String pattern=".*[a-zA-Z].*";
		
		if(userModel.getFirstName() == null || userModel.getFirstName().isEmpty()) {
			return "First Name must be filled";
		}
		if(userModel.getLastName() == null || userModel.getLastName().isEmpty()) {
			return "Last Name must be filled";
		}
		if(userModel.getUserName() == null || userModel.getUserName().isEmpty()) {
			return "Username must be filled";
		}
		if(userModel.getEmail() == null || userModel.getEmail().isEmpty()) {
			return "Email must be filled";
		}
		if(userModel.getPhoneNumber() == null || userModel.getPhoneNumber().isEmpty()||Pattern.matches(pattern, userModel.getPhoneNumber())) {
			return "Phone Number must be filled";
		}
		if(userModel.getImageFile() == null || userModel.getImageFile().isEmpty()) {
			return "Profile image must be submitted";
		}
		if(userModel.getUserType() == null) {
			return "Type must be chosen";
		}
		if(userModel.getPermissionList() == null || userModel.getPermissionList().isEmpty()) {
			return "Permission must be chosen";
		}
		
		userEntity.setId(userModel.getId());
		userEntity.setFirstName(userModel.getFirstName());
		userEntity.setLastName(userModel.getLastName());
		userEntity.setUserName(userModel.getUserName());
		userEntity.setEmail(userModel.getEmail());
		userEntity.setPhoneNumber(userModel.getPhoneNumber());
		userEntity.setImageName(userModel.getImageName());
		
		if(userModel.getUserType() != null) {
			UserType userType = userModel.getUserType();
			userEntity.setUserType(userType);
		}
		
		if(userModel.getPermissionList() != null) {
			List<Permission> permissionList = userModel.getPermissionList();
			userEntity.setPermissionList(permissionList);
		}
		
		userRepository.save(userEntity);
		return null;
	}

	public List<User> deleteUser(Long id) {
		userRepository.deleteById(id);
		return userRepository.findAll();
	}

	public String registerEmployer(User userModel) {
		if (userModel.getFirstName() == null || userModel.getFirstName().isEmpty()) {
			return "First name must be filled";
		}

		if (userModel.getLastName() == null || userModel.getLastName().isEmpty()) {
			return "Last name must be filled";
		}
		
		if (userModel.getEmail() == null || userModel.getEmail().isEmpty()) {
			return "Email must be filled";
		}
		
		if (userModel.getUserName() == null || userModel.getUserName().isEmpty()) {
			return "User name must be filled";
		}

		if (userModel.getPassword() == null || userModel.getPassword().isEmpty()) {
			return "Password must be filled";
		}

		List<Permission> permissionList = new ArrayList();
		Permission permission = permissionRepository.getByName("EMPLOYER");
		if (permission == null) {
			permission = new Permission();
			permission.setName("EMPLOYER");
			permissionRepository.save(permission);
		} 
		permissionList.add(permission);
		
		UserType type = userTypeRepository.getByName("EMPLOYER");
		if (type == null) {
			type = new UserType();
			type.setName("EMPLOYER");
			userTypeRepository.save(type);
		} 

		String encodedPassword = passwordEncoder.encode(userModel.getPassword());
		userModel.setPassword(encodedPassword);
		userModel.setIsActive(true);
		userModel.setPermissionList(permissionList);
		userModel.setUserType(type);
		userRepository.save(userModel);

		return null;
	}
	
	public String registerCandidate(User userModel) {
		if (userModel.getFirstName() == null || userModel.getFirstName().isEmpty()) {
			return "First name must be filled";
		}

		if (userModel.getLastName() == null || userModel.getLastName().isEmpty()) {
			return "Last name must be filled";
		}
		
		if (userModel.getEmail() == null || userModel.getEmail().isEmpty()) {
			return "Email must be filled";
		}
		
		if (userModel.getUserName() == null || userModel.getUserName().isEmpty()) {
			return "User name must be filled";
		}

		if (userModel.getPassword() == null || userModel.getPassword().isEmpty()) {
			return "Password must be filled";
		}

		List<Permission> permissionList = new ArrayList();
		Permission permission = permissionRepository.getByName("CANDIDATE");
		if (permission == null) {
			permission = new Permission();
			permission.setName("CANDIDATE");
			permissionRepository.save(permission);
		} 
		permissionList.add(permission);
		
		UserType type = userTypeRepository.getByName("CANDIDATE");
		if (type == null) {
			type = new UserType();
			type.setName("CANDIDATE");
			userTypeRepository.save(type);
		} 

		String encodedPassword = passwordEncoder.encode(userModel.getPassword());
		userModel.setPassword(encodedPassword);
		userModel.setIsActive(true);
		userModel.setPermissionList(permissionList);
		userModel.setUserType(type);
		userRepository.save(userModel);

		return null;
	}


	public User getUserDetailByJobAppliedId(Long jobAppliedId) {
		return userRepository.getUserDetailByJobAppliedId(jobAppliedId);
	}

}
