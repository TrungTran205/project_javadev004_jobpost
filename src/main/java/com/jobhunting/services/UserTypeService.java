package com.jobhunting.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jobhunting.entity.UserType;
import com.jobhunting.repository.UserTypeRepository;

@Service
public class UserTypeService {

	@Autowired
	private UserTypeRepository userTypeRepository;
	
	public List<UserType> getAllUserType(){
		List<UserType> userTypeList = userTypeRepository.findAll();
		return userTypeList;
	}
	
	public UserType saveUserType (String name) {
		UserType userTypeEntity = new UserType();
		userTypeEntity.setName(name);
		userTypeRepository.save(userTypeEntity);
		return userTypeEntity;
	}
	
	public UserType updateUserType (Long id, String name) {
		Optional<UserType> type = userTypeRepository.findById(id);
		UserType userTypeEntity = type.get();
		userTypeEntity.setId(id);
		userTypeEntity.setName(name);
		userTypeRepository.save(userTypeEntity);
		return userTypeEntity;
	}
	
	public List<UserType> deleteUserType(Long id) {
		userTypeRepository.deleteById(id);
		return userTypeRepository.findAll();
	}
}
