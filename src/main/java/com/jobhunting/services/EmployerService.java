package com.jobhunting.services;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jobhunting.entity.Category;
import com.jobhunting.entity.Company;
import com.jobhunting.entity.JobPost;
import com.jobhunting.entity.Permission;
import com.jobhunting.entity.User;
import com.jobhunting.repository.CompanyRepository;
import com.jobhunting.repository.JobPostRepository;
import com.jobhunting.repository.UserRepository;

@Service
public class EmployerService {
	@Autowired
	private JobPostRepository jobPostRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private CompanyRepository companyRepository;

	public List<JobPost> getAllPosts() {
		return jobPostRepository.findAll();
	}
	
	public List<JobPost> getAllPostsExceptDeleted() {
		return jobPostRepository.getAllNotDeleted();
	}

	public List<JobPost> getAllPostsNotDeleted() {
		return jobPostRepository.getAllNotDeleted();
	}

	public List<JobPost> getAllPostsNotDeletedByUserName(String userName) {
		return jobPostRepository.getAllPostsNotDeletedByUserName(userName);
	}
	
	public JobPost getJobPostDetail(Long postId) {
		Optional<JobPost> post = jobPostRepository.findById(postId);
		JobPost jp = post.get();
		return jp;
	}

	public String savePost(JobPost jobPostModel, Long userId) {
		Optional<User> user = userRepository.findById(userId);
		long millis = System.currentTimeMillis();
		Date date = new Date(millis);

		if (jobPostModel.getNameJob() == null || jobPostModel.getNameJob().isEmpty()) {
			return "Job Name should be filled";
		}

		if (jobPostModel.getCategory() == null) {
			return "Category should be filled";
		}

		if (jobPostModel.getType() == null || jobPostModel.getType().isEmpty()) {
			return "Type should be filled";
		}

		if (jobPostModel.getVacancy() == null) {
			return "Vacancy should be filled";
		}

		if (jobPostModel.getDescription() == null || jobPostModel.getDescription().isEmpty()) {
			return "Job description should be filled";
		}

		if (jobPostModel.getRequirement() == null || jobPostModel.getRequirement().isEmpty()) {
			return "Job requirement should be filled";
		}

		if (jobPostModel.getSalary() == null) {
			return "Salary should be filled";
		}

		if (jobPostModel.getEndDay() == null) {
			return "Deadline should be filled";
		}

		Optional<Company> company = companyRepository.findById(jobPostModel.getCompany().getId());
		Company comp = company.get();
		if (comp == null) {
			return "Company empty";
		}
		jobPostModel.setCompany(comp);

		List<User> userList = new ArrayList<User>();
		userList.add(user.get());
		jobPostModel.setUserList(userList);
		jobPostModel.setPostDay(date);
		jobPostModel.setEndDay(jobPostModel.getEndDay());

		jobPostRepository.save(jobPostModel);
		return null;
	}

	public String updatePost(JobPost jobPostModel) {
		Optional<JobPost> post = jobPostRepository.findById(jobPostModel.getId());
		JobPost jpEntity = post.get();
		
		
		long millis=System.currentTimeMillis();  
		Date date=new Date(millis);
		
		if(jobPostModel.getNameJob()==null||jobPostModel.getNameJob().isEmpty()){
			return "Job Name should be filled";
		}
		
		if(jobPostModel.getCategory()==null){
			return "Category should be filled";
		}
		
		if(jobPostModel.getType()==null||jobPostModel.getType().isEmpty()){
			return "Type should be filled";
		}
		
		if(jobPostModel.getDescription()==null||jobPostModel.getDescription().isEmpty()){
			return "Job description should be filled";
		}
		
		if(jobPostModel.getRequirement()==null||jobPostModel.getRequirement().isEmpty()){
			return "Job requirement should be filled";	
		}
		
		if(jobPostModel.getSalary()==null){
			return "Salary should be filled";	
		}
		
		jpEntity.setPostDay(date);
		jpEntity.setId(jobPostModel.getId());
		jpEntity.setNameJob(jobPostModel.getNameJob());
		jpEntity.setType(jobPostModel.getType());
		jpEntity.setDescription(jobPostModel.getDescription());
		jpEntity.setRequirement(jobPostModel.getRequirement());
		jpEntity.setSalary(jobPostModel.getSalary());
		jpEntity.setEndDay(jobPostModel.getEndDay());

		if (jobPostModel.getCompany().getId() != null) {
			Optional<Company> company = companyRepository.findById(jobPostModel.getCompany().getId());
			Company comp = company.get();
			jpEntity.setCompany(comp);
		}

		if (jobPostModel.getCategory() != null) {
			Category category = jobPostModel.getCategory();
			jpEntity.setCategory(category);
		}

		jobPostRepository.save(jpEntity);
		return null;
	}

	public List<JobPost> deletePost(Long id) {
		Optional<JobPost> jobPost = jobPostRepository.findById(id);
		JobPost post = jobPost.get();
		post.setDeleted(true);
		jobPostRepository.save(post);
		return jobPostRepository.getAllNotDeleted();
	}

//	1 jobpost chi co duoc 1 user like
//	public JobPost likeJobPost(Long postId, String userName) {
//		User user = userRepository.findByUserName(userName);
//		Optional<JobPost> post = jobPostRepository.findById(postId);
//		JobPost jobPost = post.get();
//		jobPost.setLikeJobBy(user);
//		jobPostRepository.save(jobPost);
//		return jobPost;
//	}
	
	//Set 1 danh sach User like job vao trong Job Post
	public String likeJobPost(Long postId, String userName) {
		Optional<JobPost> post = jobPostRepository.findById(postId);
		JobPost jobPost = post.get();
		User candidate = userRepository.findByUserName(userName);

		List<User> userLikeList = jobPost.getLikeJobByList();
		userLikeList.add(candidate);
		jobPost.setLikeJobByList(userLikeList);
		
		String msg="";
		
		//Kiem tra danh sach moi them User co candidate ko, roi moi dua vao MySQL
		if(jobPost.getLikeJobByList().contains(candidate)){
			msg = "Successfully added";
			jobPostRepository.save(jobPost);
		}else{
			msg="Added failed";
		}
		
		return msg;
	}
	
//	public JobPost dislikeJobPost(Long postId) {
//		Optional<JobPost> post = jobPostRepository.findById(postId);
//		JobPost jobPost = post.get();
//		jobPost.setLikeJobBy(null);
//		jobPostRepository.save(jobPost);
//		return jobPost;
//	}
	
	//Xoa 1 danh sach User like job trong JopPost
	public String dislikeJobPost(Long postId,String userName) {
		User candidate = userRepository.findByUserName(userName);
		String msg="";
		
		Optional<JobPost> post = jobPostRepository.findById(postId);
		JobPost jobPost = post.get();
		
		List<User> userLikeList =jobPost.getLikeJobByList();
		int index=0;
		
		if(userLikeList.contains(candidate)){
			index=userLikeList.indexOf(candidate);
			userLikeList.remove(index);
			System.out.println("Found and removing user candidate from like job list");
		}
	
		if(jobPost.getLikeJobByList().contains(candidate)==false){
			msg = "Successfully removed";
			jobPost.setLikeJobByList(userLikeList);
			jobPostRepository.save(jobPost);
		}else{
			msg="Deleting user failed";
		}
		
		return msg;
	}

	public List<JobPost> getAllSavedPost(String userName) {
		User user = userRepository.findByUserName(userName);
		return user.getJobLikedByUserList();
	}

	public List<JobPost> searchByName(String searchValue) {
		return jobPostRepository.searchByName(searchValue);
	}
	
	public List<JobPost> searchJobOfUserByName(String searchValue, String creator) {
		return jobPostRepository.searchJobOfUserByName(searchValue, creator);
	}

	public List<JobPost> searchByCategoryTypeName(String nameValue, Long categoryIdValue, String typeValue) {
		return jobPostRepository.searchByCategoryTypeName(nameValue, categoryIdValue, typeValue);
	}
	
	public List<JobPost> searchByCandidate(Long userId) {
		return jobPostRepository.searchByCandidate(userId);
	}
}
