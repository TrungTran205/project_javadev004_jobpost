<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="icon" type="image/png" href="images/icons/favicon.ico" />

<link rel="stylesheet" type="text/css"
	href="https://colorlib.com/etc/lf/Login_v5/vendor/bootstrap/css/bootstrap.min.css">

<link rel="stylesheet" type="text/css"
	href="https://colorlib.com/etc/lf/Login_v5/fonts/font-awesome-4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" type="text/css"
	href="https://colorlib.com/etc/lf/Login_v5/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">

<link rel="stylesheet" type="text/css"
	href="https://colorlib.com/etc/lf/Login_v5/vendor/animate/animate.css">

<link rel="stylesheet" type="text/css"
	href="https://colorlib.com/etc/lf/Login_v5/vendor/css-hamburgers/hamburgers.min.css">

<link rel="stylesheet" type="text/css"
	href="https://colorlib.com/etc/lf/Login_v5/vendor/animsition/css/animsition.min.css">

<link rel="stylesheet" type="text/css"
	href="https://colorlib.com/etc/lf/Login_v5/vendor/select2/select2.min.css">

<link rel="stylesheet" type="text/css"
	href="https://colorlib.com/etc/lf/Login_v5/vendor/daterangepicker/daterangepicker.css">

<link rel="stylesheet" type="text/css"
	href="https://colorlib.com/etc/lf/Login_v5/css/util.css">
<link rel="stylesheet" type="text/css"
	href="https://colorlib.com/etc/lf/Login_v5/css/main.css">
<title>Confirm</title>
</head>
<body>
	<div class="limiter">
		<div class="container-login100"
			style="background-image: url('uploads/images/bg-01.jpg');">
			<div class="wrap-login100 p-l-110 p-r-110 p-t-62 p-b-33">
				<span class="login100-form-title p-b-53"> Register as </span>

				<div class="container-login100-form-btn m-t-17">
					<a class="login100-form-btn"
						href="${pageContext.request.contextPath}/register"> Candidate </a>
				</div>
				<div class="container-login100-form-btn m-t-17">
					<a class="login100-form-btn"
						href="${pageContext.request.contextPath}/registerEmployer"> Employer </a>
				</div>
				<div class="w-full text-center p-t-55">
					<span class="txt2"> Already have account? </span> <a
						href="${pageContext.request.contextPath}/login" class="txt2 bo1">
						Sign in </a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>