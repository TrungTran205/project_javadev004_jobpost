<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Details of Candidate</title>
</head>
<body>
	<div class="card">
		<h5 class="card-header">Information of Candidate</h5>
		<div class="card-body">
			<form:form enctype="multipart/form-data"
				modelAttribute="jobApplyModel" method="get">
				<h5 class="card-title">Name: ${apply.name}</h5>
				<p class="card-text">Email: ${apply.email }</p>
				<p class="card-text">Download CV:</p>
				<a href="<c:url value="/uploads/${apply.fileName }" />"
					class="btn btn-primary">${apply.fileName }</a>
			</form:form>
		</div>
	</div>
</body>
</html>