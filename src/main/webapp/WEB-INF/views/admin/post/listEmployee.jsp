<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List Candidates</title>
</head>
<body>
	<h2>List Candidates</h2>
	<form
		class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search pb-3"
		action="./searchCandidate" method="post">
		<div class="input-group">
			<input type="text" name="search" class="form-control"
				placeholder="Search" value="${searchValue}">
			<div class="input-group-append">
				<button class="btn btn-primary" type="submit">
					<i class="fas fa-search fa-sm"></i>
				</button>
			</div>
		</div>
	</form>
	<br>
	<table class="table">
		<thead class="thead-dark">
			<tr>
				<th>Id</th>
				<th>Name</th>
				<th>Email</th>
				<th>Apply Job</th>
				<th>Status</th>
			</tr>
		</thead>
		<c:forEach items="${applyList}" var="item">
			<tr>
				<td><a href="./viewUserDetails?jobAppliedId=${item.id}">${item.id }</a></td>
				<td>${item.name }</td>
				<td>${item.email}</td>
				<td><a href="./viewDetails?postId=${item.post.id}">${item.post.nameJob}</a></td>
				<c:if test="${empty item.status}">
					<td>PENDING</td>
				</c:if>
				<c:if test="${not empty item.status}">
					<td>${item.status }</td>
				</c:if>
			</tr>
		</c:forEach>
	</table>
</body>
</html>