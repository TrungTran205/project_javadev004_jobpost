<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>
</head>
<body>
	<h2>Home</h2>
	<h5>Recent Post:</h5>
	<table class="table">
		<thead class="thead-dark">
			<tr>
				<th>Id</th>
				<th>Job's Name</th>
				<th>Description</th>
				<th>Address</th>
				<th>Salary</th>
				<th>Email</th>
			</tr>
		</thead>
		<tbody>
				<tr>
					<td>${post.id }</td>
					<td>${post.nameJob }</td>
					<td>${post.description }</td>
					<td>${post.address }</td>
					<td>${post.salary }</td>
					<td>${post.email }</td>
				</tr>
		</tbody>
	</table>
	<a href="./newPost"> Create Post</a>
	<a href="./editPost?postId=${ postId }"> Edit Post</a>
	
	<a href="./listPost">Post List</a>
	
	<a href="./deletePost?postId=${ postId }">Delete Post</a>
</body>
</html>