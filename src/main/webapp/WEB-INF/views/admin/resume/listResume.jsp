<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List Resume</title>
</head>
<body>
	<h2>List Resume</h2>
	<form
		class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search pb-3"
		action="./searchResume" method="post">
		<div class="input-group">
			<input type="text" name="search" class="form-control"
				placeholder="Search" value="${searchValue}">
			<div class="input-group-append">
				<button class="btn btn-primary" type="submit">
					<i class="fas fa-search fa-sm"></i>
				</button>
			</div>
		</div>
	</form>
	<br>
	<table class="table">
		<thead class="thead-dark">
			<tr>
				<th>Id</th>
				<th>First name</th>
				<th>Last Name</th>
				<th>Email</th>
				<th>Phone</th>
				<th>Day of Birth</th>
				<th>Experiences</th>
				<th>Skills</th>
				<th>Creator</th>
				<sec:authorize access="hasRole('EMPLOYEE')">
					<th>Edit</th>
					<th>Delete</th>
				</sec:authorize>
			</tr>
		</thead>
		<c:forEach items="${resumeList}" var="item">
			<tr>
				<td><a href="./viewResumeDetails?resumeId=${item.id}">${item.id }</a></td>
				<td>${item.firstname }</td>
				<td>${item.lastname }</td>
				<td>${item.email}</td>
				<td>${item.phonenumber}</td>
				<td>${item.dob}</td>
				<td>${item.experiences}</td>
				<td>${item.skills}</td>
				<td>${item.creator }</td>
				<sec:authorize access="hasRole('EMPLOYEE')">
					<td><a href="./editResume?resumeId=${ item.id }"><i
							class="far fa-edit"></i></a></td>
					<td><a href="./deleteResume?resumeId=${ item.id }"><i
							class="fas fa-trash-alt"></i></a></td>
				</sec:authorize>
			</tr>
		</c:forEach>
	</table>
	<sec:authorize access="hasRole('EMPLOYEE')">
		<a class="btn btn-outline-dark" href="./newResume" role="button">
			Create Resume</a>
	</sec:authorize>
</body>
</html>