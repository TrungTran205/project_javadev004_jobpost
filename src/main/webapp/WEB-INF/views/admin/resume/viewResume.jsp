<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>VIEW RESUME</title>
</head>
<body>
	<h2>VIEW RESUME</h2>
	<h5>Your Resume:</h5>
	<table class="table">
		<thead class="thead-dark">
			<tr>
				<th>Id</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Email</th>
				<th>Phone</th>
				<th>Day of Birth</th>
				<th>Experiences</th>
				<th>Skills</th>
			</tr>
		</thead>
		<tbody>
				<tr>
					<td>${cv.id }</td>
					<td>${cv.firstname }</td>
					<td>${cv.lastname }</td>
					<td>${cv.email }</td>
					<td>${cv.phonenumber }</td>
					<td>${cv.dob }</td>
					<td>${cv.experiences }</td>
					<td>${cv.skills }</td>
				</tr>
		</tbody>
	</table>
	<a href="./editResume?resumeId=${ resumeId }"> Edit Resume</a>
</body>
</html>