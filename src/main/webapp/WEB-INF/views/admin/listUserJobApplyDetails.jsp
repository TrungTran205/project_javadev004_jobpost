<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List of Post</title>
</head>
<body>

	<h1>All Posts</h1>
	<form
		class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search pb-3"
		action="./searchPost" method="post">
		<div class="input-group">
			<input type="text" name="search" class="form-control" placeholder="Search" value="${searchValue}">
			<div class="input-group-append">
				<button class="btn btn-primary" type="submit">
					<i class="fas fa-search fa-sm"></i>
				</button>
			</div>
		</div>
	</form>
	<br>
	<table class="table">
		<thead class="thead-dark">
			<tr>
				<th>Id</th>
				<th>Job's Name</th>
				<th>Category</th>
				<th>Type</th>
				<th>Vacancy</th>
				<th>Salary</th>
				<th>Deleted Post</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${postList}" var="item">
				<tr>
					<td><a href="./viewDetails?postId=${item.id}">${item.id }</a></td>
					<td>${item.nameJob }</td>
					<th>${item.category.name }</th>
					<th>${item.type }</th>
					<th>${item.vacancy }</th>
					<td>${item.salary }</td>
					<td><c:if test="${item.deleted==true}">Deleted</c:if></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

</body>