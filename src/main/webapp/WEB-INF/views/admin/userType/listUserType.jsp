<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List User Type</title>
</head>
<body>
	<h2>List User Type</h2>
	<br>
	<table class="table">
		<thead class="thead-dark">
			<tr>
				<th>Id</th>
				<th>UserType</th>
			</tr>
		</thead>
		<c:forEach items="${ typeList }" var="item">
			<tr>
				<td>${ item.id }</td>
				<td>${ item.name }</td>
			</tr>
		</c:forEach>
	</table>
	<a class="btn btn-outline-dark" href="./newUserType" role="button">Create
		Type</a>
</body>
</html>