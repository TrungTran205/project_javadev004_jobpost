<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List User - Job Apply</title>
</head>
<body>
	<h2>List User - Job Apply</h2>
	<!-- Topbar Search -->
	<form
		class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search pb-3"
		action="./searchUser" method="post">
		<div class="input-group">
			<input type="text" name="search" class="form-control" placeholder="Search" value="${searchValue}">
			<div class="input-group-append">
				<button class="btn btn-primary" type="submit">
					<i class="fas fa-search fa-sm"></i>
				</button>
			</div>
		</div>
	</form>
	<br>
	<table class="table">
		<thead class="thead-dark">
			<tr>
				<th>Id</th>
				<th>Username</th>
				<th>View Job Apply</th>
				<sec:authorize access="hasRole('ADMIN')">
					<th>Edit</th>
					<th>Delete</th>
				</sec:authorize>
			</tr>
		</thead>
		
		<c:forEach items="${userList}" var="item" varStatus="state">
			<c:forEach items="${countList}" var="count" varStatus="status">
			<tr>
				<c:if test="${status.index==state.index}">
				<td>${item.id }</td>
				<td>${item.userName}</td>
				<td><a href="./listUserJobAppliedDetails?userId=${item.id}">${count}</a></td>
				<sec:authorize access="hasRole('ADMIN')">
					<td><a href="./edituser?id=${ item.id }"><i
							class="far fa-edit"></i></a></td>
					<td><a href="./deleteuser?id=${ item.id }"> <i
							class="fas fa-user-times"></i></a></td>
				</sec:authorize>
				</c:if>
			</tr>
			</c:forEach>
		</c:forEach>
	</table>
	<%-- <sec:authorize access="hasRole('ADMIN')">
		<a class="btn btn-outline-dark" href="./newuser" role="button">
			Create User</a>
	</sec:authorize> --%>

</body>
</html>