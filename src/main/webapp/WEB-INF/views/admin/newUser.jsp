<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create User</title>
</head>
<body>
	<h2>Create User</h2>
	<c:if test="${not empty errorMessage }">
		<div class="alert alert-danger" role="alert">${errorMessage}</div>
	</c:if>
	<p class="card-text">
		<form:form action="./saveuser" modelAttribute="user" method="post">
			<div class="form-group">
				<label for="firstName">First Name</label>
				<form:input type="text" class="form-control" path="firstName" />
			</div>

			<div class="form-group">
				<label for="lastName">Last Name</label>
				<form:input type="text" class="form-control" path="lastName" />
			</div>

			<div class="form-group">
				<label for="userName">Username</label>
				<form:input type="text" class="form-control" path="userName" />
			</div>

			<div class="form-group">
				<label for="email">Email</label>
				<form:input type="email" class="form-control" path="email" />
			</div>

			<div class="form-group">
				<label for="phoneNumber">Phone Number</label>
				<div class="input-group mb-2">
					<div class="input-group-prepend">
						<div class="input-group-text">(+84)</div>
					</div>
					<form:input type="text" class="form-control" path="phoneNumber" />
				</div>
			</div>

			<div class="dropdown">
				<label for="type">Select User Type:</label>
				<form:select path="userType.id">
					<c:forEach items="${ userTypeList }" var="type">
						<option value="${ type.id }">${ type.name }</option>
					</c:forEach>
				</form:select>
			</div>

			<div class="dropdown">
				<label for="permission">Select Permission:</label>
				<form:select multiple="true" path="permissionList">
					<c:forEach items="${ permissionList }" var="permission">
						<form:option value="${permission.id}">${permission.name}</form:option>
					</c:forEach>
				</form:select>
			</div>

			<button type="submit" class="btn btn-outline-dark">Create</button>
			<a href="./listuser" class="btn btn-outline-dark">Back</a>
		</form:form>

	</p>
</body>
</html>