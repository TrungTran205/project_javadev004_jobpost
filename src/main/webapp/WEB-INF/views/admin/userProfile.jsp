<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Profile</title>
</head>
<body>

	<div class="container" style="margin:10px auto">
	<h2 class="d-flex justify-content-center">User Profile</h2>
		<div class="row">
			<div class="col-sm">
				<div class="card" style="width: 500px;margin:0 auto">
					<img class="card-img-top"
						src="<c:url value="/uploads/${user.imageName }"/>"
						alt="Card image cap" width="100%" />
					<div class="card-body">
						<h5 class="card-title">${ user.firstName }${ user.lastName }</h5>
						<p class="card-text">
						<div class="row">
							<div class="col">Phone (+84):</div>
							<div class="col">${ user.phoneNumber }</div>
						</div>
						<div class="row">
							<div class="col">Email:</div>
							<div class="col">${ user.email }</div>
						</div>
					</div>
				</div>
				<div class="d-flex justify-content-center" style="margin-top:10px">
				<a href="./edituser?id=${ user.id }" class="btn btn-outline-dark"
					role="button">Update</a> <a href="./listuser"
					class="btn btn-outline-dark">Back</a>
				</div>
				
			</div>
		</div>
	</div>
</body>