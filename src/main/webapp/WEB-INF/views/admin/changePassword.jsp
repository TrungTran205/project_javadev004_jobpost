<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create User</title>
</head>
<body>
	<h2>Change Password</h2>
	<c:if test="${not empty errorMessage }">
		<div class="alert alert-danger" role="alert">${errorMessage}</div>
	</c:if>
	<p class="card-text">
		<form:form action="./changePassword" modelAttribute="changePasswordModel"
			method="post">

			<div class="form-group">
				<label for="oldPassword">Type your old password</label>
				<form:input type="password" class="form-control" path="oldPassword" />
			</div>

			<div class="form-group">
				<label for="newPassword1">Type your new password</label>
				<form:input type="password" class="form-control" path="newPassword1" />
			</div>

			<div class="form-group">
				<label for="newPassword2">Confirm your password</label>
				<form:input type="password" class="form-control" path="newPassword2" />
			</div>

			<button type="submit" class="btn btn-outline-dark">Submit</button>
		</form:form>

	</p>
</body>
</html>