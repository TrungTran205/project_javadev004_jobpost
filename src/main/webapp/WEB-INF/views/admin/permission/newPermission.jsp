<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create Permission</title>
</head>
<body>
	<h2>Create Permission</h2>
	<form action="./savePermission" method="post">
		<div class="form-group">
			<label for="name">Permission:</label> <input type="text"
				class="form-control" name="name" id="name">
		</div>
		<button type="submit" class="btn btn-outline-dark">Create</button>
		<a href="./listPermission" class="btn btn-outline-dark">Back</a>
	</form>
</body>
</html>