<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dashboard</title>
</head>
<body>
<sec:authentication property="principal.username" var="username" scope="session"/>
	<div class="jumbotron">
  <h1 class="display-4">Welcome to HRTool Dashboard!</h1>
  <p class="lead">This is the page for administration work of the webpage since you can manage information about employers, candidates and all users</p>
  <hr class="my-4">
  <p>The column on the left will show you more information that you need to manage, edit or delete.</p>
  <p class="lead">
    <a class="btn btn-primary btn-lg" href="${pageContext.request.contextPath}/home/" role="button">Home</a>
  </p>
</div>
</body>
</html>