<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Edit User</title>
</head>
<body>
	<div class="card" style="width: 500px;">
		<div class="card-body">
			<h5 class="card-title">Update User</h5>
			<c:if test="${not empty errorMessage }">
				<div class="alert alert-danger" role="alert">${errorMessage}</div>
			</c:if>
			<p class="card-text">
				<form:form enctype="multipart/form-data" action="./updateuser"
					modelAttribute="user" method="post">
					<form:input type="hidden" path="id" />
					<div class="form-group">
						<label for="firstName">First Name</label>
						<form:input type="text" class="form-control" path="firstName" />
					</div>

					<div class="form-group">
						<label for="lastName">Last Name</label>
						<form:input type="text" class="form-control" path="lastName" />
					</div>

					<div class="form-group">
						<label for="userName">Username</label>
						<form:input type="text" class="form-control" path="userName" />
					</div>

					<div class="form-group">
						<label for="email">Email</label>
						<form:input type="email" class="form-control" path="email" />
					</div>

					<div class="form-group">
						<label for="phoneNumber">Phone Number</label>
						<form:input type="text" class="form-control" path="phoneNumber"
							ng-maxlength="20" />
					</div>

					<div class="form-group">
						<label for="profileImage">Upload Profile Image</label> <img alt=""
							width="200" height="200"
							src="<c:url value="/uploads/${ user.imageName }" />" />
						<form:input type="file" path="imageFile" class="form-control-file"
							id="profileImage" />
					</div>

					<div class="dropdown">
						<label for="type">Select User Type:</label>
						<form:select path="userType.id">
							<c:forEach items="${ userTypeList }" var="type">
								<c:if test="${ user.userType.id == type.id }">
									<option selected="selected" value="${ type.id }">${ type.name }</option>
								</c:if>
								<c:if test="${ user.userType.id != type.id }">
									<option value="${ type.id }">${ type.name }</option>
								</c:if>
							</c:forEach>
						</form:select>
					</div>

					<div class="dropdown">
						<label for="permission">Select Permission:</label>
						<form:select multiple="true" path="permissionList">
							<c:forEach items="${ permissionList }" var="permission">
								<c:set var="iselected" value="false" />
								<c:forEach items="${user.permissionList }" var="c">
									<c:if test="${permission.id == c.id }">
										<c:set var="iselected" value="true" />
										<form:option selected="selected" value="${permission.id}">${permission.name}</form:option>
									</c:if>
								</c:forEach>

								<c:if test="${iselected == false }">
									<form:option value="${permission.id}">${permission.name}</form:option>
								</c:if>
							</c:forEach>
						</form:select>
					</div>

					<button type="submit" class="btn btn-outline-dark">Update</button>
				</form:form>

			</p>

			<a href="./listuser" class="btn btn-outline-dark">Back</a>
		</div>
	</div>
</body>
</html>