<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create Category</title>
</head>
<body>
	<h2>Create Category</h2>
	<p class="card-text">
		<c:if test="${not empty errorMessage }">
			<div class="alert alert-danger" role="alert">${errorMessage}</div>
		</c:if>
		<form:form action="./saveCategory" modelAttribute="category"
			method="post">
			<div class="form-group">
				<label for="name">Category Name</label>
				<form:input type="text" class="form-control" path="name" />
			</div>

			<button type="submit" class="btn btn-outline-dark">Create</button>
			<a href="./listCategory" class="btn btn-outline-dark">Back</a>
		</form:form>

	</p>
</body>
</html>