<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Company Details</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-sm">
				<div class="card" style="width: 600px;">
					<img class="card-img-top-preview-250"
						src="<c:url value="/uploads/${company.imageName }"/>"
						alt="Card image cap" />
					<div class="card-body">
						<h5 class="card-title">${company.info }</h5>
						<p class="card-text">
						<div class="row">
							<div class="col">Address:</div>
							<div class="col">${ company.address }</div>
						</div>
						<div class="row">
							<div class="col">Email:</div>
							<div class="col">${ company.email }</div>
						</div>
					</div>
				</div>
				<a href="./editCompany?id=${ company.id }"
					class="btn btn-outline-dark" role="button">Update</a> <a
					href="./listCompany" class="btn btn-outline-dark">Back</a>
			</div>
		</div>
	</div>
</body>