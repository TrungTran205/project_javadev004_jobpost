<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update Company</title>
</head>
<body>
	<h2>Update Company</h2>
	<p class="card-text">
		<c:if test="${not empty errorMessage }">
			<div class="alert alert-danger" role="alert">${errorMessage}</div>
		</c:if>
		<form:form action="./updateCompany" enctype="multipart/form-data"
			modelAttribute="company" method="post">
			<form:input type="hidden" path="id" value="${company.id }" />
			<div class="form-group">
				<form:input type="hidden" class="form-control" path="fromJobPost"
					value="${fromJobPost}" />
			</div>

			<div class="form-group">
				<label for="name">Your Company:</label>
				<form:input type="text" class="form-control" path="name" id="name"
					value="${company.name }" />
			</div>

			<div class="form-group">
				<label for="info">Info of Company:</label>
				<form:textarea class="form-control" path="info" id="info" rows="5"
					value="${company.info }" />
			</div>

			<div class="form-group">
				<label for="logoImage">Upload Logo Image</label><img alt=""
					width="200" height="200"
					src="<c:url value="/uploads/${ company.logoName }" />" />
				<form:input type="file" path="logoFile" class="form-control-file"
					id="logoImage" value="${company.logoName }" />
			</div>

			<div class="form-group">
				<label for="companyImage">Upload Company Image</label><img alt=""
					width="200" height="200"
					src="<c:url value="/uploads/${ company.imageName }" />" />
				<form:input type="file" path="imageFile" class="form-control-file"
					id="companyImage" value="${company.imageName }" />
			</div>

			<div class="dropdown">
				<label for="scale">Scale:</label>
				<form:select path="scale" id="scale">
					<option value="< 100">Below 100</option>
					<option value="100 - 200">100 - 200</option>
					<option value="201 - 300">201 - 300</option>
					<option value="300 - 400">301 - 400</option>
					<option value="400 - 500">401 - 500</option>
					<option value="> 500">Over 500</option>
				</form:select>
			</div>

			<div class="form-group">
				<label for="welfare">Welfare:</label>
				<form:textarea class="form-control" path="welfare"
					id="company.welfare" rows="5" value="${company.welfare }" />
			</div>

			<div class="form-group">
				<label for="address">Address of comp.:</label>
				<form:input type="text" class="form-control" path="address"
					id="address" value="${company.address }" />
			</div>

			<div class="form-group">
				<label for="email">Email address</label>
				<form:input type="email" class="form-control" id="email"
					path="email" placeholder="Enter email" value="${company.email }" />
			</div>

			<button type="submit" class="btn btn-outline-dark">Update</button>
			<a href="./listCompany" class="btn btn-outline-dark">Back</a>
		</form:form>
	</p>
</body>
</html>