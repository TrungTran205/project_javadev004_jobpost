<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List Company</title>
</head>
<body>
	<h2>List Company</h2>
	<form
		class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search pb-3"
		action="./searchCompany" method="post">
		<div class="input-group">
			<input type="text" name="search" class="form-control" placeholder="Search" value="${searchValue}">
			<div class="input-group-append">
				<button class="btn btn-primary" type="submit">
					<i class="fas fa-search fa-sm"></i>
				</button>
			</div>
		</div>
	</form>
	<br>
	<table class="table">
		<thead class="thead-dark">
			<tr>
				<th>Id</th>
				<th>Image</th>
				<th>Company name</th>
				<sec:authorize access="hasRole('ADMIN')">
					<th>Edit</th>
					<th>Delete</th>
				</sec:authorize>
			</tr>
		</thead>
		<c:forEach items="${companyList}" var="item">
			<tr>
				<td><a href="./viewCompanyDetails?id=${ item.id }">${item.id }</a></td>
				<td>
					<img class="card-img-top-preview"
						src="<c:url value="${pageContext.request.contextPath}/uploads/${item.imageName }"/>"
						alt="${item.imageName }" width="50" height="50"/>
				</td><!--  -->
				<td>${item.name }
				<sec:authorize access="hasRole('ADMIN')">
					<td><a href="./editCompany?id=${ item.id }"><i
							class="far fa-edit"></i></a></td>
					<td><a href="./deleteCompany?id=${ item.id }"> <i
							class="fas fa-user-times"></i></a></td>
				</sec:authorize>

			</tr>
		</c:forEach>
	</table>
	<sec:authorize access="hasRole('ADMIN')">
		<a class="btn btn-outline-dark" href="./newCompany?fromJobPost=false" role="button">
			Create Company</a>
	</sec:authorize>

</body>
</html>