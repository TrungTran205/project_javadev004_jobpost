<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><tiles:getAsString name="title" /></title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<link
	href="<c:url value="https://colorlib.com/preview/theme/jobpply/css/open-iconic-bootstrap.min.css" />"
	rel="stylesheet">
<link
	href="<c:url value="https://colorlib.com/preview/theme/jobpply/css/animate.css" />"
	rel="stylesheet">
<link
	href="<c:url value="https://colorlib.com/preview/theme/jobpply/css/owl.carousel.min.css" />"
	rel="stylesheet">
<link
	href="<c:url value="https://colorlib.com/preview/theme/jobpply/css/owl.theme.default.min.css" />"
	rel="stylesheet">
<link
	href="<c:url value="https://colorlib.com/preview/theme/jobpply/css/magnific-popup.css" />"
	rel="stylesheet">
<link
	href="<c:url value="https://colorlib.com/preview/theme/jobpply/css/aos.css" />"
	rel="stylesheet">
<link
	href="<c:url value="https://colorlib.com/preview/theme/jobpply/css/ionicons.min.css" />"
	rel="stylesheet">
<link
	href="<c:url value="https://colorlib.com/preview/theme/jobpply/css/bootstrap-datepicker.css" />"
	rel="stylesheet">
<link
	href="<c:url value="https://colorlib.com/preview/theme/jobpply/css/jquery.timepicker.css" />"
	rel="stylesheet">
<link
	href="<c:url value="https://colorlib.com/preview/theme/jobpply/css/flaticon.css" />"
	rel="stylesheet">
<link
	href="<c:url value="https://colorlib.com/preview/theme/jobpply/css/icomoon.css" />"
	rel="stylesheet">
<link
	href="<c:url value="https://colorlib.com/preview/theme/jobpply/css/style.css" />"
	rel="stylesheet">
<link
	href="https://startbootstrap.github.io/startbootstrap-sb-admin-2/vendor/fontawesome-free/css/all.min.css"
	rel="stylesheet" type="text/css">
</head>
<body>
	<nav
		class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light"
		id="ftco-navbar">
		<div class="container">
			<tiles:insertAttribute name="header" />
		</div>
	</nav>

	<div>
		<tiles:insertAttribute name="body" />
	</div>


	<tiles:insertAttribute name="footer" />

	<script
		src="https://colorlib.com/preview/theme/jobpply/js/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="https://colorlib.com/preview/theme/jobpply/js/popper.min.js"
		type="text/javascript"></script>
	<script
		src="https://colorlib.com/preview/theme/jobpply/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
		integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
		integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
		crossorigin="anonymous"></script>
	<script src="https://colorlib.com/preview/theme/jobpply/js/main.js"
		type="887ee70b546e7252ecaf2f8f-text/javascript"></script>
</body>
</html>