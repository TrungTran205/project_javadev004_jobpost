<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<a class="navbar-brand" href="${pageContext.request.contextPath}/home/">HRTool</a>
<button class="navbar-toggler" type="button" data-toggle="collapse"
	data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false"
	aria-label="Toggle navigation">
	<span class="oi oi-menu"></span> Menu
</button>
<div class="collapse navbar-collapse" id="ftco-nav">
	<ul class="navbar-nav ml-auto">
		<li class="nav-item active"><a
			href="${pageContext.request.contextPath}/home/" class="nav-link">Home</a></li>

		<%-- <sec:authorize access="isAuthenticated()">
			<li class="nav-item"><a class="nav-link"
				href="${pageContext.request.contextPath}/admin/dashboard">Dashboard</a></li>
		</sec:authorize> --%>

		<sec:authorize access="!isAuthenticated()">
			<!-- <li class="nav-item"><a href="about.html" class="nav-link">About</a></li> -->

			<li class="nav-item"><a
				href="${pageContext.request.contextPath}/home/candidates"
				class="nav-link">Canditates</a></li>

			<li class="nav-item"><a
				href="${pageContext.request.contextPath}/home/jobs" class="nav-link">Job</a></li>

			<!-- <li class="nav-item"><a href="blog.html" class="nav-link">Blog</a></li>

			<li class="nav-item"><a href="contact.html" class="nav-link">Contact</a></li> -->

			<li class="nav-item cta mr-md-1"><a class="nav-link"
				href="/login">Login</a></li>
		</sec:authorize>


		<sec:authorize access="isAuthenticated()">
			<sec:authorize access="hasRole('EMPLOYER')">
				<li class="nav-item"><a
					href="${pageContext.request.contextPath}/home/employer/listPost"
					class="nav-link">Job</a></li>

				<li class="nav-item"><a
					href="${pageContext.request.contextPath}/home/employer/listCandidates"
					class="nav-link">Canditates</a></li>

				<li class="nav-item cta cta-colored"><a
					href="${pageContext.request.contextPath}/home/employer/newPost"
					class="nav-link"><i class="fas fa-plus-circle"></i> Post a Job</a></li>

				<li class="nav-item cta cta-colored"><a
					href="${pageContext.request.contextPath}/home/candidates"
					class="nav-link"><i class="fas fa-search"></i> Find Candidates</a></li>
			</sec:authorize>

			<sec:authorize access="hasRole('CANDIDATE')">
				<%-- <li class="nav-item"><a
					href="${pageContext.request.contextPath}/home/resumedetail"
					class="nav-link"> CV</a></li> --%>
				<li class="nav-item cta cta-colored"><a
					href="${pageContext.request.contextPath}/home/jobUserLiked"
					class="nav-link"><i class="far fa-heart"></i> Saved Jobs</a></li>
				<li class="nav-item cta cta-colored"><a
					href="${pageContext.request.contextPath}/home/listJobApplied"
					class="nav-link"><i class="fas fa-clipboard-check"></i> Applied
						Jobs</a></li>
			</sec:authorize>
		</sec:authorize>

		<sec:authorize access="isAuthenticated()">
			<li class="nav-item cta mr-md-1"><a class="nav-link "
				href="${pageContext.request.contextPath}/home/profile"><i
					class="fas fa-user-circle"></i> Welcome <sec:authentication
						property="principal.username" /></a></li>
			<li class="nav-item cta mr-md-1"><a class="nav-link"
				href="/logout">Log out</a></li>
		</sec:authorize>
	</ul>
</div>
