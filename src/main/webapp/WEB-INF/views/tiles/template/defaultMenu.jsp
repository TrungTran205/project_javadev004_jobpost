<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<ul class="navbar-nav bg-gradient-danger sidebar sidebar-dark accordion"
	id="accordionSidebar">

	<!-- Sidebar - Brand -->
	<a
		class="sidebar-brand d-flex align-items-center justify-content-center"
		href="index.html">
		<div class="sidebar-brand-icon rotate-n-15">
			<!-- <i class="fas fa-laugh-wink"></i> -->
		</div>
		<div class="sidebar-brand-text mx-3">Hr Tool</div>
	</a>

	<!-- Divider -->
	<hr class="sidebar-divider my-0">

	<!-- Nav Item - Dashboard -->
	<li class="nav-item active"><a class="nav-link"
		href="${pageContext.request.contextPath}/admin/dashboard"> <i
			class="fas fa-fw fa-tachometer-alt"></i> <span>Dashboard</span></a></li>


	<!-- Divider -->
	<hr class="sidebar-divider my-0">

	<!-- Nav Item - Dashboard -->
	<li class="nav-item active"><a class="nav-link"
		href="${pageContext.request.contextPath}/home/"><i
			class="fas fa-house-user"></i> <span>Home</span></a></li>


	<sec:authorize access="hasAnyRole('EMPLOYER','ADMIN')">
		<!-- Divider -->
		<hr class="sidebar-divider">

		<!-- Heading -->

		<div class="sidebar-heading">Employer</div>

		<!-- Nav Item - Job Post -->
		<li class="nav-item"><a class="nav-link collapsed" href=""
			data-toggle="collapse" data-target="#collapseTwo"
			aria-expanded="true" aria-controls="collapseTwo"> <i
				class="fas fa-fw fa-cog"></i> <span>Job Post</span>
		</a>

			<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
				data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded">
					<h6 class="collapse-header">Action</h6>
					<sec:authorize access="hasRole('EMPLOYER')">
						<a class="collapse-item"
							href="${pageContext.request.contextPath}/employer/newPost">Add
							new</a>
					</sec:authorize>
					<a class="collapse-item"
						href="${pageContext.request.contextPath}/admin/listPost">List</a>
				</div>
			</div></li>

		<!-- Nav Item - CV -->
		<li class="nav-item"><a class="nav-link collapsed" href=""
			data-toggle="collapse" data-target="#collapseCV" aria-expanded="true"
			aria-controls="collapseCV"> <i class="fas fa-fw fa-wrench"></i> <span>Candidates</span>
		</a>
			<div id="collapseCV" class="collapse" aria-labelledby="headingCV"
				data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded">
					<h6 class="collapse-header">Action</h6>
					<sec:authorize access="hasRole('EMPLOYER')">
						<a class="collapse-item"
							href="${pageContext.request.contextPath}/employer/listEmployee">My
							Candidates</a>
					</sec:authorize>
					<a class="collapse-item"
						href="${pageContext.request.contextPath}/admin/listCandidates">List
						Candidates</a>
				</div>
			</div></li>
	</sec:authorize>
	<sec:authorize access="hasAnyRole('CANDIDATE','ADMIN')">
		<!-- Divider -->
		<hr class="sidebar-divider">

		<!-- Heading -->
		<div class="sidebar-heading">Candidate</div>

		<!-- Nav Item - My CV -->
		<li class="nav-item"><a class="nav-link collapsed" href="#"
			data-toggle="collapse" data-target="#collapseThree"
			aria-expanded="true" aria-controls="collapseThree"> <i
				class="fas fa-fw fa-cog"></i> <span>My CV</span>
		</a>
			<div id="collapseThree" class="collapse"
				aria-labelledby="headingThree" data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded">
					<h6 class="collapse-header">Action</h6>
					<sec:authorize access="hasRole('CANDIDATE')">
						<a class="collapse-item"
							href="${pageContext.request.contextPath}/resume/newResume">Add
							new</a>
					</sec:authorize>
					<a class="collapse-item"
						href="${pageContext.request.contextPath}/admin/listCVs">List</a>
				</div>
			</div></li>

		<!-- Nav Item - My Job -->
		<li class="nav-item"><a class="nav-link collapsed" href="#"
			data-toggle="collapse" data-target="#collapseJob"
			aria-expanded="true" aria-controls="collapseJob"> <i
				class="fas fa-fw fa-wrench"></i> <span>My Job</span>
		</a>
			<div id="collapseJob" class="collapse" aria-labelledby="headingjob"
				data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded">
					<h6 class="collapse-header">Action</h6>
					<sec:authorize access="hasRole('CANDIDATE')">
						<a class="collapse-item"
							href="${pageContext.request.contextPath}/resume/listJobApplied">List
							Job Applied</a>
					</sec:authorize>
					<a class="collapse-item"
						href="${pageContext.request.contextPath}/admin/listUserJobApplied">List
						All Jobs</a>
				</div>
			</div></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ADMIN')">
		<!-- Divider -->
		<hr class="sidebar-divider">

		<!-- Heading -->
		<div class="sidebar-heading">Admin</div>

		<!-- Nav Item - User -->
		<li class="nav-item"><a class="nav-link collapsed" href="#"
			data-toggle="collapse" data-target="#collapseFour"
			aria-expanded="true" aria-controls="collapseFour"> <i
				class="fas fa-fw fa-cog"></i> <span>Users</span>
		</a>
			<div id="collapseFour" class="collapse" aria-labelledby="headingFour"
				data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded">
					<h6 class="collapse-header">Action</h6>
					<a class="collapse-item"
						href="${pageContext.request.contextPath}/admin/newuser">Add
						new</a> <a class="collapse-item"
						href="${pageContext.request.contextPath}/admin/listuser">List</a>

					<!-- <h6 class="collapse-header">Login Screens:</h6>
				<a class="collapse-item" href="login.html">Login</a> <a
					class="collapse-item" href="register.html">Register</a> <a
					class="collapse-item" href="forgot-password.html">Forgot
					Password</a>
				<div class="collapse-divider"></div>
				<h6 class="collapse-header">Other Pages:</h6>
				<a class="collapse-item" href="404.html">404 Page</a> <a
					class="collapse-item" href="blank.html">Blank Page</a> -->

				</div>
			</div></li>

		<!-- Nav Item - User Type -->
		<li class="nav-item"><a class="nav-link collapsed" href="#"
			data-toggle="collapse" data-target="#collapseFive"
			aria-expanded="true" aria-controls="collapseFive"> <i
				class="fas fa-fw fa-folder"></i> <span>User Type</span></a>
			<div id="collapseFive" class="collapse" aria-labelledby="headingFive"
				data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded">
					<h6 class="collapse-header">Action</h6>
					<a class="collapse-item"
						href="${pageContext.request.contextPath}/admin/userType/newUserType">Add
						new</a> <a class="collapse-item"
						href="${pageContext.request.contextPath}/admin/userType/listUserType">List</a>
				</div>
			</div></li>

		<!-- Nav Item - Category -->
		<li class="nav-item"><a class="nav-link collapsed" href="#"
			data-toggle="collapse" data-target="#collapseSix"
			aria-expanded="true" aria-controls="collapseSix"> <i
				class="fas fa-fw fa-folder"></i> <span>Category</span></a>
			<div id="collapseSix" class="collapse" aria-labelledby="headingSix"
				data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded">
					<h6 class="collapse-header">Action</h6>
					<a class="collapse-item"
						href="${pageContext.request.contextPath}/admin/category/newCategory">Add
						new</a> <a class="collapse-item"
						href="${pageContext.request.contextPath}/admin/category/listCategory">List</a>
				</div>
			</div></li>

		<!-- Nav Item - Company -->
		<li class="nav-item"><a class="nav-link collapsed" href="#"
			data-toggle="collapse" data-target="#collapseSeven"
			aria-expanded="true" aria-controls="collapseSeven"> <i
				class="fas fa-fw fa-folder"></i> <span>Company</span></a>
			<div id="collapseSeven" class="collapse"
				aria-labelledby="headingSeven" data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded">
					<h6 class="collapse-header">Action</h6>
					<a class="collapse-item"
						href="${pageContext.request.contextPath}/masterdata/company/newCompany?fromJobPost=false">Add
						new</a> <a class="collapse-item"
						href="${pageContext.request.contextPath}/masterdata/company/listCompany">List</a>
				</div>
			</div></li>

		<!-- Nav Permission-->
		<li class="nav-item"><a class="nav-link" href="#"
			data-toggle="collapse" data-target="#collapsePermission"
			aria-expanded="true" aria-controls="collapsePermission"> <i
				class="fas fa-fw fa-folder"></i> <span>Permission</span></a>
			<div id="collapsePermission" class="collapse"
				aria-labelledby="headingPermission" data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded">
					<h6 class="collapse-header">Action</h6>
					<a class="collapse-item"
						href="${pageContext.request.contextPath}/admin/permission/newPermission">Add
						new</a> <a class="collapse-item"
						href="${pageContext.request.contextPath}/admin/permission/listPermission">List</a>


					<!--  <a
					class="collapse-item"
					href="http://localhost:8080/userType/listUserType">List</a>
			
		 -->
				</div>
			</div></li>
	</sec:authorize>
	<!-- Divider -->
	<hr class="sidebar-divider d-none d-md-block">

	<!-- Sidebar Toggler (Sidebar) -->
	<div class="text-center d-none d-md-inline">
		<button class="rounded-circle border-0" id="sidebarToggle"></button>
	</div>

</ul>