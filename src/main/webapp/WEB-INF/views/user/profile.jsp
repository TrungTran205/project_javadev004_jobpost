<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Saved Jobs</title>
</head>
<body>
	<div class="hero-wrap hero-wrap-2"
		style="background-image: url(https://content.thriveglobal.com/wp-content/uploads/2019/12/Job.jpg); background-position: 30% 0%;"
		data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div
				class="row no-gutters slider-text align-items-end justify-content-start">
				<div
					class="col-md-8 ftco-animate text-center text-md-left mb-5 fadeInUp ftco-animated">
					<p class="breadcrumbs mb-0">
						<span class="mr-3"><a
							href="${pageContext.request.contextPath}/home/">Home <i
								class="ion-ios-arrow-forward"></i></a></span> <span>Profile</span>
					</p>
					<h2 class="mb-3 bread">Info</h2>
				</div>
			</div>
		</div>
	</div>

	<div class="container" style="margin-top:50px">
		<div class="card">
			<div class="card-header">
				<h3>Your Profile</h3>
			</div>
			<div class="card-body">
				<form action="./editProfile" method="get">
					<input type="hidden" name="id" value="${user.id }" /> <input
						type="hidden" name="userType" value="${user.userType.id }" />
					<c:forEach items="${user.permissionList}" var="item">
						<input type="hidden" name="permissionList" value="${item.id}" />
					</c:forEach>
					<img class="card-img-top-preview-250"
						src="<c:url value="/uploads/${user.imageName }"/>"
						alt="Card image cap" />

					<div class="form-group row">
						<label for="name" class="col-sm-2 col-form-label">Your
							Name:</label>
						<div class="col-sm-10">
							<input type="text" readonly class="form-control-plaintext"
								value="${user.firstName} ${user.lastName}" />
						</div>
					</div>

					<div class="form-group row">
						<label for="birthday" class="col-sm-2 col-form-label">Birthday:</label>
						<div class="col-sm-10">
							<input type="text" readonly class="form-control-plaintext"
								value="${user.dob}" />
						</div>
					</div>

					<div class="form-group row">
						<label for="phone" class="col-sm-2 col-form-label">Phone:</label>
						<div class="col-sm-10">
							<input type="text" readonly class="form-control-plaintext"
								value="${user.phoneNumber}" />
						</div>
					</div>

					<div class="form-group row">
						<label for="email" class="col-sm-2 col-form-label">Email:</label>
						<div class="col-sm-10">
							<input type="text" readonly class="form-control-plaintext"
								value="${user.email}" />
						</div>
					</div>

					<div class="form-group row">
						<label for="userName" class="col-sm-2 col-form-label">Username:</label>
						<div class="col-sm-10">
							<input type="text" readonly class="form-control-plaintext"
								value="${user.userName}" />
						</div>
					</div>

					<div class="form-group row">
						<label for="password" class="col-sm-2 col-form-label">Password:</label>
						<div class="col-sm-10">
							<a
								href="${pageContext.request.contextPath}/home/profile/changePassword"
								class="btn btn-outline-dark">Change Password</a>
						</div>
					</div>
					<button type="submit" class="btn btn-outline-dark">Edit</button>
				</form>
			</div>
		</div>
	</div>
</body>
</html>