<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Candidates</title>
</head>
<body>
	<div class="hero-wrap hero-wrap-2"
		style="background-image: url(https://content.thriveglobal.com/wp-content/uploads/2019/12/Job.jpg); background-position: 30% 0%;"
		data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div
				class="row no-gutters slider-text align-items-end justify-content-start">
				<div
					class="col-md-8 ftco-animate text-center text-md-left mb-5 fadeInUp ftco-animated">
					<p class="breadcrumbs mb-0">
						<span class="mr-3"><a href="index.html">Home <i
								class="ion-ios-arrow-forward"></i></a></span> <span>Candidates 
					</p>
					<h2 class="mb-3 bread">Candidates</h2>
				</div>
			</div>
		</div>
	</div>

	<section
		class="ftco-section ftco-candidates ftco-candidates-2 bg-light">
	<c:forEach items="${candidateList }" var="item">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="team d-md-flex">

						<div class="img"
							style="background-image: url(images/person_1.jpg);"></div>
						<div class="text pl-md-4">

							<span class="location mb-0">${item.userName }</span>

							<h2>${item.firstName }</h2>

							<span class="position">${item.dob }</span>

							<p>${item.experiences }</p>

							<span class="seen">${item.email }</span>
							
							<sec:authorize access="hasRole('EMPLOYER')">
							<p>
								<a href="#" class="btn btn-primary">Shortlist</a>
							</p>
							</sec:authorize>
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:forEach> </section>
</body>
</html>