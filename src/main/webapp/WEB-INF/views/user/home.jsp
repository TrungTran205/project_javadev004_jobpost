<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>
<link href="<c:url value='/resources/css/style.css' />" rel="stylesheet"></link>
<%@ page import="java.util.ArrayList" %>
</head>
<body>

	<!-- This is for any User -->
	<sec:authorize access="!isAuthenticated()">
		<div class="hero-wrap js-fullheight" style="height: 741px;">
			<div class="overlay"></div>
			<div class="container-fluid px-0">
				<div
					class="row d-md-flex no-gutters slider-text align-items-end js-fullheight justify-content-end"
					style="height: 741px;">
					<img class="one-third align-self-end order-md-last img-fluid"
						src="https://colorlib.com/preview/theme/jobpply/images/undraw_work_time_lhoj.svg"
						alt="">
					<div
						class="one-forth d-flex align-items-center ftco-animate js-fullheight fadeInUp ftco-animated"
						style="height: 741px;">
						<div class="text mt-5">
							<p class="mb-4 mt-5 pt-5">
								We have <span class="number" data-number="200000">200,000</span>
								great job offers you deserve!
							</p>
							<h1 class="mb-5">Largest Job Site In The World</h1>
							<div class="ftco-search">
								<div class="row">
									<div class="col-md-12 nav-link-wrap">
										<div class="nav nav-pills text-center" id="v-pills-tab"
											role="tablist" aria-orientation="vertical">
											<a class="nav-link mr-md-1 active" id="v-pills-1-tab"
												data-toggle="pill" href="#v-pills-1" role="tab"
												aria-controls="v-pills-1" aria-selected="true">Find a
												Job</a> <a class="nav-link" id="v-pills-2-tab"
												data-toggle="pill" href="#v-pills-2" role="tab"
												aria-controls="v-pills-2" aria-selected="false">Find a
												Candidate</a>
										</div>
									</div>
									<div class="col-md-12 tab-wrap">
										<div class="tab-content p-4" id="v-pills-tabContent">
											<div class="tab-pane fade active show" id="v-pills-1"
												role="tabpanel" aria-labelledby="v-pills-nextgen-tab">
												<form action="./searchPost" method="post" class="search-job">
													<div class="row no-gutters">
														<div class="col-md mr-md-2">
															<div class="form-group">
																<div class="form-field">
																	<div class="select-wrap">
																		<div class="icon">
																			<span class="ion-ios-arrow-down"></span>
																		</div>
																		<select name="category" id="" class="form-control"
																			value="${categoryValue}">
																			<option value="" disabled selected>Category</option>
																			<c:forEach items="${categoryList }" var="cat">
																				<option value="${cat.id }">${cat.name }</option>
																			</c:forEach>
																		</select>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md mr-md-2">
															<div class="form-group">
																<div class="form-field">
																	<div class="select-wrap">
																		<div class="icon">
																			<span class="ion-ios-arrow-down"></span>
																		</div>
																		<select name="type" id="" class="form-control"
																			value="${typeValue}">
																			<option value="" disabled selected>Type</option>
																			<option value="Full Time">Full Time</option>
																			<option value="Part Time">Part Time</option>
																			<option value="Freelance">Freelance</option>
																			<option value="Temporary">Temporary</option>
																		</select>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md mr-md-2">
															<div class="form-group">
																<div class="form-field">
																	<input type="text" class="form-control" name="name"
																		placeholder="Enter Keyword..." value="${nameValue}">
																</div>
															</div>
														</div>
														<div class="col-md">
															<div class="form-group">
																<div class="form-field">
																	<button type="submit"
																		class="form-control btn btn-secondary">Search</button>
																</div>
															</div>
														</div>
													</div>
												</form>
											</div>
											<div class="tab-pane fade" id="v-pills-2" role="tabpanel"
												aria-labelledby="v-pills-performance-tab">
												<form action="./searchResume" method="post"
													class="search-job">
													<div class="row">
														<div class="col-md">
															<div class="form-group">
																<div class="form-field">
																	<input type="text" name="search" class="form-control"
																		placeholder="Enter First Name or Last Name ..."
																		value="${searchValue}">
																</div>
															</div>
														</div>
														<div class="col-md">
															<div class="col-md">
																<div class="col-md">
																	<div class="form-group">
																		<div class="form-field">
																			<button type="submit"
																				class="form-control btn btn-secondary">
																				<i class="fas fa-search fa-sm"></i> Search
																			</button>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<section class="ftco-section services-section bg-primary">
		<div class="container">
			<div class="row d-flex">
				<div
					class="col-md-3 d-flex align-self-stretch ftco-animate fadeInUp ftco-animated">
					<div class="media block-6 services d-block">
						<div class="icon">
							<span class="flaticon-resume"></span>
						</div>
						<div class="media-body">
							<h3 class="heading mb-3">Search Millions of Jobs</h3>
							<p>A small river named Duden flows by their place and
								supplies.</p>
						</div>
					</div>
				</div>
				<div
					class="col-md-3 d-flex align-self-stretch ftco-animate fadeInUp ftco-animated">
					<div class="media block-6 services d-block">
						<div class="icon">
							<span class="flaticon-collaboration"></span>
						</div>
						<div class="media-body">
							<h3 class="heading mb-3">Easy To Manage Jobs</h3>
							<p>A small river named Duden flows by their place and
								supplies.</p>
						</div>
					</div>
				</div>
				<div
					class="col-md-3 d-flex align-self-stretch ftco-animate fadeInUp ftco-animated">
					<div class="media block-6 services d-block">
						<div class="icon">
							<span class="flaticon-promotions"></span>
						</div>
						<div class="media-body">
							<h3 class="heading mb-3">Top Careers</h3>
							<p>A small river named Duden flows by their place and
								supplies.</p>
						</div>
					</div>
				</div>
				<div
					class="col-md-3 d-flex align-self-stretch ftco-animate fadeInUp ftco-animated">
					<div class="media block-6 services d-block">
						<div class="icon">
							<span class="flaticon-employee"></span>
						</div>
						<div class="media-body">
							<h3 class="heading mb-3">Search Expert Candidates</h3>
							<p>A small river named Duden flows by their place and
								supplies.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		</section>
		<section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center mb-5 pb-3">
				<div
					class="col-md-7 heading-section text-center ftco-animate fadeInUp ftco-animated">
					<span class="subheading">Job Categories</span>
					<h2 class="mb-4">Top Categories</h2>
				</div>
			</div>
			<div class="row">
				<c:forEach items="${categoryList1 }" var="cat">
					<div class="col-md-3 ftco-animate fadeInUp ftco-animated">
						<ul class="category">
							<li><a href="./category?catId=${cat.id}">${cat.name } <br>
									<span class="number">${cat.availableJobCount }</span> <c:if
										test="${cat.availableJobCount < 1}">
										<span>No new Job</span>
									</c:if> <c:if test="${cat.availableJobCount >= 1}">
										<span>New Jobs</span>
									</c:if> <i class="ion-ios-arrow-forward"></i>
							</a></li>
						</ul>
					</div>
				</c:forEach>
			</div>
		</div>
		</section>
		<section class="ftco-section bg-light">
		<div class="container">
			<div class="row">
				<div class="col-lg-9 pr-lg-5">
					<div class="row justify-content-center pb-3">
						<div
							class="col-md-12 heading-section ftco-animate fadeInUp ftco-animated">
							<span class="subheading">Recently Added Jobs</span>
							<h2 class="mb-4">Hot Jobs</h2>
						</div>
					</div>
					<div class="row">
						<c:forEach items="${postList}" var="item">
							<c:if test="${item.vacancy >= 1}">
								<div class="col-md-12 ftco-animate fadeInUp ftco-animated">
									<div
										class="job-post-item py-4 d-block d-lg-flex align-items-center">
										<div class="one-third mb-4 mb-md-0">
											<div class="job-post-item-header d-flex align-items-center">
												<h2 class="mr-3 text-black">
													<a href="./jobdetail?postId=${item.id }">${item.nameJob }</a>
												</h2>
												<div class="badge-wrap">
													<c:if test="${item.type == 'Full Time'}">
														<span class="bg-danger text-white badge py-2 px-3">${item.type }</span>
													</c:if>
													<c:if test="${item.type == 'Part Time'}">
														<span class="bg-primary text-white badge py-2 px-3">${item.type }</span>
													</c:if>
													<c:if test="${item.type == 'Temporary'}">
														<span class="bg-warning text-white badge py-2 px-3">${item.type }</span>
													</c:if>
													<c:if test="${item.type == 'Freelance'}">
														<span class="bg-success text-white badge py-2 px-3">${item.type }</span>
													</c:if>
												</div>
											</div>
											<div class="job-post-item-body d-block d-md-flex">
												<div class="mr-3">
													<c:set var="balance" value="${item.salary }" />
													<span class="icon-money"></span> <span><fmt:formatNumber
															value="${balance}" type="currency" currencyCode="USD" /></span>
												</div>
												<div>
													<span class="icon-my_location"></span> <span>${item.company.address}</span>
												</div>
											</div>
										</div>
										<div
											class="one-forth ml-auto d-flex align-items-center mt-4 md-md-0">
											<%-- <sec:authorize access="isAuthenticated()">
												<sec:authentication property="principal.username"
													var="username"/>
												<div>
													<sec:authorize access="hasRole('CANDIDATE')">
														<c:if
															test="${(not empty item.likeJobBy) && (item.likeJobBy.userName==username) }">

															<a href="./likeJobPost?postId=${item.id}"
																class="icon text-center d-flex justify-content-center align-items-center icon mr-2">
																<span class="icon-minus"></span>
															</a>

														</c:if>
														<c:if
															test="${(not empty item.likeJobBy) && (item.likeJobBy.userName!=username) || (empty item.likeJobBy)}">
															<a href="./likeJobPost?postId=${item.id}"
																class="icon text-center d-flex justify-content-center align-items-center icon mr-2">
																<span class="icon-heart"></span>
															</a>
														</c:if>
														<c:if test="${empty item.likeJobBy}">
															<a href="./likeJobPost?postId=${item.id}"
																class="icon text-center d-flex justify-content-center align-items-center icon mr-2">
																<span class="icon-heart"></span>
															</a>
														</c:if>
													</sec:authorize>
												</div>
												<c:if test="${item.expired==false}">
													<a href="./applyjob?postId=${item.id }"
														class="btn btn-primary py-2">Apply Job</a>
												</c:if>
											</sec:authorize> --%>
											<%-- <sec:authorize access="!isAuthenticated()"> --%>
											<div>
												<a href="${pageContext.request.contextPath}/login"
													class="icon text-center d-flex justify-content-center align-items-center icon mr-2">
													<span class="icon-heart"></span>
												</a>
											</div>
											<c:if test="${item.expired==false}">
												<a href="${pageContext.request.contextPath}/login"
													class="btn btn-primary py-2">Apply Job</a>
											</c:if>
											<%-- </sec:authorize> --%>
										</div>

									</div>
								</div>
							</c:if>
						</c:forEach>
					</div>

				</div>
			</div>
		</div>
		</section>
		<%-- <section class="ftco-section ftco-candidates bg-primary">
		<div class="container">
			<div class="row justify-content-center pb-3">
				<div
					class="col-md-10 heading-section heading-section-white text-center ftco-animate fadeInUp ftco-animated">
					<span class="subheading">Candidates</span>
					<h2 class="mb-4">Latest Candidates</h2>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-12 ftco-animate fadeInUp ftco-animated">
					<div class="carousel-candidates owl-carousel owl-loaded owl-drag">
						<div class="owl-stage-outer owl-height" style="height: 305.778px;">
							<div class="owl-stage"
								style="transform: translate3d(-192px, 0px, 0px); transition: all 0.25s ease 0s; width: 1152px;">
								<c:forEach items="${resumeList}" var="item">
									<div class="owl-item" style="width: 162px; margin-right: 30px;">
										<div class="item">
											<a href="#" class="team text-center">
												<div class="img"
													style="background-image: url(images/person_1.jpg);"></div>
												<h2>${item.firstname }${item.lastname }</h2> <span
												class="position">${item.skills }</span>
											</a>
										</div>
									</div>
								</c:forEach>
							</div>
						</div>
						<div class="owl-nav disabled">
							<button role="presentation" class="owl-prev">
								<span class="ion-ios-arrow-back"></span>
							</button>
							<button role="presentation" class="owl-next">
								<span class="ion-ios-arrow-forward"></span>
							</button>
						</div>
						<div class="owl-dots">
							<button class="owl-dot">
								<span></span>
							</button>
							<button class="owl-dot active">
								<span></span>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		</section> --%>
	</sec:authorize>

	<sec:authorize access="isAuthenticated()">
		
		<!-- This is for Admin -->
		<sec:authorize access="hasRole('ADMIN')">
			<div class="hero-wrap hero-wrap-2"
				style="background-image: url(https://content.thriveglobal.com/wp-content/uploads/2019/12/Job.jpg); background-position: 30% 0%;"
				data-stellar-background-ratio="0.5">
				<div class="overlay"></div>
				<div class="container">
					<div
						class="row no-gutters slider-text align-items-end justify-content-start">
						<div
							class="col-md-8 ftco-animate text-center text-md-left mb-5 fadeInUp ftco-animated">
							<p class="breadcrumbs mb-0">
								<span class="mr-3"><a
									href="${pageContext.request.contextPath}/home/">Home <i
										class="ion-ios-arrow-forward"></i></a></span> <span>Admin 
							</p>
							<h2 class="mb-3 bread">You are logging as Admin</h2>
							<a class="btn btn-info"
								href="${pageContext.request.contextPath}/admin/dashboard"
								role="button">Move to Admin Page <i
								class="fas fa-arrow-circle-right"></i></a>
						</div>
					</div>
				</div>
			</div>
		</sec:authorize>

		<!-- This is for Employer -->
		<sec:authorize access="hasRole('EMPLOYER')">
			<div class="hero-wrap hero-wrap-2"
				style="background-image: url(https://content.thriveglobal.com/wp-content/uploads/2019/12/Job.jpg); background-position: 30% 0%;"
				data-stellar-background-ratio="0.5">
				<div class="container">
					<div
						class="row no-gutters slider-text align-items-end justify-content-start">
						<div
							class="col-md-8 ftco-animate text-center text-md-left mb-5 fadeInUp ftco-animated">
							<p class="breadcrumbs mb-0">
								<span class="mr-3"><a href="index.html">Home</a></span>
							</p>
							<h2 class="mb-3 bread">YOUR HOME</h2>
						</div>
					</div>
				</div>
			</div>
		</sec:authorize>

		<!-- This is for Candidates -->
		<sec:authorize access="hasRole('CANDIDATE')">

			<div class="hero-wrap js-fullheight" style="height: 741px;">
				<div class="overlay"></div>
				<div class="container-fluid px-0">
					<div
						class="row d-md-flex no-gutters slider-text align-items-end js-fullheight justify-content-end"
						style="height: 741px;">
						<img class="one-third align-self-end order-md-last img-fluid"
							src="https://colorlib.com/preview/theme/jobpply/images/undraw_work_time_lhoj.svg"
							alt="">
						<div
							class="one-forth d-flex align-items-center ftco-animate js-fullheight fadeInUp ftco-animated"
							style="height: 741px;">
							<div class="text mt-5">
								<p class="mb-4 mt-5 pt-5">
									We have <span class="number" data-number="200000">200,000</span>
									great job offers you deserve!
								</p>
								<h1 class="mb-5">Largest Job Site In The World	</h1>
								<div class="ftco-search">
									<div class="row">
										<div class="col-md-12 nav-link-wrap">
											<div class="nav nav-pills text-center" id="v-pills-tab"
												role="tablist" aria-orientation="vertical">
												<a class="nav-link mr-md-1 active" id="v-pills-1-tab"
													data-toggle="pill" href="#v-pills-1" role="tab"
													aria-controls="v-pills-1" aria-selected="true">Find a
													Job</a>
											</div>
										</div>
										<div class="col-md-12 tab-wrap">
											<div class="tab-content p-4" id="v-pills-tabContent">
												<div class="tab-pane fade active show" id="v-pills-1"
													role="tabpanel" aria-labelledby="v-pills-nextgen-tab">
													<form action="./searchPost" method="post"
														class="search-job">
														<div class="row no-gutters">
															<div class="col-md mr-md-2">
																<div class="form-group">
																	<div class="form-field">
																		<div class="select-wrap">
																			<div class="icon">
																				<span class="ion-ios-arrow-down"></span>
																			</div>
																			<select name="category" id="" class="form-control"
																				value="${categoryValue}">
																				<option value="" disabled selected>Category</option>
																				<c:forEach items="${categoryList }" var="cat">
																					<option value="${cat.id }">${cat.name }</option>
																				</c:forEach>
																			</select>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md mr-md-2">
																<div class="form-group">
																	<div class="form-field">
																		<div class="select-wrap">
																			<div class="icon">
																				<span class="ion-ios-arrow-down"></span>
																			</div>
																			<select name="type" id="" class="form-control"
																				value="${typeValue}">
																				<option value="" disabled selected>Type</option>
																				<option value="Full Time">Full Time</option>
																				<option value="Part Time">Part Time</option>
																				<option value="Freelance">Freelance</option>
																				<option value="Temporary">Temporary</option>
																			</select>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md mr-md-2">
																<div class="form-group">
																	<div class="form-field">
																		<input type="text" class="form-control" name="name"
																			placeholder="Enter Keyword..." value="${nameValue}">
																	</div>
																</div>
															</div>
															<div class="col-md">
																<div class="form-group">
																	<div class="form-field">
																		<button type="submit"
																			class="form-control btn btn-secondary">Search</button>
																	</div>
																</div>
															</div>
														</div>
													</form>
												</div>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<section class="ftco-section services-section bg-primary">
			<div class="container">
				<div class="row d-flex">
					<div
						class="col-md-3 d-flex align-self-stretch ftco-animate fadeInUp ftco-animated">
						<div class="media block-6 services d-block">
							<div class="icon">
								<span class="flaticon-resume"></span>
							</div>
							<div class="media-body">
								<h3 class="heading mb-3">Search Millions of Jobs</h3>
								<p>A small river named Duden flows by their place and
									supplies.</p>
							</div>
						</div>
					</div>
					<div
						class="col-md-3 d-flex align-self-stretch ftco-animate fadeInUp ftco-animated">
						<div class="media block-6 services d-block">
							<div class="icon">
								<span class="flaticon-collaboration"></span>
							</div>
							<div class="media-body">
								<h3 class="heading mb-3">Easy To Manage Jobs</h3>
								<p>A small river named Duden flows by their place and
									supplies.</p>
							</div>
						</div>
					</div>
					<div
						class="col-md-3 d-flex align-self-stretch ftco-animate fadeInUp ftco-animated">
						<div class="media block-6 services d-block">
							<div class="icon">
								<span class="flaticon-promotions"></span>
							</div>
							<div class="media-body">
								<h3 class="heading mb-3">Top Careers</h3>
								<p>A small river named Duden flows by their place and
									supplies.</p>
							</div>
						</div>
					</div>
					<div
						class="col-md-3 d-flex align-self-stretch ftco-animate fadeInUp ftco-animated">
						<div class="media block-6 services d-block">
							<div class="icon">
								<span class="flaticon-employee"></span>
							</div>
							<div class="media-body">
								<h3 class="heading mb-3">Search Expert Candidates</h3>
								<p>A small river named Duden flows by their place and
									supplies.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			</section>
			<section class="ftco-section">
			<div class="container">
				<div class="row justify-content-center mb-5 pb-3">
					<div
						class="col-md-7 heading-section text-center ftco-animate fadeInUp ftco-animated">
						<span class="subheading">Job Categories</span>
						<h2 class="mb-4">Top Categories</h2>
					</div>
				</div>
				<div class="row">
					<c:forEach items="${categoryList1 }" var="cat">
						<div class="col-md-3 ftco-animate fadeInUp ftco-animated">
							<ul class="category">
								<li><a href="./category?catId=${cat.id}">${cat.name } <br>
										<span class="number">${cat.availableJobCount }</span> <c:if
											test="${cat.availableJobCount <= 1}">
											<span>No new Job</span>
										</c:if> <c:if test="${cat.availableJobCount > 1}">
											<span>New Jobs</span>
										</c:if> <i class="ion-ios-arrow-forward"></i>
								</a></li>
							</ul>
						</div>
					</c:forEach>
				</div>
			</div>
			</section>
			<section class="ftco-section bg-light">
			<div class="container">
				<div class="row">
					<div class="col-lg-9 pr-lg-5">
						<div class="row justify-content-center pb-3">
							<div
								class="col-md-12 heading-section ftco-animate fadeInUp ftco-animated">
								<span class="subheading">Recently Added Jobs</span>
								<h2 class="mb-4">Hot Jobs</h2>
							</div>
						</div>
						<div class="row">
							
							<c:forEach items="${postList}" var="item">
								<c:if test="${item.vacancy >= 1}">
									<div class="col-md-12 ftco-animate fadeInUp ftco-animated">
										<div
											class="job-post-item py-4 d-block d-lg-flex align-items-center">
											<div class="one-third mb-4 mb-md-0">
												<div class="job-post-item-header d-flex align-items-center">
													<h2 class="mr-3 text-black">
														<a href="./jobdetail?postId=${item.id }">${item.nameJob }</a>
													</h2>
													<div class="badge-wrap">
														<c:if test="${item.type == 'Full Time'}">
															<span class="bg-danger text-white badge py-2 px-3">${item.type }</span>
														</c:if>
														<c:if test="${item.type == 'Part Time'}">
															<span class="bg-primary text-white badge py-2 px-3">${item.type }</span>
														</c:if>
														<c:if test="${item.type == 'Temporary'}">
															<span class="bg-warning text-white badge py-2 px-3">${item.type }</span>
														</c:if>
														<c:if test="${item.type == 'Freelance'}">
															<span class="bg-success text-white badge py-2 px-3">${item.type }</span>
														</c:if>
													</div>
												</div>
												<div class="job-post-item-body d-block d-md-flex">
													<div class="mr-3">
														<c:set var="salary" value="${item.salary }" />
														<span class="icon-money"></span> <a href="#"><fmt:formatNumber
																value="${salary}" type="currency" currencyCode="USD" /></a>
													</div>
													<div>
														<span class="icon-my_location"></span> <span>${item.company.address}</span>
													</div>
												</div>
											</div>
											<div
												class="one-forth ml-auto d-flex align-items-center mt-4 md-md-0">
												<div>
													<sec:authorize access="hasRole('CANDIDATE')">
														<sec:authentication property="principal.username" var="username" />
														<c:if test="${empty item.likeJobByList}">
															<a href="./likeJobPost?postId=${item.id}"
																class="icon text-center d-flex justify-content-center align-items-center icon mr-2">
																<span class="icon-heart"></span>
															</a>
														</c:if>
														
														<c:if test="${not empty item.likeJobByList}">
														
															<c:forEach items="${item.likeJobByList}" var="userLike" varStatus="status">
																<c:forEach items="${userLike.jobLikedByUserList}" var="job" varStatus="status1">
																		
																		<!-- Loc ra cac job cua user la principal -->
																		<c:if test="${(userLike.userName==username) && (job.id==item.id)}">
																			<c:set var="jobId" value="${job.id }"/>
																			<a href="./likeJobPost?postId=${item.id}"
																			class="icon text-center d-flex justify-content-center align-items-center icon mr-2">
																			<span class="icon-minus"></span>
																			</a>
																		</c:if>
																		
																</c:forEach>
															</c:forEach>
															
															<c:if test="${(item.id!=jobId)}">
																<a href="./likeJobPost?postId=${item.id}"
																	class="icon text-center d-flex justify-content-center align-items-center icon mr-2">
																	<span class="icon-heart"></span>
																</a>
															</c:if>
															
														</c:if>
													</sec:authorize>
												</div>
												<c:if test="${item.expired==false}">
													<a href="./applyjob?postId=${item.id }"
														class="btn btn-primary py-2">Apply Job</a>
												</c:if>

											</div>
										</div>
									</div>
								</c:if>
							</c:forEach>
						</div>

					</div>
				</div>
			</div>
			</section>
		</sec:authorize>

	</sec:authorize>


</body>
</html>