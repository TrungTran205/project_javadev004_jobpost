<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Post</title>
<style>
.buttonpaddingleft {
	padding-left: 5px;
	padding-right: 5px;
}

input:invalid {
	box-shadow: 0 0 5px 1px red;
}
</style>
</head>
<body>
	<div class="hero-wrap hero-wrap-2"
		style="background-image: url(https://content.thriveglobal.com/wp-content/uploads/2019/12/Job.jpg); background-position: 30% 0%;"
		data-stellar-background-ratio="0.5">
		<div class="container">
			<div
				class="row no-gutters slider-text align-items-end justify-content-start">
				<div
					class="col-md-8 ftco-animate text-center text-md-left mb-5 fadeInUp ftco-animated">
					<p class="breadcrumbs mb-0">
						<span class="mr-3"><a href="index.html">Home <i
								class="ion-ios-arrow-forward"></i></a></span> <span>Employer <i
							class="ion-ios-arrow-forward"></i></span> <span>New Post</span>
					</p>
					<h2 class="mb-3 bread">Add New Job Post</h2>
				</div>
			</div>
		</div>
	</div>
	<div class="ftco-section bg-light">
		<div class="container">
			<c:if test="${not empty errorMessage }">
				<div class="alert alert-danger" role="alert">${errorMessage}</div>
			</c:if>
			<form:form action="./savePost" modelAttribute="post" method="post">
				<hr />
				<h4>Job Details</h4>
				<hr />
				<form:input type="hidden" path="creator" value="${userName }" />
				<div class="form-group">
					<label for="nameJob">Job's Name</label>
					<form:input type="text" class="form-control" path="nameJob"
						id="nameJob" />
				</div>

				<div class="dropdown">
					<label for="type">Select Category:</label>
					<form:select path="category.id">
						<c:if test="${empty post.category }">
							<c:forEach items="${ categoryList }" var="cat">
								<option value="${ cat.id }">${ cat.name }</option>
							</c:forEach>
						</c:if>
						<c:if test="${not empty post.category }">
							<c:forEach items="${ categoryList }" var="cat">
								<c:if test="${post.category.id == cat.id }">
									<option selected="selected" value="${ cat.id }">${ cat.name }</option>
								</c:if>
								<c:if test="${post.category.id != cat.id }">
									<option value="${ cat.id }">${ cat.name }</option>
								</c:if>
							</c:forEach>
						</c:if>
					</form:select>
				</div>

				<div class="form-group">
					<label for="type">Type:</label>
					<div class="form-check form-check-inline">
						<form:radiobutton path="type" value="Full Time" />
						<div class="buttonpaddingleft">Full Time</div>
					</div>
					<div class="form-check form-check-inline">
						<form:radiobutton path="type" value="Part Time" />
						<div class="buttonpaddingleft">Part Time</div>
					</div>
					<div class="form-check form-check-inline">
						<form:radiobutton path="type" value="Freelance" />
						<div class="buttonpaddingleft">Freelance</div>
					</div>
					<div class="form-check form-check-inline">
						<form:radiobutton path="type" value="Temporary" />
						<div class="buttonpaddingleft">Temporary</div>
					</div>
				</div>

				<div class="form-group">
					<label for="vacancy">Vacancy:</label>
					<form:input type="text" class="form-control" path="vacancy"
						id="vacancy" />
				</div>

				<div class="form-group">
					<label for="description">Description:</label>
					<form:textarea class="form-control" path="description"
						id="description" rows="5" />
				</div>

				<div class="form-group">
					<label for="requirement">Requirements:</label>
					<form:textarea class="form-control" path="requirement"
						id="requirement" rows="5" />
				</div>

				<div class="form-group">
					<label for="salary">Salary:</label>
					<form:input type="text" class="form-control" path="salary"
						id="salary" />
				</div>

				<div class="form-group">
					<label for="endDay">Deadline:</label>
					<form:input type="date" class="form-control" path="endDay"
						id="endDay" required="true" />
				</div>

				<hr />
				<h4>Company</h4>
				<hr />

				<div class="dropdown">
					<label for="company.id">Select Company:</label>
					<form:select path="company.id">
						<option value="" disabled selected>-- Choose company --</option>
						<c:forEach items="${ companyList }" var="comp">
							<option value="${ comp.id }">${ comp.name }</option>
						</c:forEach>
					</form:select>
				</div>
				<div class="form-group">
					<p>
						Don't Have Company?
						<button type="submit" class="btn btn-outline-dark">New Company</button>
					</p>
				</div>

				<div>
					<button type="submit" class="btn btn-outline-dark">Create</button>
					<a href="./listPost" class="btn btn-outline-dark">Back</a>
				</div>
			</form:form>
		</div>
	</div>

</body>
</html>