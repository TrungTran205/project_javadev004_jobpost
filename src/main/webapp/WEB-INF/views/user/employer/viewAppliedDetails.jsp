<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Details of Candidate</title>
</head>
<body>
	<div class="hero-wrap hero-wrap-2"
		style="background-image: url(https://content.thriveglobal.com/wp-content/uploads/2019/12/Job.jpg); background-position: 30% 0%;"
		data-stellar-background-ratio="0.5">
		<div class="container">
			<div
				class="row no-gutters slider-text align-items-end justify-content-start">
				<div
					class="col-md-8 ftco-animate text-center text-md-left mb-5 fadeInUp ftco-animated">
					<p class="breadcrumbs mb-0">
						<span class="mr-3"><a href="index.html">Home <i
								class="ion-ios-arrow-forward"></i></a></span> <span>Applied Details</span>
					</p>
					<h2 class="mb-3 bread">Details of Candidate</h2>
				</div>
			</div>
		</div>
	</div>
	<div class="ftco-section bg-light">
		<div class="container">

			<div class="card">
				<h5 class="card-header">Information of Candidate</h5>
				<div class="card-body">
					<form:form enctype="multipart/form-data"
						modelAttribute="jobApplyModel" method="get">
						<h5 class="card-title">Name: ${apply.name}</h5>
						<p class="card-text">Email: ${apply.email }</p>
						<p class="card-text">
							Download CV: <a
								href="<c:url value="/uploads/${apply.fileName }" />"
								class="btn btn-primary">${apply.fileName }</a>
						</p>

					</form:form>
				</div>
				<div class="card-body">
					<a
						href="${pageContext.request.contextPath}/home/employer/listCandidates"
						class="btn btn-outline-dark">Back</a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>