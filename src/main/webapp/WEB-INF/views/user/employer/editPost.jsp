<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Edit Post</title>
</head>
<body>
	<div class="hero-wrap hero-wrap-2"
		style="background-image: url(https://content.thriveglobal.com/wp-content/uploads/2019/12/Job.jpg); background-position: 30% 0%;"
		data-stellar-background-ratio="0.5">
		<div class="container">
			<div
				class="row no-gutters slider-text align-items-end justify-content-start">
				<div
					class="col-md-8 ftco-animate text-center text-md-left mb-5 fadeInUp ftco-animated">
					<p class="breadcrumbs mb-0">
						<span class="mr-3"><a href="index.html">Home <i
								class="ion-ios-arrow-forward"></i></a></span> <span>Employer <i
							class="ion-ios-arrow-forward"></i></a></span> <span>Edit</span>
					</p>
					<h2 class="mb-3 bread">Update Post</h2>
				</div>
			</div>
		</div>
	</div>
	<div class="ftco-section bg-light">
		<div class="container">
			<form:form action="./updatePost" enctype="multipart/form-data"
				modelAttribute="post" method="post">
				<form:input type="hidden" id="id" path="id" value="${post.id}" />
				<form:input type="hidden" path="creator" value="${userName }" />
				<hr />
				<h4>Job Details</h4>
				<hr />
				<div class="form-group">
					<label for="nameJob">Job's Name</label>
					<form:input type="text" class="form-control" path="nameJob"
						id="nameJob" value="${post.nameJob}" />
				</div>

				<div class="dropdown">
					<label for="type">Select Category:</label>
					<form:select path="category.id">
						<c:forEach items="${ categoryList }" var="cat">
							<c:if test="${ post.category.id == cat.id }">
								<option selected="selected" value="${ cat.id }">${ cat.name }</option>
							</c:if>
							<c:if test="${ post.category.id != cat.id }">
								<option value="${ cat.id }">${ cat.name }</option>
							</c:if>
						</c:forEach>
					</form:select>
				</div>


				<div class="form-group">
					<label for="type">Type:</label>
					<div class="form-check form-check-inline">
						<form:radiobutton path="type" value="Full Time" label="Full Time" />
					</div>
					<div class="form-check form-check-inline">
						<form:radiobutton path="type" value="Part Time" label="Part Time" />
					</div>
					<div class="form-check form-check-inline">
						<form:radiobutton path="type" value="Freelance" label="Freelance" />
					</div>
					<div class="form-check form-check-inline">
						<form:radiobutton path="type" value="Temporary" label="Temporary" />
					</div>
				</div>

				<div class="form-group">
					<label for="description">Description:</label>
					<form:textarea class="form-control" path="description"
						id="description" value="${post.description}" />
				</div>

				<div class="form-group">
					<label for="requirement">Requirements:</label>
					<form:textarea type="text" class="form-control" path="requirement"
						id="requirement" value="${post.requirement}" />
				</div>

				<div class="form-group">
					<label for="salary">Salary:</label>
					<form:input type="text" class="form-control" path="salary"
						id="salary" value="${post.salary}" />
				</div>
				
				<div class="form-group">
					<label for="endDay">Deadline:</label>
					<form:input type="date" class="form-control" path="endDay"
						id="endDay" required="true" value="${post.endDay }"/>
				</div>

				<hr />
				<h4>Company</h4>
				<hr />

				<div class="dropdown">
					<label for="type">Select Company:</label>
					<form:select path="company.id">
						<c:forEach items="${ companyList }" var="comp">
							<c:if test="${ post.company.id == comp.id }">
								<option selected="selected" value="${ comp.id }">${ comp.name }</option>
							</c:if>
							<c:if test="${ post.company.id != comp.id }">
								<option value="${ comp.id }">${ comp.name }</option>
							</c:if>
						</c:forEach>
					</form:select>
				</div>

				<button type="submit" class="btn btn-outline-dark">Update</button>
			</form:form>

			<a href="./listPost" class="btn btn-outline-dark">Back</a>
		</div>
	</div>
</body>
</html>