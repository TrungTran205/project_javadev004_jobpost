<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View Details</title>
</head>
<body>
	<div class="hero-wrap hero-wrap-2"
		style="background-image: url(https://content.thriveglobal.com/wp-content/uploads/2019/12/Job.jpg); background-position: 30% 0%;"
		data-stellar-background-ratio="0.5">
		<div class="container">
			<div
				class="row no-gutters slider-text align-items-end justify-content-start">
				<div
					class="col-md-8 ftco-animate text-center text-md-left mb-5 fadeInUp ftco-animated">
					<p class="breadcrumbs mb-0">
						<span class="mr-3"><a href="index.html">Home <i
								class="ion-ios-arrow-forward"></i></a></span> <span>Details</span>
					</p>
					<h2 class="mb-3 bread">${post.nameJob}</h2>
				</div>
			</div>
		</div>
	</div>
	<section class="site-section" style="margin-top: 50px">
	<div class="container">
		<div class="row">
			<div class="col-lg-8">
				<div class="mb-5">
					<figure class="mb-5"> <img alt="" width="500" height="200"
						src="<c:url value="/uploads/${ post.company.imageName }" />" /></figure>
					<h3 class="h5 d-flex align-items-center mb-4 text-primary">
						<span class="icon-align-left mr-3"></span>Job Description
					</h3>
					<p>${post.description }</p>
				</div>

				<div class="mb-5">
					<h3 class="h5 d-flex align-items-center mb-4 text-primary">
						<span class="icon-book mr-3"></span>Education + Experience
					</h3>
					<ul class="list-unstyled m-0 p-0">
						<li class="d-flex align-items-start mb-2"><span
							class="icon-check_circle mr-2 text-muted"></span><span>${post.requirement }</span></li>
					</ul>
				</div>

				<div class="mb-5">
					<h3 class="h5 d-flex align-items-center mb-4 text-primary">
						<span class="icon-turned_in mr-3"></span>Other Benefits
					</h3>
					<ul class="list-unstyled m-0 p-0">
						<li class="d-flex align-items-start mb-2"><span
							class="icon-check_circle mr-2 text-muted"></span><span>${post.company.welfare }</span></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="bg-light p-3 border rounded mb-4">
					<img alt="${post.company.logoName }" width="200px" height="auto"
						src="<c:url value="/uploads/${ post.company.logoName }" />" />
					<h3 class="text-primary  mt-3 h5 pl-3 mb-3 ">${post.company.name }</h3>
					<ul class="list-unstyled pl-3 mb-0">
						<li class="mb-2">${post.company.info }</li>
						<li class="mb-2"><i class="fas fa-users"></i>
							${post.company.scale }</li>
						<li class="mb-2"><i class="fas fa-map-marked-alt"></i>
							${post.company.address }</li>
						<li class="mb-2"><i class="fas fa-envelope"></i>
							${post.company.email }</li>
					</ul>
				</div>
				<div class="bg-light p-3 border rounded mb-4">
					<h3 class="text-primary  mt-3 h5 pl-3 mb-3 ">Job Summary</h3>
					<ul class="list-unstyled pl-3 mb-0">
						<li class="mb-2"><strong class="text-black">
								Published on:</strong> ${post.postDay }</li>
						<li class="mb-2"><strong class="text-black">
								Vacancy:</strong> ${post.vacancy}</li>
						<li class="mb-2"><strong class="text-black">
								Employment Status:</strong> ${post.type }</li>
						<li class="mb-2"><strong class="text-black"> Job
								Location:</strong> ${post.company.address }</li>
						<li class="mb-2"><strong class="text-black"> Salary:</strong>
							$${post.salary }</li>
						<li class="mb-2"><strong class="text-black">
								Application Deadline:</strong> ${post.endDay}</li>
						<c:if test="${post.expired == true}">
							<h2 style="text-decoration: line-through; color: red">Expired!!!</h2>
						</c:if>
					</ul>
				</div>
			</div>
		</div>
		<sec:authorize access="hasRole('EMPLOYER')">
			<a class="btn btn-outline-dark" href="./listPost" role="button">
				<i class="far fa-arrow-alt-circle-left"></i> Back to List
			</a>
		</sec:authorize>
		<sec:authorize access="hasRole('CANDIDATE')">
			<a class="btn btn-outline-dark" href="./listJobApplied" role="button">
				<i class="far fa-arrow-alt-circle-left"></i> Back to List
			</a>
		</sec:authorize>
	</div>
	</section>
</body>