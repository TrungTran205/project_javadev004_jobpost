<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List of Post</title>
</head>
<body>
	<div class="hero-wrap hero-wrap-2"
		style="background-image: url(https://content.thriveglobal.com/wp-content/uploads/2019/12/Job.jpg); background-position: 30% 0%;"
		data-stellar-background-ratio="0.5">
		<div class="container">
			<div
				class="row no-gutters slider-text align-items-end justify-content-start">
				<div
					class="col-md-8 ftco-animate text-center text-md-left mb-5 fadeInUp ftco-animated">
					<p class="breadcrumbs mb-0">
						<span class="mr-3"><a href="index.html">Home <i
								class="ion-ios-arrow-forward"></i></a></span> <span>List Posts</span>
					</p>
					<sec:authorize access="hasRole('EMPLOYER')">
						<h2 class="mb-3 bread">View Your Job Posts</h2>
					</sec:authorize>
					<sec:authorize access="hasRole('CANDIDATE')">
						<h2 class="mb-3 bread">All Posts</h2>
					</sec:authorize>
					<sec:authorize access="!isAuthenticated()">
						<h2 class="mb-3 bread">All Posts</h2>
					</sec:authorize>
				</div>
			</div>
		</div>
	</div>
	<div class="ftco-section bg-light">
		<div class="container">
			<sec:authorize access="hasRole('EMPLOYER')">
				<h1>All Posts</h1>
				<form
					class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search pb-3"
					action="./searchYourPost" method="post">
					<div class="input-group">
						<input type="text" name="search" class="form-control"
							placeholder="Search" value="${searchValue}">
						<div class="input-group-append">
							<button class="btn btn-primary" type="submit">
								<i class="fas fa-search fa-sm"></i>
							</button>
						</div>
					</div>
				</form>
				<table class="table">
					<thead class="thead-dark">
						<tr>
							<th>Id</th>
							<th>Job's Name</th>
							<th>Category</th>
							<th>Type</th>
							<th>Vacancy</th>
							<th>Salary</th>
							<th>Creator</th>
							<sec:authorize access="hasRole('EMPLOYER')">
								<th>Edit</th>
								<th>Delete</th>
							</sec:authorize>
							<sec:authorize access="hasRole('EMPLOYEE')">
								<th>Apply</th>
							</sec:authorize>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${postList}" var="item">
							<tr>
								<td><a href="./viewJobDetails?postId=${item.id}">${item.id }</a></td>
								<td>${item.nameJob }</td>
								<th>${item.category.name }</th>
								<th>${item.type }</th>
								<th>${item.vacancy }</th>
								<c:set var="salary" value="${item.salary }"/>
								<td><fmt:formatNumber value = "${salary}" type = "currency" currencyCode="USD"/></td>
								<td>${item.creator }</td>
								<sec:authorize access="hasRole('EMPLOYER')">
									<c:if test="${item.deleted == false }">
										<td><a href="./editPost?postId=${ item.id }"><i
												class="far fa-edit"></i></a></td>
										<td><a href="./deletePost?postId=${ item.id }"> <i
												class="fas fa-trash-alt"> </i></a></td>
									</c:if>
									<c:if test="${item.deleted == true }">
										<td></td>
										<td>DELETED</td>
									</c:if>
								</sec:authorize>
								<sec:authorize access="hasRole('CANDIDATE')">
									<c:if test="${item.vacancy > 0} && ${item.expired==false }">
										<td><a href="./applyjob?postId=${ item.id }">Apply</a></td>
									</c:if>
								</sec:authorize>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<a class="btn btn-outline-dark" href="./newPost" role="button">Create
					Post</a>
			</sec:authorize>

			<sec:authorize access="hasRole('CANDIDATE')">
			<sec:authentication property="principal.username" var="username" scope="session"/>
				<form action="./searchPost" method="post" class="search-job">
					<div class="row no-gutters">
						<div class="col-md mr-md-2">
							<div class="form-group">
								<div class="form-field">
									<div class="select-wrap">
										<div class="icon">
											<span class="ion-ios-arrow-down"></span>
										</div>
										<select name="category" id="" class="form-control"
											value="${categoryValue}">
											<option disabled selected>Category</option>
											<c:forEach items="${categoryList }" var="cat">
												<option value="${cat.id }">${cat.name }</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md mr-md-2">
							<div class="form-group">
								<div class="form-field">
									<div class="select-wrap">
										<div class="icon">
											<span class="ion-ios-arrow-down"></span>
										</div>
										<select name="type" id="" class="form-control"
											value="${typeValue}">
											<option value="" disabled selected>Type</option>
											<option value="Full Time">Full Time</option>
											<option value="Part Time">Part Time</option>
											<option value="Freelance">Freelance</option>
											<option value="Temporary">Temporary</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md mr-md-2">
							<div class="form-group">
								<div class="form-field">
									<input type="text" class="form-control" name="name"
										placeholder="Enter Keyword..." value="${nameValue}" required="true">
								</div>
							</div>
						</div>
						<div class="col-md">
							<div class="form-group">
								<div class="form-field">
									<button type="submit" class="form-control btn btn-secondary">Search</button>
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="col-lg-9 pr-lg-5">
					<c:forEach items="${postList1}" var="item">
						<c:if test="${item.vacancy > 0}">
							<div class="col-md-12 ftco-animate fadeInUp ftco-animated">
								<div
									class="job-post-item py-4 d-block d-lg-flex align-items-center">
									<div class="one-third mb-4 mb-md-0">
										<div class="job-post-item-header d-flex align-items-center">
											<h2 class="mr-3 text-black">
												<a href="./jobdetail?postId=${item.id }">${item.nameJob }</a>
											</h2>
											<div class="badge-wrap">
												<c:if test="${item.type == 'Full Time'}">
													<span class="bg-danger text-white badge py-2 px-3">${item.type }</span>
												</c:if>
												<c:if test="${item.type == 'Part Time'}">
													<span class="bg-primary text-white badge py-2 px-3">${item.type }</span>
												</c:if>
												<c:if test="${item.type == 'Temporary'}">
													<span class="bg-warning text-white badge py-2 px-3">${item.type }</span>
												</c:if>
												<c:if test="${item.type == 'Freelance'}">
													<span class="bg-success text-white badge py-2 px-3">${item.type }</span>
												</c:if>

											</div>
										</div>
										<div class="job-post-item-body d-block d-md-flex">
											<div class="mr-3">
												<c:set var="salary01" value="${item.salary }"/>
												<span class="icon-money"></span> <span><fmt:formatNumber value = "${salary01}" type = "currency" currencyCode="USD"/></span>
											</div>
											<div>
												<span class="icon-my_location"></span> <span>${item.company.address}</span>
											</div>
										</div>
									</div>
									<div
										class="one-forth ml-auto d-flex align-items-center mt-4 md-md-0" style="margin:10px">
										<sec:authorize access="isAuthenticated()">
											<div>
												<sec:authentication property="principal.username" var="username" />
														<c:if test="${empty item.likeJobByList}">
															<a href="./likeJobPost?postId=${item.id}"
																class="icon text-center d-flex justify-content-center align-items-center icon mr-2">
																<span class="icon-heart"></span>
															</a>
														</c:if>
														
														<c:if test="${not empty item.likeJobByList}">
														
															<c:forEach items="${item.likeJobByList}" var="userLike" varStatus="status">
																<c:forEach items="${userLike.jobLikedByUserList}" var="job" varStatus="status1">
																		
																		<!-- Loc ra cac job cua user la principal -->
																		<c:if test="${(userLike.userName==username) && (job.id==item.id)}">
																			<c:set var="jobId" value="${job.id }"/>
																			<a href="./likeJobPost?postId=${item.id}"
																			class="icon text-center d-flex justify-content-center align-items-center icon mr-2">
																			<span class="icon-minus"></span>
																			</a>
																		</c:if>
																		
																</c:forEach>
															</c:forEach>
															
															<c:if test="${(item.id!=jobId)}">
																<a href="./likeJobPost?postId=${item.id}"
																	class="icon text-center d-flex justify-content-center align-items-center icon mr-2">
																	<span class="icon-heart"></span>
																</a>
															</c:if>
															
														</c:if>
													
											</div>
											<div>
												<c:if test="${item.expired==false}">
													<a href="${pageContext.request.contextPath}/home/applyjob?postId=${item.id }"
												class="btn btn-primary py-2">Apply Job</a>
												</c:if>
											</div>
										</sec:authorize>
									</div>
								</div>
							</div>
						</c:if>
					</c:forEach>
				</div>
			</sec:authorize>

			<sec:authorize access="!isAuthenticated()">
				<form action="./searchPost" method="post" class="search-job">
					<div class="row no-gutters">
						<div class="col-md mr-md-2">
							<div class="form-group">
								<div class="form-field">
									<div class="select-wrap">
										<div class="icon">
											<span class="ion-ios-arrow-down"></span>
										</div>
										<select name="category" id="" class="form-control"
											value="${categoryValue}">
											<option disabled selected>Category</option>
											<c:forEach items="${categoryList }" var="cat">
												<option value="${cat.id }">${cat.name }</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md mr-md-2">
							<div class="form-group">
								<div class="form-field">
									<div class="select-wrap">
										<div class="icon">
											<span class="ion-ios-arrow-down"></span>
										</div>
										<select name="type" id="" class="form-control"
											value="${typeValue}">
											<option value="" disabled selected>Type</option>
											<option value="Full Time">Full Time</option>
											<option value="Part Time">Part Time</option>
											<option value="Freelance">Freelance</option>
											<option value="Temporary">Temporary</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md mr-md-2">
							<div class="form-group">
								<div class="form-field">
									<input type="text" class="form-control" name="name"
										placeholder="Enter Keyword..." value="${nameValue}">
								</div>
							</div>
						</div>
						<div class="col-md">
							<div class="form-group">
								<div class="form-field">
									<button type="submit" class="form-control btn btn-secondary">Search</button>
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="col-lg-9 pr-lg-5">
					<c:forEach items="${postList1}" var="item">
						<c:if test="${item.vacancy > 0}">
							<div class="col-md-12 ftco-animate fadeInUp ftco-animated">
								<div
									class="job-post-item py-4 d-block d-lg-flex align-items-center">
									<div class="one-third mb-4 mb-md-0">
										<div class="job-post-item-header d-flex align-items-center">
											<h2 class="mr-3 text-black">
												<a href="./jobdetail?postId=${item.id }">${item.nameJob }</a>
											</h2>
											<div class="badge-wrap" style="margin:auto 10px">
												<c:if test="${item.type == 'Full Time'}">
													<span class="bg-danger text-white badge py-2 px-3">${item.type }</span>
												</c:if>
												<c:if test="${item.type == 'Part Time'}">
													<span class="bg-primary text-white badge py-2 px-3">${item.type }</span>
												</c:if>
												<c:if test="${item.type == 'Temporary'}">
													<span class="bg-warning text-white badge py-2 px-3">${item.type }</span>
												</c:if>
												<c:if test="${item.type == 'Freelance'}">
													<span class="bg-success text-white badge py-2 px-3">${item.type }</span>
												</c:if>

											</div>
										</div>
										<div class="job-post-item-body d-block d-md-flex">
											<div class="mr-3">
												<c:set var="balance" value="${item.salary }"/>
												<span class="icon-money"></span> <span><fmt:formatNumber value = "${balance}" type = "currency" currencyCode="USD"/></span>
											</div>
											<div>
												<span class="icon-my_location"></span> <span>${item.company.address}</span>
											</div>
										</div>
									</div>
									<c:if test="${item.expired==false }">
									<a href="${pageContext.request.contextPath}/login"
										class="btn btn-primary py-2">Apply Job</a>
									</c:if>
								</div>
							</div>
						</c:if>
					</c:forEach>
				</div>
			</sec:authorize>
		</div>
	</div>
</body>