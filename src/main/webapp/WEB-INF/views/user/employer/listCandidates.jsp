<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List Candidates</title>
</head>
<body>
	<div class="hero-wrap hero-wrap-2"
		style="background-image: url(https://content.thriveglobal.com/wp-content/uploads/2019/12/Job.jpg); background-position: 30% 0%;"
		data-stellar-background-ratio="0.5">
		<div class="container">
			<div
				class="row no-gutters slider-text align-items-end justify-content-start">
				<div
					class="col-md-8 ftco-animate text-center text-md-left mb-5 fadeInUp ftco-animated">
					<p class="breadcrumbs mb-0">
						<span class="mr-3"><a href="index.html">Home <i
								class="ion-ios-arrow-forward"></i></a></span> <span>Candidates</span>
					</p>
					<h2 class="mb-3 bread">List Candidates</h2>
				</div>
			</div>
		</div>
	</div>
	<div class="ftco-section bg-light">
		<div class="container">
			<form
				class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search pb-3"
				action="./searchCandidate" method="post">
				<div class="input-group">
					<input type="text" name="search" class="form-control"
						placeholder="Search" value="${searchValue}">
					<div class="input-group-append">
						<button class="btn btn-primary" type="submit">
							<i class="fas fa-search fa-sm"></i>
						</button>
					</div>
				</div>
			</form>
			<table class="table">
				<thead class="thead-dark">
					<tr>
						<th>Id</th>
						<th>Name</th>
						<th>Email</th>
						<th>Apply Job</th>
						<th>Status</th>
						<sec:authorize access="hasRole('EMPLOYER')">
							<th>ACCEPT</th>
							<th>DENY</th>
						</sec:authorize>
					</tr>
				</thead>
				<c:forEach items="${applyList}" var="item">
					<tr>
						<td><a href="./viewAppliedDetails?id=${item.id }">${item.id }</a></td>
						<td>${item.name }</td>
						<td>${item.email}</td>
						<td>${item.post.nameJob}</td>
						<c:if test="${empty item.status}">
							<td>PENDING</td>
						</c:if>
						<c:if test="${not empty item.status}">
							<td>${item.status }</td>
						</c:if>

						<sec:authorize access="hasRole('EMPLOYER')">
							<c:if test="${empty item.status}">
								<td><a href="./accept?id=${item.id}">Accept</a></td>
								<td><a href="./deny?id=${item.id}">Deny</a></td>
							</c:if>
							<c:if test="${not empty item.status}">
								<td></td>
								<td></td>
							</c:if>
						</sec:authorize>
					</tr>
				</c:forEach>
			</table>
			<sec:authorize access="hasRole('CANDIDATE')">
				<a class="btn btn-outline-dark" href="./newResume" role="button">
					Create Resume</a>
			</sec:authorize>
		</div>
	</div>
</body>
</html>