<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create Company</title>
</head>
<body>
	<div class="hero-wrap hero-wrap-2"
		style="background-image: url(https://content.thriveglobal.com/wp-content/uploads/2019/12/Job.jpg); background-position: 30% 0%;"
		data-stellar-background-ratio="0.5">
		<div class="container">
			<div
				class="row no-gutters slider-text align-items-end justify-content-start">
				<div
					class="col-md-8 ftco-animate text-center text-md-left mb-5 fadeInUp ftco-animated">
					<p class="breadcrumbs mb-0">
						<span class="mr-3"><a href="index.html">Home <i
								class="ion-ios-arrow-forward"></i></a></span> <span>Employer <i
							class="ion-ios-arrow-forward"></i></span> <span>New Post</span>
					</p>
					<h2 class="mb-3 bread">Add New Company</h2>
				</div>
			</div>
		</div>
	</div>
	<div class="ftco-section bg-light">
		<div class="container">
			<p class="card-text">
				<c:if test="${not empty errorMessage }">
					<div class="alert alert-danger" role="alert">${errorMessage}</div>
				</c:if>
				<form:form action="./saveCompany" enctype="multipart/form-data"
					modelAttribute="company" method="post">

					<div class="form-group">
						<form:input type="hidden" class="form-control" path="fromJobPost"
							value="${fromJobPost}" />
					</div>

					<div class="form-group">
						<label for="name">Your Company:</label>
						<form:input type="text" class="form-control" path="name" id="name" />
					</div>

					<div class="form-group">
						<label for="info">Info of Company:</label>
						<form:textarea class="form-control" path="info" id="info" rows="5" />
					</div>

					<div class="form-group">
						<label for="logoImage">Upload Logo Image</label>
						<form:input type="file" path="logoFile" class="form-control-file"
							id="logoImage" />
					</div>

					<div class="form-group">
						<label for="companyImage">Upload Company Image</label>
						<form:input type="file" path="imageFile" class="form-control-file"
							id="companyImage" />
					</div>

					<div class="dropdown">
						<label for="scale">Scale:</label>
						<form:select path="scale" id="scale">
							<option value="< 100">Below 100</option>
							<option value="100 - 200">100 - 200</option>
							<option value="201 - 300">201 - 300</option>
							<option value="300 - 400">301 - 400</option>
							<option value="400 - 500">401 - 500</option>
							<option value="> 500">Over 500</option>
						</form:select>
					</div>

					<div class="form-group">
						<label for="welfare">Welfare:</label>
						<form:textarea class="form-control" path="welfare"
							id="company.welfare" rows="5" />
					</div>

					<div class="form-group">
						<label for="address">Address of comp.:</label>
						<form:input type="text" class="form-control" path="address"
							id="address" />
					</div>

					<div class="form-group">
						<label for="email">Email address</label>
						<form:input type="email" class="form-control" id="email"
							path="email" placeholder="Enter email" />
					</div>

					<button type="submit" class="btn btn-outline-dark">Create</button>
					<a href="${pageContext.request.contextPath}/home/employer/newPost" class="btn btn-outline-dark">Back</a>
				</form:form>

			</p>
		</div>
	</div>

</body>
</html>