<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>
<link href="<c:url value='/resources/css/style.css' />" rel="stylesheet"></link>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="hero-wrap hero-wrap-2"
		style="background-image: url(https://content.thriveglobal.com/wp-content/uploads/2019/12/Job.jpg); background-position: 30% 0%;"
		data-stellar-background-ratio="0.5">
		<div class="container">
			<div
				class="row no-gutters slider-text align-items-end justify-content-start">
				<div
					class="col-md-8 ftco-animate text-center text-md-left mb-5 fadeInUp ftco-animated">
					<p class="breadcrumbs mb-0">
						<span class="mr-3"><a href="index.html">Home <i
								class="ion-ios-arrow-forward"></i></a></span> <span>Employer</span>
					</p>
					<h2 class="mb-3 bread">YOUR HOME</h2>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<ul class="nav nav-pills mb-3 nav-justified" id="pills-tab"
			role="tablist">
			<li class="nav-item"><a class="nav-link active"
				id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
				aria-controls="pills-home" aria-selected="true">Home</a></li>
			<li class="nav-item"><a class="nav-link" id="pills-profile-tab"
				data-toggle="pill" href="#pills-profile" role="tab"
				aria-controls="pills-profile" aria-selected="false">My Posts</a></li>
			<li class="nav-item"><a class="nav-link" id="pills-contact-tab"
				data-toggle="pill" href="#pills-contact" role="tab"
				aria-controls="pills-contact" aria-selected="false">Candidates</a></li>
		</ul>
		<div class="tab-content" id="pills-tabContent">
			<div class="tab-pane fade show active" id="pills-home"
				role="tabpanel" aria-labelledby="pills-home-tab">
				<section class="ftco-section bg-light">
				<div class="container">
					<div class="row">
						<div class="col-lg-9 pr-lg-5">
							<div class="row justify-content-center pb-3">
								<div
									class="col-md-12 heading-section ftco-animate fadeInUp ftco-animated">
									<span class="subheading">Recently Added Resume</span>
									<h2 class="mb-4">Hot CV</h2>
								</div>
							</div>
							<div class="row">
								<c:forEach items="${resumeList}" var="item">
									<div class="col-md-12 ftco-animate fadeInUp ftco-animated">
										<div
											class="job-post-item py-4 d-block d-lg-flex align-items-center">
											<div class="one-third mb-4 mb-md-0">
												<div class="job-post-item-header d-flex align-items-center">
													<h2 class="mr-3 text-black">
														<a href="./resumedetail?resumeId=${item.id }">${item.firstname }
															${item.lastname }</a>
													</h2>
												</div>
												<div class="job-post-item-body d-block d-md-flex">
													<div class="mr-3">
														<span class="icon-mail_outline"></span> <a href="#">${item.email}</a>
													</div>
													<div>
														<span class="icon-phone"></span> <span>${item.phonenumber}</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</c:forEach>
							</div>

						</div>
					</div>
				</div>
				</section>
			</div>
			<div class="tab-pane fade" id="pills-profile" role="tabpanel"
				aria-labelledby="pills-profile-tab">
				<h1>All Posts</h1>
				<table class="table">
					<thead class="thead-dark">
						<tr>
							<th>Id</th>
							<th>Job's Name</th>
							<th>Category</th>
							<th>Type</th>
							<th>Vacancy</th>
							<th>Salary</th>
							<th>Creator</th>
							<sec:authorize access="hasRole('EMPLOYER')">
								<th>Edit</th>
								<th>Delete</th>
							</sec:authorize>
							<sec:authorize access="hasRole('EMPLOYEE')">
								<th>Apply</th>
							</sec:authorize>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${postList}" var="item">
							<tr>
								<td><a href="./viewDetails?postId=${item.id}">${item.id }</a></td>
								<td>${item.nameJob }</td>
								<th>${item.category.name }</th>
								<th>${item.type }</th>
								<th>${item.vacancy }</th>
								<td>${item.salary }</td>
								<td>${item.creator }</td>
								<sec:authorize access="hasRole('EMPLOYER')">
									<td><a href="./editPost?postId=${ item.id }"><i
											class="far fa-edit"></i></a></td>
									<td><a href="./deletePost?postId=${ item.id }"> <i
											class="fas fa-trash-alt"> </i></a></td>
								</sec:authorize>
								<sec:authorize access="hasRole('EMPLOYEE')">
									<td><a href="./applyjob?postId=${ item.id }">Apply</a></td>
								</sec:authorize>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<sec:authorize access="hasRole('EMPLOYER')">
					<a class="btn btn-outline-dark" href="./newPost" role="button">Create
						Post</a>
				</sec:authorize>
			</div>
			<div class="tab-pane fade" id="pills-contact" role="tabpanel"
				aria-labelledby="pills-contact-tab">
				<h2>List Candidates</h2>
				<br>
				<table class="table">
					<thead class="thead-dark">
						<tr>
							<th>Id</th>
							<th>Name</th>
							<th>Email</th>
							<th>Apply Job</th>
							<sec:authorize access="hasRole('EMPLOYER')">
								<th>ACCEPT</th>
								<th>DENIED</th>
							</sec:authorize>
						</tr>
					</thead>
					<c:forEach items="${applyList}" var="item">
						<tr>
							<td><a href="./viewAppliedDetails?id=${item.id }">${item.id }</a></td>
							<td>${item.name }</td>
							<td>${item.email}</td>
							<td>${item.post.nameJob}</td>
							<sec:authorize access="hasRole('EMPLOYER')">
								<td><a href="">Accept</a></td>
								<td><a href="">Denied</a></td>
							</sec:authorize>
						</tr>
					</c:forEach>
				</table>
			</div>
		</div>
	</div>
</body>
</html>