<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create CV</title>
<style>
input:invalid {
	box-shadow: 0 0 5px 1px red;
}
</style>
</head>
<body>
	<div class="hero-wrap hero-wrap-2"
		style="background-image: url(https://content.thriveglobal.com/wp-content/uploads/2019/12/Job.jpg); background-position: 30% 0%;"
		data-stellar-background-ratio="0.5">
		<div class="container">
			<div
				class="row no-gutters slider-text align-items-end justify-content-start">
				<div
					class="col-md-8 ftco-animate text-center text-md-left mb-5 fadeInUp ftco-animated">
					<p class="breadcrumbs mb-0">
						<span class="mr-3"><a href="index.html">Home <i
								class="ion-ios-arrow-forward"></i></a></span> <span>Resume</span>
					</p>
					<h2 class="mb-3 bread">New Resume</h2>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="card">
			<div class="card-body">
				<c:if test="${not empty errorMessage}">
					<div class="alert alert-danger" role="alert">${errorMessage}</div>
				</c:if>
				<form:form modelAttribute="resume" enctype="multipart/form-data"
					action="./saveResume" method="post">
					<form:input type="hidden" path="creator" value="${userName }" />

					<div class="form-group">
						<label for="resumeImage">Your Image</label> <img alt=""
							width="200" height="200" src="<c:url value="" />" />
						<form:input type="file" path="imageFile" class="form-control-file"
							id="profileImage" />
					</div>

					<div class="form-group">
						<label for="firstname">First Name: </label>
						<form:input type="text" class="form-control" path="firstname"
							id="firstname" />
					</div>

					<div class="form-group">
						<label for="lastname">Last Name: </label>
						<form:input type="text" class="form-control" path="lastname"
							id="lastname" />
					</div>

					<div class="form-group">
						<label for="dob">Day of birth: </label>
						<form:input type="date" class="form-control" path="dob" id="dob"
							required="true" />
					</div>

					<div class="form-group">
						<label for="phonenumber">Phone Number: </label>
						<form:input type="text" class="form-control" path="phonenumber"
							id="phonenumber" />
					</div>

					<div class="form-group">
						<label for="email">Email: </label>
						<form:input type="email" class="form-control" id="email"
							path="email" placeholder="Enter email" />
					</div>

					<div class="form-group">
						<label for="address">Address: </label>
						<form:input type="text" class="form-control" path="address"
							id="address" />
					</div>

					<div class="form-group">
						<label for="experiences">Experiences: </label>
						<form:textarea class="form-control" path="experiences"
							id="experiences" rows="5" />
					</div>

					<div class="form-group">
						<label for="skills">Skills: </label>
						<form:textarea class="form-control" path="skills" id="skills"
							rows="5" />
					</div>

					<button type="submit" class="btn btn-outline-dark">Create</button>
					<a href="./listResume" class="btn btn-outline-dark">Back</a>
				</form:form>
			</div>
		</div>
	</div>
</body>
</html>