<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>EDIT RESUME</title>
</head>
<body>
	<h2>Update Resume</h2>
	<form:form method="post" action="./updateResume">

		<form:input type="hidden" id="resumeId" path="resumeId"
			value="${resume.id}" />

		<div class="form-group">
			<label for="firstname">First Name</label>
			<form:input type="text" class="form-control" path="firstname"
				id="firstname" />
		</div>

		<div class="form-group">
			<label for="lastname">Last Name</label>
			<form:input type="text" class="form-control" path="lastName"
				id="lastName" />
		</div>

		<div class="form-group">
			<label for="email">Email address</label>
			<form:input type="email" class="form-control" id="email" path="email"
				placeholder="Enter email" />
		</div>

		<div class="form-group">
			<label for="phonenumber">Phone Number</label>
			<form:input type="text" class="form-control" path="phonenumber"
				id="phonenumber" />
		</div>

		<div class="form-group">
			<label for="dob">Day of birth:</label>
			<form:input type="date" class="form-control" path="dob" id="dob" />
		</div>

		<div class="form-group">
			<label for="experiences">Experiences:</label>
			<form:textarea class="form-control" path="experiences"
				id="experiences" rows="5" />
		</div>

		<div class="form-group">
			<label for="skills">Skills:</label>
			<form:textarea class="form-control" path="skills" id="skills"
				rows="5" />
		</div>



		<button type="submit" class="btn btn-outline-dark">Update</button>
		<a href="./listResume" class="btn btn-outline-dark">Back</a>
	</form:form>
</body>
</html>