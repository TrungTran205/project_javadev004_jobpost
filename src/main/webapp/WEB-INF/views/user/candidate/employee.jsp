<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>
<link href="<c:url value='/resources/css/style.css' />" rel="stylesheet"></link>
</head>
<body>
	<div class="hero-wrap hero-wrap-2"
		style="background-image: url(https://content.thriveglobal.com/wp-content/uploads/2019/12/Job.jpg); background-position: 30% 0%;"
		data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div
				class="row no-gutters slider-text align-items-end justify-content-start">
				<div
					class="col-md-8 ftco-animate text-center text-md-left mb-5 fadeInUp ftco-animated">
					<p class="breadcrumbs mb-0">
						<span class="mr-3"><a href="index.html">Home <i
								class="ion-ios-arrow-forward"></i></a></span> <span>Employee</span>
					</p>
					<h2 class="mb-3 bread">YOUR HOME</h2>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<ul class="nav nav-pills mb-3 nav-justified" id="pills-tab" role="tablist">
			<li class="nav-item"><a class="nav-link active"
				id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
				aria-controls="pills-home" aria-selected="true">Home</a></li>
			<li class="nav-item"><a class="nav-link" id="pills-profile-tab"
				data-toggle="pill" href="#pills-profile" role="tab"
				aria-controls="pills-profile" aria-selected="false">My CV</a></li>
			<li class="nav-item"><a class="nav-link" id="pills-contact-tab"
				data-toggle="pill" href="#pills-contact" role="tab"
				aria-controls="pills-contact" aria-selected="false">My Jobs</a></li>
		</ul>
		<div class="tab-content" id="pills-tabContent">
			<div class="tab-pane fade show active" id="pills-home"
				role="tabpanel" aria-labelledby="pills-home-tab">
				<section class="ftco-section bg-light">
				<div class="container">
					<div class="row">
						<div class="col-lg-9 pr-lg-5">
							<div class="row justify-content-center pb-3">
								<div
									class="col-md-12 heading-section ftco-animate fadeInUp ftco-animated">
									<span class="subheading">Recently Added Jobs</span>
									<h2 class="mb-4">Hot Jobs</h2>
								</div>
							</div>
							<div class="row">
								<c:forEach items="${postList}" var="item">
									<div class="col-md-12 ftco-animate fadeInUp ftco-animated">
										<div
											class="job-post-item py-4 d-block d-lg-flex align-items-center">
											<div class="one-third mb-4 mb-md-0">
												<div class="job-post-item-header d-flex align-items-center">
													<h2 class="mr-3 text-black">
														<a href="./jobdetail?postId=${item.id }">${item.nameJob }</a>
													</h2>
													<div class="badge-wrap">
														<span class="bg-primary text-white badge py-2 px-3">${item.type }</span>
													</div>
												</div>
												<div class="job-post-item-body d-block d-md-flex">
													<div class="mr-3">
														<span class="icon-money"></span> <a href="#">${item.salary}</a>
													</div>
													<div>
														<span class="icon-my_location"></span> <span>${item.company.address}</span>
													</div>
												</div>
											</div>
											<div
												class="one-forth ml-auto d-flex align-items-center mt-4 md-md-0">
												<a href="./applyjob?postId=${item.id }"
													class="btn btn-primary py-2">Apply Job</a>
											</div>
										</div>
									</div>
								</c:forEach>
							</div>

						</div>
					</div>
				</div>
				</section>
			</div>
			<div class="tab-pane fade" id="pills-profile" role="tabpanel"
				aria-labelledby="pills-profile-tab">...</div>
			<div class="tab-pane fade" id="pills-contact" role="tabpanel"
				aria-labelledby="pills-contact-tab">...</div>
		</div>
	</div>

</body>
</html>