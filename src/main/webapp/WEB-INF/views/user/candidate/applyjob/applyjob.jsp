<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Apply Job</title>
</head>
<body>
	<div class="hero-wrap hero-wrap-2"
		style="background-image: url(https://content.thriveglobal.com/wp-content/uploads/2019/12/Job.jpg); background-position: 30% 0%;"
		data-stellar-background-ratio="0.5">
		<div class="container">
			<div
				class="row no-gutters slider-text align-items-end justify-content-start">
				<div
					class="col-md-8 ftco-animate text-center text-md-left mb-5 fadeInUp ftco-animated">
					<p class="breadcrumbs mb-0">
						<span class="mr-3"><a href="index.html">Home <i
								class="ion-ios-arrow-forward"></i></a></span> <span>Apply</span>
					</p>
					<h2 class="mb-3 bread">Apply ${post.nameJob }</h2>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="card">
			<div class="card-body">
				<c:if test="${not empty errorMessage }">
					<div class="alert alert-danger" role="alert">${errorMessage}</div>
				</c:if>
				<form:form enctype="multipart/form-data" action="./applyjob"
					modelAttribute="jobApplyModel" method="post">
					<form:input type="hidden" path="jobPostId" value="${post.id }" />
					<div class="form-group">
						<label for="name">Your Name:</label>
						<form:input type="text" class="form-control" path="name" />
					</div>
					<div class="form-group">
						<label for="name">Your Email:</label>
						<form:input type="email" class="form-control" path="email"
							placeholder="Enter email..." />
					</div>
					<div class="form-group">

						<label for="profileImage">Your CV</label>
						<form:input type="file" path="cvImageFile" class="form-control-file"
							id="yourCV" />
					</div>
					<button type="submit" class="btn btn-outline-dark">Send My
						CV</button>
				</form:form>
			</div>
		</div>
	</div>
</body>
</html>