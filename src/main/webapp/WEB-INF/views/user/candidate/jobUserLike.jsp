<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Saved Jobs</title>
</head>
<body>
	<div class="hero-wrap hero-wrap-2"
		style="background-image: url(https://content.thriveglobal.com/wp-content/uploads/2019/12/Job.jpg); background-position: 30% 0%;"
		data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div
				class="row no-gutters slider-text align-items-end justify-content-start">
				<div
					class="col-md-8 ftco-animate text-center text-md-left mb-5 fadeInUp ftco-animated">
					<p class="breadcrumbs mb-0">
						<span class="mr-3"><a
							href="${pageContext.request.contextPath}/home/">Home <i
								class="ion-ios-arrow-forward"></i></a></span> <span>Saved Jobs</span>
					</p>
					<h2 class="mb-3 bread">All Jobs You Liked</h2>
				</div>
			</div>
		</div>
	</div>

	<section class="ftco-section bg-light">
	<div class="container">
		<c:if test="${empty postList}">
			<h4>You didn't like any jobs recently.</h4>
		</c:if>
		<c:if test="${not empty postList}">
			<div class="row">
				<div class="col-lg-9 pr-lg-5">
					<c:forEach items="${postList}" var="item">
						<c:if test="${item.vacancy > 0}">
							<div class="col-md-12 ftco-animate fadeInUp ftco-animated">
								<div
									class="job-post-item py-4 d-block d-lg-flex align-items-center">
									<div class="one-third mb-4 mb-md-0">
										<div class="job-post-item-header d-flex align-items-center">
											<h2 class="mr-3 text-black">
												<a href="./jobdetail?postId=${item.id }">${item.nameJob }</a>
											</h2>
											<div class="badge-wrap">
												<c:if test="${item.type == 'Full Time'}">
													<span class="bg-danger text-white badge py-2 px-3">${item.type }</span>
												</c:if>
												<c:if test="${item.type == 'Part Time'}">
													<span class="bg-primary text-white badge py-2 px-3">${item.type }</span>
												</c:if>
												<c:if test="${item.type == 'Temporary'}">
													<span class="bg-warning text-white badge py-2 px-3">${item.type }</span>
												</c:if>
												<c:if test="${item.type == 'Freelance'}">
													<span class="bg-success text-white badge py-2 px-3">${item.type }</span>
												</c:if>

											</div>
										</div>
										<div class="job-post-item-body d-block d-md-flex">
											<div class="mr-3">
												<span class="icon-money"></span> <a href="#">${item.salary}</a>
											</div>
											<div>
												<span class="icon-my_location"></span> <span>${item.company.address}</span>
											</div>
										</div>
									</div>
									<div
										class="one-forth ml-auto d-flex align-items-center mt-4 md-md-0">
										<div>
											<a href="./likeJobPost?postId=${item.id}"
												class="icon text-center d-flex justify-content-center align-items-center icon mr-2">
												<span class="icon-minus"></span>
											</a>
										</div>
										<a href="./applyjob?postId=${item.id }"
											class="btn btn-primary py-2">Apply Job</a>
									</div>
								</div>
							</div>
						</c:if>
					</c:forEach>
				</div>
			</div>
		</c:if>
	</div>
	</section>
</body>
</html>