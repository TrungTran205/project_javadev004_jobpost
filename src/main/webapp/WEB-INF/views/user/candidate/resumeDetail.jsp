<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Resume's Detail</title>
</head>
<body>
	<div class="hero-wrap hero-wrap-2"
		style="background-image: url(https://content.thriveglobal.com/wp-content/uploads/2019/12/Job.jpg); background-position: 30% 0%;"
		data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container"></div>
	</div>
	<div class="container">
		<div class="card border-primary mb-3" style="width: 400px;">
			<div class="card-header">CV</div>
			<div class="card-body text-primary">
				<h5 class="card-title">Name:</h5>
				<p class="card-text">${resume.firstname }${resume.lastname }</p>
				<h5 class="card-title">Email:</h5>
				<p class="card-text">${resume.email }</p>
				<h5 class="card-title">Phone:</h5>
				<p class="card-text">(+84)${resume.phonenumber }</p>
				<h5 class="card-title">Day of birth:</h5>
				<p class="card-text">${resume.dob }</p>
				<h5 class="card-title">Experiences:</h5>
				<p class="card-text">${resume.experiences }</p>
				<h5 class="card-title">Skills:</h5>
				<p class="card-text">${resume.skills }</p>
			</div>
			<a class="btn btn-outline-dark"
				href="./editResume?resumeId=${ item.id }"><i class="far fa-edit"></i> Edit</a>
		</div>
	</div>
</body>