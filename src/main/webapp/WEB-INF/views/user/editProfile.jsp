<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Saved Jobs</title>
</head>
<body>
	<div class="hero-wrap hero-wrap-2"
		style="background-image: url(https://content.thriveglobal.com/wp-content/uploads/2019/12/Job.jpg); background-position: 30% 0%;"
		data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div
				class="row no-gutters slider-text align-items-end justify-content-start">
				<div
					class="col-md-8 ftco-animate text-center text-md-left mb-5 fadeInUp ftco-animated">
					<p class="breadcrumbs mb-0">
						<span class="mr-3"><a
							href="${pageContext.request.contextPath}/home/">Home <i
								class="ion-ios-arrow-forward"></i></a></span> <span>Profile</span>
					</p>
					<h2 class="mb-3 bread">Edit</h2>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="card">
			<div class="card-header">
				<h3>Your Profile</h3>
			</div>
			<div class="card-body">
				<form:form enctype="multipart/form-data" action="./updateProfile"
					modelAttribute="user" method="post">
					<c:if test="${not empty errorMessage }">
						<div class="alert alert-danger" role="alert">${errorMessage}</div>
					</c:if>
					<form:input type="hidden" path="id" value="${user.id }" />
					<form:input type="hidden" path="userType"
						value="${user.userType.id }" />
					<c:forEach items="${user.permissionList}" var="item">
						<form:input type="hidden" path="permissionList"
							value="${item.id }" />
					</c:forEach>
					<div class="form-group">
						<label for="profileImage">Upload Profile Image</label> <img alt=""
							width="200" height="200"
							src="<c:url value="/uploads/${ user.imageName }" />" />
						<form:input type="file" path="imageFile" class="form-control-file"
							id="profileImage" />
					</div>

					<div class="form-group">
						<label for="firstName">First Name:</label>
						<form:input type="text" class="form-control" path="firstName"
							value="${user.firstName}" />
					</div>

					<div class="form-group">
						<label for="lastName">Last Name:</label>
						<form:input type="text" class="form-control" path="lastName"
							value="${user.lastName}" />
					</div>

					<div class="form-group">
						<label for="lastName">Username:</label>
						<form:input type="text" class="form-control" path="userName"
							value="${user.userName}" />
					</div>

					<div class="form-group">
						<label for="name">Birthday:</label>
						<form:input type="date" class="form-control" path="dob"
							value="${user.dob}" />
					</div>

					<div class="form-group">
						<label for="name">Phone:</label>
						<form:input type="text" class="form-control" path="phoneNumber"
							value="${user.phoneNumber}" />
					</div>

					<div class="form-group">
						<label for="name">Email:</label>
						<form:input type="email" class="form-control" path="email"
							value="${user.email}" />
					</div>
					<%-- <sec:authorize access="hasRole('CANDIDATE')">
						<div class="form-group">
							<label for="name">Experiences:</label>
							<form:input type="text" class="form-control" path="experiences"
								value="${user.experiences}" />
						</div>

						<div class="form-group">
							<label for="name">Skills:</label>
							<form:input type="text" class="form-control" path="skills"
								value="${user.skills}" />
						</div>
					</sec:authorize> --%>
					<button type="submit" class="btn btn-outline-dark">Save</button>
				</form:form>
			</div>
		</div>
	</div>
</body>
</html>