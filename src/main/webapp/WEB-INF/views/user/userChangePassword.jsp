<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Saved Jobs</title>
</head>
<body>
	<div class="hero-wrap hero-wrap-2"
		style="background-image: url(https://content.thriveglobal.com/wp-content/uploads/2019/12/Job.jpg); background-position: 30% 0%;"
		data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div
				class="row no-gutters slider-text align-items-end justify-content-start">
				<div
					class="col-md-8 ftco-animate text-center text-md-left mb-5 fadeInUp ftco-animated">
					<p class="breadcrumbs mb-0">
						<span class="mr-3"><a
							href="${pageContext.request.contextPath}/home/">Home <i
								class="ion-ios-arrow-forward"></i></a></span> <span>Profile</span>
					</p>
					<h2 class="mb-3 bread">Change Password</h2>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="card">
			<c:if test="${not empty errorMessage }">
				<div class="alert alert-danger" role="alert">${errorMessage}</div>
			</c:if>
			<div class="card-body">
				<form:form action="./changePassword"
					modelAttribute="changePasswordModel" method="post">

					<div class="form-group">
						<label for="oldPassword">Type your old password</label>
						<form:input type="password" class="form-control"
							path="oldPassword" />
					</div>

					<div class="form-group">
						<label for="newPassword1">Type your new password</label>
						<form:input type="password" class="form-control"
							path="newPassword1" />
					</div>

					<div class="form-group">
						<label for="newPassword2">Confirm your password</label>
						<form:input type="password" class="form-control"
							path="newPassword2" />
					</div>

					<button type="submit" class="btn btn-outline-dark">Submit</button>
				</form:form>
			</div>
		</div>
	</div>
</body>
</html>