<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="icon" type="image/png" href="images/icons/favicon.ico" />

<link rel="stylesheet" type="text/css"
	href="https://colorlib.com/etc/lf/Login_v5/vendor/bootstrap/css/bootstrap.min.css">

<link rel="stylesheet" type="text/css"
	href="https://colorlib.com/etc/lf/Login_v5/fonts/font-awesome-4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" type="text/css"
	href="https://colorlib.com/etc/lf/Login_v5/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">

<link rel="stylesheet" type="text/css"
	href="https://colorlib.com/etc/lf/Login_v5/vendor/animate/animate.css">

<link rel="stylesheet" type="text/css"
	href="https://colorlib.com/etc/lf/Login_v5/vendor/css-hamburgers/hamburgers.min.css">

<link rel="stylesheet" type="text/css"
	href="https://colorlib.com/etc/lf/Login_v5/vendor/animsition/css/animsition.min.css">

<link rel="stylesheet" type="text/css"
	href="https://colorlib.com/etc/lf/Login_v5/vendor/select2/select2.min.css">

<link rel="stylesheet" type="text/css"
	href="https://colorlib.com/etc/lf/Login_v5/vendor/daterangepicker/daterangepicker.css">

<link rel="stylesheet" type="text/css"
	href="https://colorlib.com/etc/lf/Login_v5/css/util.css">
<link rel="stylesheet" type="text/css"
	href="https://colorlib.com/etc/lf/Login_v5/css/main.css">
<title>Register</title>
</head>
<body>
	<div class="limiter">
		<div class="container-login100"
			style="background-image: url('uploads/images/bg-01.jpg');">
			<div class="wrap-login100 p-l-110 p-r-110 p-t-62 p-b-33">
				<form class="login100-form validate-form flex-sb flex-w"
					method="post" action="./registerEmployer">
					<span class="login100-form-title p-b-53"> Sign Up As
						Employer
						<div class="p-t-31 p-b-9">
							<span class="txt1"> <c:if test="${not empty errorMessage}">
									<div class="alert alert-danger" role="alert">
										${errorMessage}</div>
								</c:if>
							</span>
						</div>
					</span>
					<!-- <a
						href="#" class="btn-face m-b-20"> <i
						class="fa fa-facebook-official"></i> Facebook
					</a> <a href="#" class="btn-google m-b-20"> <img
						src="uploads/images/icon-google.png"
						alt="GOOGLE"> Google
					</a> -->
					<div class="p-t-31 p-b-9">
						<span class="txt1"> First Name </span>
					</div>
					<div class="wrap-input100 validate-input"
						data-validate="First Name is required">
						<input class="input100" type="text" name="firstName"
							id="firstName"> <span class="focus-input100"></span>
					</div>
					<div class="p-t-31 p-b-9">
						<span class="txt1"> Last Name </span>
					</div>
					<div class="wrap-input100 validate-input"
						data-validate="Last Name is required">
						<input class="input100" type="text" name="lastName" id="lastName">
						<span class="focus-input100"></span>
					</div>
					<div class="p-t-31 p-b-9">
						<span class="txt1"> Birthday </span>
					</div>
					<div class="wrap-input100 validate-input"
						data-validate="Birthday is required">
						<input class="input100" type="date" name="dob" id="dob">
						<span class="focus-input100"></span>
					</div>
					<div class="p-t-31 p-b-9">
						<span class="txt1"> Email </span>
					</div>
					<div class="wrap-input100 validate-input"
						data-validate="Email is required">
						<input class="input100" type="email" name="email" id="email">
						<span class="focus-input100"></span>
					</div>
					<div class="p-t-31 p-b-9">
						<span class="txt1"> Phone Number </span>
					</div>
					<div class="wrap-input100 validate-input"
						data-validate="Phone Number is required">
						<input class="input100" type="text" name="phoneNumber" id="phoneNumber">
						<span class="focus-input100"></span>
					</div>
					<div class="p-t-31 p-b-9">
						<span class="txt1"> Username </span>
					</div>
					<div class="wrap-input100 validate-input"
						data-validate="Username is required">
						<input class="input100" type="text" name="userName" id="userName">
						<span class="focus-input100"></span>
					</div>
					<div class="p-t-13 p-b-9">
						<span class="txt1"> Password </span>
					</div>
					<div class="wrap-input100 validate-input"
						data-validate="Password is required">
						<input class="input100" type="password" name="password"
							id="password"> <span class="focus-input100"></span>
					</div>

					<div class="container-login100-form-btn m-t-17">
						<button class="login100-form-btn" type="submit">Sign Up</button>
					</div>
					<div class="w-full text-center p-t-55">
						<span class="txt2"> Want to find a Job? <a
							href="${pageContext.request.contextPath}/register"
							class="txt2 bo1"> Sign up </a> as Candidate
						</span>
					</div>
					<div class="w-full text-center p-t-55">
						<span class="txt2"> Already have account? </span> <a
							href="${pageContext.request.contextPath}/login" class="txt2 bo1">
							Sign in </a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script
		src="https://colorlib.com/etc/lf/Login_v5/vendor/jquery/jquery-3.2.1.min.js"
		type="text/javascript"></script>

	<script
		src="https://colorlib.com/etc/lf/Login_v5/vendor/animsition/js/animsition.min.js"
		type="text/javascript"></script>

	<script
		src="https://colorlib.com/etc/lf/Login_v5/vendor/bootstrap/js/popper.js"
		type="text/javascript"></script>
	<script
		src="https://colorlib.com/etc/lf/Login_v5/vendor/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>

	<script
		src="https://colorlib.com/etc/lf/Login_v5/vendor/select2/select2.min.js"
		type="text/javascript"></script>
	<script src="https://colorlib.com/etc/lf/Login_v5/js/main.js"
		type="text/javascript"></script>
</body>
</html>